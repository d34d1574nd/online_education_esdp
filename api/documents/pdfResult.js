module.exports = (props) => {

  const resultTest = props.result.resultTest.map((item, index) => {
    return `<div key=${index + 1}>
    <p>${item.question.titleRu}</p>
    <div class="answers">
    <span class='answer'
      style=${item.answers.find(answer => answer.position === "a").isCorrect === true ? "color:green" : item.answers.find(answer => answer.position === "a").titleRu === item.answersTest.titleRu ? "color:red" : null}
      >
    <span>${item.answers.find(answer => answer.position === "a").position}) </span>
      ${item.answers.find(answer => answer.position === 'a').titleRu}
    </span>
    <span class='answer'
      style=${item.answers.find(answer => answer.position === "b").isCorrect === true ? "color:green" : item.answers.find(answer => answer.position === "b").titleRu === item.answersTest.titleRu ? "color:red" : null}
    >
    <span>${item.answers.find(answer => answer.position === "b").position}) </span>
  ${item.answers.find(answer => answer.position === 'b').titleRu}
</span>
  <span class='answer'
  style=${item.answers.find(answer => answer.position === "c").isCorrect === true ? "color:green" : item.answers.find(answer => answer.position === "c").titleRu === item.answersTest.titleRu ? "color:red" : null}
    >
    <span>${item.answers.find(answer => answer.position === "c").position}) </span>
  ${item.answers.find(answer => answer.position === 'c').titleRu}
</span>
  <span class='answer'
  style=${item.answers.find(answer => answer.position === "d").isCorrect === true ? "color:green" : item.answers.find(answer => answer.position === "d").titleRu === item.answersTest.titleRu ? "color:red" : null}
    >
    <span>${item.answers.find(answer => answer.position === "d").position}) </span>
  ${item.answers.find(answer => answer.position === 'd').titleRu}
</span>
  <span class='answer'
  style=${item.answers.find(answer => answer.position === "e").isCorrect === true ? "color:green" : (item.answers.find(answer => answer.position === "d").titleRu === item.answersTest.titleRu ? "color:red" : null)}
    >
    <span>${item.answers.find(answer => answer.position === "e").position}) </span>
  ${item.answers.find(answer => answer.position === 'e').titleRu}
</span>
          <br/>
          <div>
            <p><strong>Метод решения задачи: </strong></p>
            <img src=${`http://localhost:8000/uploads/${item.question.methodSolution}`} style="width:50%;margin-right:10px" align="top" class="img-thumbnail" alt="method solution" />;
          </div>
          <hr/>
  </div>
  </div>
  `
  });
  return `
<!doctype html>
<html lang="en">
  <head>
  <meta charset="utf-8">
    <title>Pdf results</title>
  <style>
  body {
  font-size: 8px;
  }

.answers {
    display: flex;
    flex-direction: row;
    align-items: stretch;
    margin-bottom: 10px;
}
.answer {
    flex-grow: 1;
    order: 1;
}
.userTest {
    width: 400px;
}
.testText {
    text-align: center;
}
.colorGreen {
    color: green;
}
.colorRed {
    color: red;
}
.textColor {
    padding: 0;
    margin: 0;
}
.legendColor {
    position: absolute;
    top: 15px;
    right: 50px;
}


  </style>
  </head>
    <body>     
        <div class='userTest'>
          <p>
            <span>Имя: </span>
            <span>${props.user.name}</span>
          </p>
          <p>
            <span>Фамилия: </span>
            <span>${props.user.surname}</span>
          </p>
           <p>
            <span>Номер телефона: </span>
            <span>${props.user.phoneNumber}</span>
          </p>
          <p>
            <span>Общее время выполнения: </span>
            <span>${props.result.result && props.result.result.timer}</span>
          </p>
          <p>
            <span>Количество баллов: </span>
            <span>${props.result.result && props.result.result.result}</span>
          </p>
        </div>
        <div class="legendColor">
          <p class="textColor"><span class="colorGreen">зеленый</span> правильный ответ</p>
          <p class="textColor"><span class="colorRed">крассный</span> не правильный ответ</p>
        </div>
        <h4 class="testText">Подробное описание прохождения теста</h4>
        ${resultTest}
    </body>
  </html>
    `;
};

