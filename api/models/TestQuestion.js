const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const TestQuestionSchema = new Schema({
  testId: {
    type: Schema.Types.ObjectId,
    ref: 'Test',
    required: true
  },
  questionId: {
    type: Schema.Types.ObjectId,
    ref: 'Question',
    required: true
  }
});

const TestQuestion = mongoose.model('TestQuestion', TestQuestionSchema);

module.exports = TestQuestion;
