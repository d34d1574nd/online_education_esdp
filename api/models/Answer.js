const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const AnswerSchema = new Schema({
    position: {
        type: String,
        required: true
    },
    titleRu: {
        type: String,
    },
    titleKy: {
        type: String,
    },
    questionId: {
        type: Schema.Types.ObjectId,
        ref: 'Question',
        required: true
    },
    isCorrect: {
        type: Boolean,
        required: true
    }
});

const Answer = mongoose.model('Answer', AnswerSchema);

module.exports = Answer;
