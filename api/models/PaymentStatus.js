const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const PaymentStatusShema = new Schema({
    order_id:{
        type: String
    },
    invoice_id:{
        type: String
    },
    qr:{
        type: String,
        unique: true,
    },
    link_app:{
        type: String
    },
    site_pay:{
        type: String
    },
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    }
});

const PaymentStatus = mongoose.model('PaymentStatus', PaymentStatusShema);

module.exports = PaymentStatus;