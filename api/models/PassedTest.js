const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const PassedTestSchema = new Schema({
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    date: {
        type: String,
        required: true
    },
    answers: {
        type: Array,
        required: true
    },
    result: {
        type: Number
        // required: true  // пока что уберем, пока не сделаем скрипт для проверки
    },
    testId: {
        type: Schema.Types.ObjectId,
        ref: 'Test',
        required: true
    },
    timer: {
        type: String,
        required: true
    }
});

const PassedTest = mongoose.model('PassedTest', PassedTestSchema);

module.exports = PassedTest;
