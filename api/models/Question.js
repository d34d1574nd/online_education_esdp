const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const QuestionSchema = new Schema({
    titleRu: {
        type: String
    },
    titleKy: {
        type: String
    },
    subjectId: {
        type: Schema.Types.ObjectId,
        ref: 'Subject',
        required: true
    },
    rank: {
        type: Number,
        required: true
    },
    themeRu: {
        type: String
    },
    themeKy: {
        type: String
    },
    methodSolution: {
        type: String
    },
    isFree: {
        type: Boolean,
        default: false,
    }
});

const Question = mongoose.model('Question', QuestionSchema);

module.exports = Question;
