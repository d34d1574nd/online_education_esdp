const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const nanoid = require("nanoid");

const SALT_WORK_FACTOR = 10;

const Schema = mongoose.Schema;

const UserSchema = new Schema({
    phoneNumber: {
        type: String,
        required: true,
        unique: true,
        validate: {
            validator: async function (value) {
                if (!this.isModified('phoneNumber')) return;
                const user = await User.findOne({phoneNumber: value});
                if (user) throw new Error();
            },
            message: 'This phoneNumber is already taken'
        }
    },
    password: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    surname: {
        type: String,
        required: true
    },
    paid: {
        type: Boolean,
        default: false,
        required: true
    },
    role: {
        type: String,
        required: true,
        default: 'pupil',
        enum: ['pupil', 'parent', 'teacher', 'deputy director', 'director', 'admin']
    },
    token: {
        type: String,
        required: true
    },
    balance: {
        type: Number,
        default: 0
    }
});

UserSchema.methods.checkPassword = function (password) {
    return bcrypt.compare(password, this.password);
};

UserSchema.methods.generateToken = function () {
    this.token = nanoid();
};

UserSchema.methods.setDate = function (date) {
    this.date = date;
};

UserSchema.pre('save', async function (next) {
    if (!this.isModified('password')) return next();

    const salt = await bcrypt.genSalt(SALT_WORK_FACTOR);
    const hash = await bcrypt.hash(this.password, salt);

    this.password = hash;

    next();
});

UserSchema.set('toJSON', {
    transform: (doc, ret, options) => {
        delete ret.password;
        return ret;
    }
});

const User = mongoose.model('User', UserSchema);

module.exports = User;

