const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const TestSchema = new Schema({
  subjectId: {
    type: Schema.Types.ObjectId,
    ref: 'Subject',
    required: true
  }
});

const Test = mongoose.model('Test', TestSchema);

module.exports = Test;
