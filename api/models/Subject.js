const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const SubjectSchema = new Schema({
  titleRu: {
    type: String,
    required: true,
    unique: true,
    validate: {
      validator: async function (value) {
        if (!this.isModified('titleRu')) return;
        const subject = await Subject.findOne({titleRu: value});
        if (subject) throw new Error();
      },
      message: 'Такой предмет уже есть в базе'
    }
  },
  titleKy: {
    type: String,
    required: true,
    unique: true,
    validate: {
      validator: async function (value) {
        if (!this.isModified('titleKy')) return;
        const subject = await Subject.findOne({titleKy: value});
        if (subject) throw new Error();
      },
      message: 'Такой предмет уже есть в базе'
    }
  },
  main : {
    type: Boolean,
    default: false,
    required: true
  },
  timer: {
    type: String,
  }
});

const Subject = mongoose.model('Subject', SubjectSchema);

module.exports = Subject;
