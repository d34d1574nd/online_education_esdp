const Answer = require('./models/Answer');

const resultModule = async tests => {
  let result = 0;

  for (let i = 0; i < tests.length; i++) {
    const answer = await Answer.findById(tests[i]).populate('questionId');

    if (await answer && answer.isCorrect === true) {
      result += await answer && answer.questionId && answer.questionId.rank
    }
  }

  return await result
};

module.exports = resultModule;
