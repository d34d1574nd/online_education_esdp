const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

const config = require('./config');
const user = require('./app/users');
const subject = require('./app/subjects');
const question = require('./app/questions');
const answer = require('./app/answers');
const test = require('./app/tests');
const payment = require('./app/payment');
const passedTest = require('./app/passedTest');
const testsQuestions = require('./app/testsQuestions');
const admin = require('./app/admin');
const detailResult = require('./app/detailResult');
const createPdf = require('./app/createPdf');
const bodyParser = require('body-parser');

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const port = process.env.NODE_ENV === 'test' ? 8010 : 8000;

mongoose.connect(config.dbUrl, config.mongoOptions).then(() => {
  app.use('/users', user);
  app.use('/admin', admin);
  app.use('/subjects', subject);
  app.use('/questions', question);
  app.use('/answers', answer);
  app.use('/tests', test);
  app.use('/passedTest', passedTest);
  app.use('/detailResult', detailResult);
  app.use('/testsQuestions', testsQuestions);
  app.use('/payment', payment);
  app.use('/result', createPdf);

  app.listen(port, () => {
    console.log(`Server started on ${port} port`);
  });
});
