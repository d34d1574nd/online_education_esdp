const mongoose = require('mongoose');
const config = require('./config');
const User = require('./models/User');
const Question = require('./models/Question');
const Subject = require('./models/Subject');
const Answer = require('./models/Answer');
const PassedTest = require('./models/PassedTest');

const Test = require('./models/Test');

const TestQuestion = require('./models/TestQuestion');

const run = async () => {
  await mongoose.connect(config.dbUrl, config.mongoOptions);

  const connection = mongoose.connection;

  const collections = await connection.db.collections();

  for (let collection of collections) {
    await collection.drop();
  }

  const users = await User.create(
    {
      name: 'Камиль',
      surname: 'Дауров',
      password: '123456Aa',
      phoneNumber: '+996 (551) 00-00-00',
      role: 'pupil',
      token: '1',
      paid: true,
      date: '0',
      balance: 100
    },
    {
      name: 'Максим',
      surname: 'Кривендо',
      password: '123456Aa',
      phoneNumber: '+996 (702) 24-77-83',
      role: 'pupil',
      token: '2',
      paid: false,
      date: '0'
    },
    {
      name: 'Светаслав',
      surname: 'Ким',
      password: '123456Aa',
      phoneNumber: '+996 (555) 33-00-83',
      role: 'pupil',
      token: '3',
      paid: true,
      date: '0',
      balance: 150
    },
    {
      name: 'Сергей',
      surname: 'Буров',
      password: '123456Aa',
      phoneNumber: '+996 (550) 33-25-83',
      role: 'pupil',
      token: '3',
      paid: true,
      date: '0',
      balance: 2000
    },
    {
      name: 'Александр',
      surname: 'Фрост',
      password: '123456Aa',
      phoneNumber: '+996 (555) 55-55-55',
      role: 'admin',
      token: '4',
      paid: false,
      date: '0'
    }
  );


  const subjects = await Subject.create(
    {titleRu: 'Русский', titleKy: 'Орус', main: true, timer: "01:00"},
    {titleRu: 'Математика', titleKy: 'Математика', main: true, timer: "01:30"},
    {titleRu: 'История', titleKy: 'История', timer: "00:30"},
    {titleRu: 'Физика', titleKy: 'Физика', timer: "02:00"},
    {titleRu: 'Химия', titleKy: 'Химия', timer: "00:50"},
  );


  const questions = await Question.create(
    {
      titleRu: 'Предлагаем начать с простого – определения частей речи. Одно из этих слов не является междометием',
      titleKy: 'Биз жөнөкөй баштап сунуш - сөз түркүмдөрүн аныктоо. Бул сөздөрдүн бири междометий эмес,',
      subjectId: subjects[0]._id,
      rank: Math.floor((Math.random() * 3) + 1),
      methodSolution: "images.jpeg",
    },
    {
      titleRu: 'Когда предмет в единственном числе, просклонять его не представляется проблемой. При использовании множественного числа ошибиться гораздо проще. Например, ошибочной будет форма',
      titleKy: 'Жекелик санда бир нерсе болгондо, төмөндөө көйгөй эмес болчу. Көптүк колдонуп жатканда ката кыйла оъой. Мисалы, туура эмес бир түрү болуп саналат',
      subjectId: subjects[0]._id,
      rank: Math.floor((Math.random() * 3) + 1),
      methodSolution: "images.jpeg",
    },
    {
      titleRu: 'Иногда сложности вызывает и необходимость сосчитать что-либо или кого-либо. Из нижеперечисленных правильным будет вариант',
      titleKy: 'Кээде себептерин жана муктаж татаалдыгы бир нерсеге же кимдир бирөөгө санап. Төмөнкүдөй адамдар туура чыгаруу болуп саналат',
      subjectId: subjects[0]._id,
      rank: Math.floor((Math.random() * 3) + 1),
      methodSolution: "images.jpeg",
    },
    {
      titleRu: 'В современном русском языке широко распространены случаи вариативного написания тех или иных слов. Тем не менее в соответствии со строгими литературными нормами, одно из исторических сражений могло происходить',
      titleKy: 'Азыркы орус тилинде кээ бир сөздөрдүн Variant жазуу жайылган иштер болуп саналат. Ошентсе да, адабияттын катуу эрежелерге ылайык, тарыхый согуш бири болушу мүмкүн ',
      subjectId: subjects[0]._id,
      rank: Math.floor((Math.random() * 3) + 1),
      methodSolution: "images.jpeg",
    },
    {
      titleRu: 'Правила русского языка не всегда отличаются единообразием. Вот и одно из этих слов написано с ошибкой',
      titleKy: 'Орус тилинин эрежелери дайыма эле бир түрдүүлүгү менен мүнөздөлөт эмес. Бул туура эмес, ошол сөздөрдүн бири ',
      subjectId: subjects[0]._id,
      rank: Math.floor((Math.random() * 3) + 1),
      methodSolution: "images.jpeg",
    },
    {
      titleRu: 'Тире от дефиса отличается не только происхождением (первое от французского "тянуть", второе от немецкого "деление"), но и правилами использования. В частности, в обязанности дефиса не входит',
      titleKy: 'Ташымалдоо тартып Dash гана келип чыккан эмес, (French "тарткан" биринчи, немис "бөлүнүү" экинчи), ошондой эле эрежелерди колдонуу. Атап айтканда, ташымалдоо милдеттери киргизилген эмес',
      subjectId: subjects[0]._id,
      rank: Math.floor((Math.random() * 3) + 1),
      methodSolution: "images.jpeg",
    },
    {
      titleRu: 'Использование союза "как" еще одна болевая точка для изучающих русский язык. В этом случае перед ним запятую ставить не нужно:',
      titleKy: 'Орус тилин изилдөө үчүн биримдикке дагы бир катуу пунктунда "деп" колдонуу. Бул учурда, ал үтүр үчүн зарыл эмес чейин',
      subjectId: subjects[0]._id,
      rank: Math.floor((Math.random() * 3) + 1),
      methodSolution: "images.jpeg",
    },
    {
      titleRu: 'Еще немного пунктуации. Запятая нужна только после одного из этих выражений, и это:',
      titleKy: 'Дагы бир аз тыныш. Үтүр менен бул сөздөрдү гана кийин талап кылынат, ал эми ал',
      subjectId: subjects[0]._id,
      rank: Math.floor((Math.random() * 3) + 1),
      methodSolution: "images.jpeg",
    },
    {
      titleRu: 'Часто путаница возникает при написании слов иноязычного происхождения. Из нижеперечисленных лишь одно написано правильно, это:',
      titleKy: 'Чет тилинде сөздөрдү жазып жатканда, көп учурда баш аламандык пайда болот. төмөнкү бир туура эмес жазылган, ал',
      subjectId: subjects[0]._id,
      rank: Math.floor((Math.random() * 3) + 1),
      methodSolution: "images.jpeg",
    },
    {
      titleRu: 'Преодоление трудностей перевода не исключает последующих трудностей с ударением. Многие забывают, что в этом слове оно ставится на последний слог:',
      titleKy: 'Котормонун кыйынчылыктарды жоюу басым кийинки кыйынчылык алып келбейт. Көп адамдар бул сөз аны акыркы муун жайгаштырылган экенин унутпа:',
      subjectId: subjects[0]._id,
      rank: Math.floor((Math.random() * 3) + 1),
      methodSolution: "images.jpeg",
    },

    {
      titleRu: 'Автор теории общественного договора как основы концепции антифеодального государства и права, свободного развития личности:',
      titleKy: 'Мамлекеттик жана чактын укуктар түшүнүгүнүн негизинде коомдук келишим теориясына жазуучу, инсанынын эркин иштеп чыгуу:',
      subjectId: subjects[1]._id,
      rank: Math.floor((Math.random() * 3) + 1),
      methodSolution: "images.jpeg",
    },
    {
      titleRu: 'Автор поэм предпринявший попытки рационализации представления об этическом, нравственно-правовом порядке в человеческих делах и отношениях:',
      titleKy: 'Ырлар Author этикалык, укук иштери, өз ара мамилелери аркылуу адеп-ахлактык жана укуктук тартипти түшүнүү актап, аракет:',
      subjectId: subjects[1]._id,
      rank: Math.floor((Math.random() * 3) + 1),
      methodSolution: "images.jpeg",
    },
    {
      titleRu: 'Автором «Народной монархии» является:',
      titleKy: 'Адамдардын монархия" деген жазуучу болуп саналат:',
      subjectId: subjects[1]._id,
      rank: Math.floor((Math.random() * 3) + 1),
      methodSolution: "images.jpeg",
    },
    {
      titleRu: 'Автором «Великой лжи нашего времени» является:',
      titleKy: 'Биздин замандын улуу жалган" деген жазуучу:',
      subjectId: subjects[1]._id,
      rank: Math.floor((Math.random() * 3) + 1),
      methodSolution: "images.jpeg",
    },
    {
      titleRu: 'В историю русской политической мысли П.И. Пестель вошел как:',
      titleKy: 'Орусиянын саясий ой PI тарыхында Pestel катары келип:',
      subjectId: subjects[1]._id,
      rank: Math.floor((Math.random() * 3) + 1),
      methodSolution: "images.jpeg",
    }, {
      titleRu: 'Великий французский мыслитель, просветитель, литератор. Автор изречения: «Свобода состоит в том, чтобы зависеть только от законов',
      titleKy: 'Улуу French ойчул, агартуучу жана жазуучу. Author сөздөрү: "Эркиндик мыйзамдарына гана көз каранды болуп саналат',
      subjectId: subjects[1]._id,
      rank: Math.floor((Math.random() * 3) + 1),
      methodSolution: "images.jpeg",
    }, {
      titleRu: 'Кто из древних мыслителей говорил об арифметической и геометрической справедливости:',
      titleKy: 'Ким кошууну жана геометриялык адилеттүүлүк жөнүндө сөз байыркы ойчулдарынан:',
      subjectId: subjects[1]._id,
      rank: Math.floor((Math.random() * 3) + 1),
      methodSolution: "images.jpeg",
    }, {
      titleRu: 'Кто из древних политических мыслителей считал, что государство должно состоять из 5040 граждан:',
      titleKy: 'Ким кошууну жана геометриялык адилеттүүлүк жөнүндө сөз байыркы ойчулдарынан',
      subjectId: subjects[1]._id,
      rank: Math.floor((Math.random() * 3) + 1),
      methodSolution: "images.jpeg",
    }, {
      titleRu: 'О монархическом правосознании писал:',
      titleKy: 'Адилеттүүлүк жөнүндө монархический мааниде жазган:',
      subjectId: subjects[1]._id,
      rank: Math.floor((Math.random() * 3) + 1),
      methodSolution: "images.jpeg",
    },
    {
      titleRu: 'Запорожская Сечь была ликвидирована, потому что..',
      titleKy: 'Анткени Бишкек баракта, жоюлган болду ..',
      subjectId: subjects[1]._id,
      rank: Math.floor((Math.random() * 3) + 1),
      methodSolution: "images.jpeg",
    },
    {
      titleRu: 'Где карабль погружается в воду глубже в море или в реке ',
      titleKy: 'Karabl суу же дарыя аралай сууга кайда',
      subjectId: subjects[2]._id,
      rank: Math.floor((Math.random() * 3) + 1),
      methodSolution: "images.jpeg",
    }, {
      titleRu: 'Какое поле появляется вокруг любого предмета',
      titleKy: 'Кайсы талаа болбосун объектисине айланып пайда ',
      subjectId: subjects[2]._id,
      rank: Math.floor((Math.random() * 3) + 1),
      methodSolution: "images.jpeg",
    }, {
      titleRu: 'Как называется стабильная элементарная частица с отрицательным электрическим зарядом',
      titleKy: 'Кантип туруктуу терс электр заряддуу башталгыч бөлүкчө деп аталат',
      subjectId: subjects[2]._id,
      rank: Math.floor((Math.random() * 3) + 1),
      methodSolution: "images.jpeg",
    }, {
      titleRu: 'Где поднять камень легче на суше или воле Какой закон это объясняет это',
      titleKy: 'Түшүндүрүп кайсы жерге таш терип жардам, же эч кандай мыйзам жок болот',
      subjectId: subjects[2]._id,
      rank: Math.floor((Math.random() * 3) + 1),
      methodSolution: "images.jpeg",
    },
    {
      titleRu: 'Как называется еденница измерения работы',
      titleKy: 'Аты edennitsa аткаруу өлчөө деген эмне',
      subjectId: subjects[2]._id,
      rank: Math.floor((Math.random() * 3) + 1),
      methodSolution: "images.jpeg",
    }, {
      titleRu: 'Температура при которой нагреваемая жидкость превращается в газа называтся',
      titleKy: 'Турган температурасы кызуу суюк газ nazyvatsya айландырылат',
      subjectId: subjects[2]._id,
      rank: Math.floor((Math.random() * 3) + 1),
      methodSolution: "images.jpeg",
    }, {
      titleRu: 'Как выглядит форула 3-го закрна Ньютона о силе действия и противолействия',
      titleKy: 'Кантип forula үчүнчү Newton иш-аракеттер жана protivoleystviya күчүн zakrna кылат',
      subjectId: subjects[2]._id,
      rank: Math.floor((Math.random() * 3) + 1),
      methodSolution: "images.jpeg",
    }, {
      titleRu: 'Что больше масса 1 л подсолнечного масла или масса 1 л воды',
      titleKy: 'Массалык 1л күн карама майын же 1 литр сууга массасы менен',
      subjectId: subjects[2]._id,
      rank: Math.floor((Math.random() * 3) + 1),
      methodSolution: "images.jpeg",
    }, {
      titleRu: 'Назовите формулу для вычесление массы тела(m) при известных объеме(v) и  плотности(p) ',
      titleKy: 'Орган жалпыга эсептелген номерлерине Аты формула (м) белгилүү көлөмү (V) жана тыгыздыгы (б)',
      subjectId: subjects[2]._id,
      rank: Math.floor((Math.random() * 3) + 1),
      methodSolution: "images.jpeg",
    }, {
      titleRu: 'Ядро атома состоит из ',
      titleKy: 'бир атомдун ядросунда турат',
      subjectId: subjects[3]._id,
      rank: Math.floor((Math.random() * 3) + 1),
      methodSolution: "images.jpeg",
    }, {
      titleRu: ' Число молей растворенного вещества, присутствующего в 1 кг растворителя называется',
      titleKy: ' Эритме менен эмес чечилсе азыркы чычкандарга саны 1 кг чакырды',
      subjectId: subjects[3]._id,
      rank: Math.floor((Math.random() * 3) + 1),
      methodSolution: "images.jpeg",
    }, {
      titleRu: 'Наиболее электроотрицательным элементом среди представленных ниже есть',
      titleKy: 'Көп электр элементтин бири төмөндө көрсөтүлгөн',
      subjectId: subjects[3]._id,
      rank: Math.floor((Math.random() * 3) + 1),
      methodSolution: "images.jpeg",
    }, {
      titleRu: 'Какой металл используется для извлечения меди из раствора сульфата меди',
      titleKy: 'Кайсы металл ургатуу жез казып алуу үчүн колдонулат',
      subjectId: subjects[3]._id,
      rank: Math.floor((Math.random() * 3) + 1),
      methodSolution: "images.jpeg",
    }, {
      titleRu: 'Число -электронов в Fe2 + (Z = 26) не равна',
      titleKy: 'Fe2 + электрондордун саны (Z = 26) бирдей эмес,',
      subjectId: subjects[3]._id,
      rank: Math.floor((Math.random() * 3) + 1),
      methodSolution: "images.jpeg",
    },
    {
      titleRu: 'Металлургический процесс, в котором металл получен в расплавленном состоянии, называется',
      titleKy: 'Металл турган металлургиялык чакырып, ээриген абалда алынат',
      subjectId: subjects[3]._id,
      rank: Math.floor((Math.random() * 3) + 1),
      methodSolution: "images.jpeg",
    },
    {
      titleRu: 'Наиболее древние породы в земной коре были когда-то расплавлен, и пришли из глубины земли. Расплавленная порода, называемая магмой, выбрасывалось в вулканических извержений в начале земной жизни и затвердевает в скальных породах, называемых',
      titleKy: 'Жер кыртышынын байыркы тектер бир эрип, жер түбүндө болду. Магма деп аталат куйма рок деп аталган жердеги жашоосунун башында жана рок катууланышында менен жанар тоолордун атылышы менен чыгарылды',
      subjectId: subjects[3]._id,
      rank: Math.floor((Math.random() * 3) + 1),
      methodSolution: "images.jpeg",
    }, {
      titleRu: ' Закон, который гласит, что количество газа, растворенного в жидкости, пропорционально его парциальному давлению',
      titleKy: ' Суюктук ээриген газ көлөмү анын толук кысым жараша экени айтылат Мыйзам',
      subjectId: subjects[3]._id,
      rank: Math.floor((Math.random() * 3) + 1),
      methodSolution: "images.jpeg",
    }, {
      titleRu: ' Основной буферной системой крови человека есть',
      titleKy: ' Негизги туруучу адамдын кан системасы бар',
      subjectId: subjects[3]._id,
      rank: Math.floor((Math.random() * 3) + 1),
      methodSolution: "images.jpeg",
    }, {
      titleRu: ' Газ, присутствующий в стратосфере, который отфильтровывает некоторые из солнечного ультрафиолетового излучения и обеспечивает эффективную защиту от лучевого поражения живых существ',
      titleKy: ' Күндүн Газ ушул оонадагы айрым чыпкалай анын UV нурлары жана жандуулар үчүн нурлануу зыян каршы натыйжалуу коргоону камсыз кылат',
      subjectId: subjects[3]._id,
      rank: Math.floor((Math.random() * 3) + 1),
      methodSolution: "images.jpeg",
    }, {
      titleRu: ' Наиболее часто используемым отбеливателем является',
      titleKy: 'Көбүнчө колдонулган аки таш болуп',
      subjectId: subjects[3]._id,
      rank: Math.floor((Math.random() * 3) + 1),
      methodSolution: "images.jpeg",
    },
  );

  const answers = await Answer.create(
    {
      position: 'a',
      titleRu: 'Увы',
      titleKy: 'аттигин',
      questionId: questions[0]._id,
      isCorrect: false
    },
    {
      position: 'b',
      titleRu: 'Физкульт-привет',
      titleKy: 'Физкульт-салам',
      questionId: questions[0]._id,
      isCorrect: false
    },
    {
      position: 'c',
      titleRu: 'Авось',
      titleKy: 'Авось',
      questionId: questions[0]._id,
      isCorrect: true
    },
    {
      position: 'd',
      titleRu: 'Ага',
      titleKy: 'и-и',
      questionId: questions[0]._id,
      isCorrect: false
    },
    {
      position: 'e',
      titleRu: 'Кто-то',
      titleKy: 'Кимдир бирөө',
      questionId: questions[0]._id,
      isCorrect: false
    },

    {
      position: 'a',
      titleRu: 'Несколько башен',
      titleKy: 'бир нече башня-го',
      questionId: questions[1]._id,
      isCorrect: false
    },
    {
      position: 'b',
      titleRu: 'Несколько вальтов',
      titleKy: 'бир нече вальтов',
      questionId: questions[1]._id,
      isCorrect: true
    },
    {
      position: 'c',
      titleRu: 'Несколько кочерег',
      titleKy: 'бир нече кочерег',
      questionId: questions[1]._id,
      isCorrect: false
    },
    {
      position: 'd',
      titleRu: 'Несколько машин',
      titleKy: 'бир нече  машин',
      questionId: questions[1]._id,
      isCorrect: false
    },
    {
      position: 'e',
      titleRu: 'Несколько рыб',
      titleKy: 'бир нече балык',
      questionId: questions[1]._id,
      isCorrect: false
    },

    {
      position: 'a',
      titleRu: 'Двое мужчин',
      titleKy: 'эки эркек, киши',
      questionId: questions[2]._id,
      isCorrect: true
    },
    {
      position: 'b',
      titleRu: 'Два мужчины',
      titleKy: 'эки киши',
      questionId: questions[2]._id,
      isCorrect: false
    },
    {
      position: 'c',
      titleRu: 'Можно и так, и так',
      titleKy: 'Сиз мүмкүн, ошондуктан',
      questionId: questions[2]._id,
      isCorrect: false
    },
    {
      position: 'd',
      titleRu: 'Две мужчин',
      titleKy: 'эки киш',
      questionId: questions[2]._id,
      isCorrect: false
    },
    {
      position: 'e',
      titleRu: 'Двое мужчина',
      titleKy: 'эки кише',
      questionId: questions[2]._id,
      isCorrect: false
    },
    {
      position: 'a',
      titleRu: 'Под Бородино',
      titleKy: ' Бородино',
      questionId: questions[3]._id,
      isCorrect: false
    },
    {
      position: 'b',
      titleRu: 'Под Бородином',
      titleKy: 'Бородином',
      questionId: questions[3]._id,
      isCorrect: true
    },
    {
      position: 'c',
      titleRu: 'Под Бородиным',
      titleKy: 'Под Бородиным',
      questionId: questions[3]._id,
      isCorrect: false
    },
    {
      position: 'd',
      titleRu: 'Под Бородным',
      titleKy: 'алкагында',
      questionId: questions[3]._id,
      isCorrect: false
    },

    {
      position: 'e',
      titleRu: 'Под Бородныме',
      titleKy: 'Под Бородныме',
      questionId: questions[3]._id,
      isCorrect: false

    },

    {
      position: 'a',
      titleRu: 'Пол-Чехии',
      titleKy: 'Пол-Чехии',
      questionId: questions[4]._id,
      isCorrect: false

    },
    {
      position: 'b',
      titleRu: 'Пол-лекции',
      titleKy: 'Пол-лекции',
      questionId: questions[4]._id,
      isCorrect: false

    },

    {
      position: 'c',
      titleRu: 'Пол-четверти',
      titleKy: 'Пол-четверти',
      questionId: questions[4]._id,
      isCorrect: true

    },

    {
      position: 'd',
      titleRu: 'Пол-четверт',
      titleKy: 'Пол-четверт',
      questionId: questions[4]._id,
      isCorrect: false

    },

    {
      position: 'e',
      titleRu: 'Пол-четверте',
      titleKy: 'Пол-четверте',
      questionId: questions[4]._id,
      isCorrect: false

    },

    {
      position: 'a',
      titleRu: 'Пол-Чехии',
      titleKy: 'Пол-Чехии',
      questionId: questions[5]._id,
      isCorrect: false

    },
    {
      position: 'b',
      titleRu: 'Пол-лекции',
      titleKy: 'Пол-лекции',
      questionId: questions[5]._id,
      isCorrect: false

    },

    {
      position: 'c',
      titleRu: 'Пол-лекциии',
      titleKy: 'Пол-лекциии',
      questionId: questions[5]._id,
      isCorrect: false

    },
    {
      position: 'd',
      titleRu: 'Пол-четверти',
      titleKy: 'Пол-четверти',
      questionId: questions[5]._id,
      isCorrect: true

    },
    {
      position: 'e',
      titleRu: 'Пол-лекци',
      titleKy: 'Пол-лекци',
      questionId: questions[5]._id,
      isCorrect: false

    },

    {
      position: 'a',
      titleRu: '"Руки его дрожали_ как ртуть". (Гоголь)',
      titleKy: '"Өз колу менен сымап катары". (Гоголь)',
      questionId: questions[6]._id,
      isCorrect: false

    },
    {
      position: 'b',
      titleRu: '"И видел он себя богатым_ как во сне". (Крылов)',
      titleKy: '"Ал түшүндө өзүн көрүп". (Крылов)',
      questionId: questions[6]._id,
      isCorrect: false

    },
    {
      position: 'c',
      titleRu: 'Руки его дрожали_ как ртуть (Достоевский)',
      titleKy: 'Өз колу менен сымап катары (Достоевский)',
      questionId: questions[6]._id,
      isCorrect: false

    },
    {
      position: 'd',
      titleRu: 'И видел он себя богатым_ как во сне (Пушкин)',
      titleKy: '"Ал түшүндө өзүн көрүп (Пушкин)',
      questionId: questions[6]._id,
      isCorrect: false

    },
    {
      position: 'e',
      titleRu: '"Я говорю_ как литератор". (Горький)',
      titleKy: '"Мен жазуучу". (Горький)',
      questionId: questions[6]._id,
      isCorrect: true

    },
    {
      position: 'a',
      titleRu: 'По всей видимости',
      titleKy: 'Кыязы',
      questionId: questions[7]._id,
      isCorrect: true

    },
    {
      position: 'b',
      titleRu: 'В первую очередь',
      titleKy: 'Биринчи кезекте',
      questionId: questions[7]._id,
      isCorrect: false

    },

    {
      position: 'c',
      titleRu: 'В итоге',
      titleKy: 'Жыйынтыгында',
      questionId: questions[7]._id,
      isCorrect: false

    },
    {
      position: 'd',
      titleRu: 'В итоге будет',
      titleKy: 'Натыйжасы болуп калат',
      questionId: questions[7]._id,
      isCorrect: false

    },
    {
      position: 'e',
      titleRu: 'В очередь первую',
      titleKy: 'Биринчи кезекте',
      questionId: questions[7]._id,
      isCorrect: false
    },

    {
      position: 'a',
      titleRu: 'Координально',
      titleKy: 'Координального',
      questionId: questions[8]._id,
      isCorrect: false
    },
    {
      position: 'b',
      titleRu: 'Фальстарт',
      titleKy: 'Фальстарт',
      questionId: questions[8]._id,
      isCorrect: true
    },
    {
      position: 'c',
      titleRu: 'Каппучино',
      titleKy: 'Каппучино',
      questionId: questions[8]._id,
      isCorrect: false
    },
    {
      position: 'd',
      titleRu: 'Каппучи',
      titleKy: 'Каппучи',
      questionId: questions[8]._id,
      isCorrect: false
    },
    {
      position: 'e',
      titleRu: 'фальш',
      titleKy: 'жалган',
      questionId: questions[8]._id,
      isCorrect: false
    },
    {
      position: 'a',
      titleRu: 'Пурпур',
      titleKy: 'Пурпур',
      questionId: questions[9]._id,
      isCorrect: false
    },
    {
      position: 'b',
      titleRu: 'Латте',
      titleKy: 'Латте',
      questionId: questions[9]._id,
      isCorrect: false
    },
    {
      position: 'c',
      titleRu: 'Фетиш',
      titleKy: 'Фетиш',
      questionId: questions[9]._id,
      isCorrect: true
    },
    {
      position: 'd',
      titleRu: 'Фетишист',
      titleKy: 'Фетишишст',
      questionId: questions[9]._id,
      isCorrect: false
    },
    {
      position: 'e',
      titleRu: 'мышь',
      titleKy: 'мышь',
      questionId: questions[9]._id,
      isCorrect: false
    },
    {
      position: 'a',
      titleRu: 'Кант',
      titleKy: 'Кант',
      questionId: questions[10]._id,
      isCorrect: false
    },
    {
      position: 'b',
      titleRu: 'Руссо',
      titleKy: 'Руссо',
      questionId: questions[10]._id,
      isCorrect: true
    },
    {
      position: 'c',
      titleRu: 'Монтескье',
      titleKy: 'Монтескье',
      questionId: questions[10]._id,
      isCorrect: false
    },
    {
      position: 'd',
      titleRu: 'Гегель',
      titleKy: 'Гегель',
      questionId: questions[10]._id,
      isCorrect: false
    },
    {
      position: 'e',
      titleRu: 'Рита',
      titleKy: 'Рита',
      questionId: questions[10]._id,
      isCorrect: false
    },
    {
      position: 'a',
      titleRu: 'Фалес',
      titleKy: 'Фалес',
      questionId: questions[11]._id,
      isCorrect: false
    },
    {
      position: 'b',
      titleRu: 'Солон',
      titleKy: 'Солон ',
      questionId: questions[11]._id,
      isCorrect: false
    },
    {
      position: 'c',
      titleRu: 'Гомер',
      titleKy: 'Гомер ',
      questionId: questions[11]._id,
      isCorrect: true
    },
    {
      position: 'd',
      titleRu: 'Хилон',
      titleKy: 'Хилон',
      questionId: questions[11]._id,
      isCorrect: false
    },
    {
      position: 'e',
      titleRu: 'Адом',
      titleKy: 'Адом',
      questionId: questions[11]._id,
      isCorrect: false
    },
    {
      position: 'a',
      titleRu: 'Тихомиров',
      titleKy: 'Тихомиров',
      questionId: questions[12]._id,
      isCorrect: false
    },
    {
      position: 'b',
      titleRu: 'П. Победоносцев',
      titleKy: 'П. Победоносцев',
      questionId: questions[12]._id,
      isCorrect: false
    },
    {
      position: 'c',
      titleRu: 'П. Победоносц',
      titleKy: 'П. Победоносц',
      questionId: questions[12]._id,
      isCorrect: false
    },
    {
      position: 'd',
      titleRu: 'Н. Леонтьев',
      titleKy: 'Н. Леонтьев',
      questionId: questions[12]._id,
      isCorrect: false
    },
    {
      position: 'e',
      titleRu: 'А.Ильин',
      titleKy: 'А.Ильин',
      questionId: questions[12]._id,
      isCorrect: false
    },
    {
      position: 'a',
      titleRu: 'Д. Кавелин',
      titleKy: 'Д. Кавелин',
      questionId: questions[13]._id,
      isCorrect: false
    },
    {
      position: 'b',
      titleRu: 'Н. Леонтьев',
      titleKy: 'Н. Леонтьев',
      questionId: questions[13]._id,
      isCorrect: false
    },
    {
      position: 'c',
      titleRu: 'П. Победоносцев',
      titleKy: 'П. Победоносцев',
      questionId: questions[13]._id,
      isCorrect: true
    },
    {
      position: 'd',
      titleRu: 'Бакунин',
      titleKy: 'Бакунин',
      questionId: questions[13]._id,
      isCorrect: false
    },
    {
      position: 'e',
      titleRu: 'Плутарх',
      titleKy: 'Плутарх',
      questionId: questions[13]._id,
      isCorrect: false
    },
    {
      position: 'a',
      titleRu: 'Сторонник предоставления право избирать всем гражданам',
      titleKy: 'Укугунун жактоочусу бардык жарандары добуш берүүгө ',
      questionId: questions[14]._id,
      isCorrect: false
    },
    {
      position: 'b',
      titleRu: 'Сторонник идеи эгалитаризма',
      titleKy: 'Эгалитаризмге идеясынын өтүүдө',
      questionId: questions[14]._id,
      isCorrect: false
    }, {
      position: 'c',
      titleRu: 'Последовательный республиканец',
      titleKy: 'Арты-артынан Республикалык',
      questionId: questions[14]._id,
      isCorrect: true
    }, {
      position: 'd',
      titleRu: 'Сторонник самодержавия',
      titleKy: 'жеке бийлиги ',
      questionId: questions[14]._id,
      isCorrect: false
    }, {
      position: 'e',
      titleRu: 'Последовательный сторонник ',
      titleKy: 'ырааттуу жактоочусу',
      questionId: questions[14]._id,
      isCorrect: false
    }, {
      position: 'a',
      titleRu: 'Боден',
      titleKy: 'Боден',
      questionId: questions[15]._id,
      isCorrect: false
    }, {
      position: 'b',
      titleRu: 'Кельвин',
      titleKy: 'Кельвин',
      questionId: questions[15]._id,
      isCorrect: false
    }, {
      position: 'c',
      titleRu: 'Вольтер',
      titleKy: 'Вольтер',
      questionId: questions[15]._id,
      isCorrect: true
    }, {
      position: 'd',
      titleRu: 'Дидро',
      titleKy: 'Дидро',
      questionId: questions[15]._id,
      isCorrect: false
    }, {
      position: 'e',
      titleRu: 'Самарин ',
      titleKy: 'Самарин',
      questionId: questions[15]._id,
      isCorrect: false
    }, {
      position: 'a',
      titleRu: 'Цицерон',
      titleKy: 'Цицерон',
      questionId: questions[16]._id,
      isCorrect: false
    }, {
      position: 'b',
      titleRu: 'Платон',
      titleKy: 'Платон',
      questionId: questions[16]._id,
      isCorrect: false
    }, {
      position: 'c',
      titleRu: 'Полибий',
      titleKy: 'Полибий',
      questionId: questions[16]._id,
      isCorrect: false
    }, {
      position: 'd',
      titleRu: 'Аристотель ',
      titleKy: 'Аристотель',
      questionId: questions[16]._id,
      isCorrect: true
    }, {
      position: 'e',
      titleRu: 'Самарин ',
      titleKy: 'Самарин',
      questionId: questions[16]._id,
      isCorrect: false
    }, {
      position: 'a',
      titleRu: 'Полибий',
      titleKy: 'Полибий',
      questionId: questions[17]._id,
      isCorrect: false
    }, {
      position: 'b',
      titleRu: 'Аристотель',
      titleKy: 'Аристотель',
      questionId: questions[17]._id,
      isCorrect: false
    }, {
      position: 'c',
      titleRu: 'Цицерон ',
      titleKy: 'Цицерон',
      questionId: questions[17]._id,
      isCorrect: false
    }, {
      position: 'd',
      titleRu: 'Лео',
      titleKy: 'Лео',
      questionId: questions[17]._id,
      isCorrect: false
    }, {
      position: 'e',
      titleRu: 'Платон ',
      titleKy: 'Платон',
      questionId: questions[17]._id,
      isCorrect: true
    }, {
      position: 'a',
      titleRu: 'А.Ильин',
      titleKy: 'А.Ильин',
      questionId: questions[18]._id,
      isCorrect: true
    }, {
      position: 'b',
      titleRu: 'Н.Леонтьев',
      titleKy: 'Н.Леонтьев',
      questionId: questions[18]._id,
      isCorrect: false
    }, {
      position: 'c',
      titleRu: 'Бкунин',
      titleKy: 'Бкунин',
      questionId: questions[18]._id,
      isCorrect: false
    }, {
      position: 'd',
      titleRu: 'Д.Кавелин',
      titleKy: 'Д.Кавелин',
      questionId: questions[18]._id,
      isCorrect: false
    }, {
      position: 'e',
      titleRu: 'П.Победоносцев',
      titleKy: 'П.Победоносцев',
      questionId: questions[18]._id,
      isCorrect: false
    }, {
      position: 'a',
      titleRu: 'Екатерине II нужны были её земли;',
      titleKy: 'Екатерине  II анын жерин керек;',
      questionId: questions[19]._id,
      isCorrect: false
    }, {
      position: 'b',
      titleRu: 'Сечь была населена «беспокойным » элементом, готовым в любую минуту выступить «за землю и волю»;',
      titleKy: 'Издөө "жер жана эркиндик үчүн" иш-аракет кылууга ар кандай учурда даяр "тынчы жок" элементи менен тарабайт»;',
      questionId: questions[19]._id,
      isCorrect: true
    }, {
      position: 'c',
      titleRu: 'потому что казаки могли примкнуть к донскому казаку Емельяну Пугачеву.',
      titleKy: 'Казактар Дон казактары Emelyan Болтушкин кошулушу мүмкүн, анткени,.',
      questionId: questions[19]._id,
      isCorrect: false
    }, {
      position: 'd',
      titleRu: 'потому что казаки могли зделать засаду',
      titleKy: 'Казактар бир буктурма түзүүгө мүмкүн, анткени,',
      questionId: questions[19]._id,
      isCorrect: false
    },
    {
      position: 'e',
      titleRu: 'потому что казаки могли напасть',
      titleKy: 'Себеби, казактар чабуул жасашы мүмкүн',
      questionId: questions[19]._id,
      isCorrect: false
    },
    {
      position: 'a',
      titleRu: 'в реке',
      titleKy: 'дарыядан',
      questionId: questions[20]._id,
      isCorrect: true
    }, {
      position: 'b',
      titleRu: 'в море',
      titleKy: 'деңиз',
      questionId: questions[20]._id,
      isCorrect: false
    }, {
      position: 'c',
      titleRu: 'везде одинаково',
      titleKy: 'Ошол эле жерде',
      questionId: questions[20]._id,
      isCorrect: false
    }, {
      position: 'd',
      titleRu: 'в океане',
      titleKy: 'океанда',
      questionId: questions[20]._id,
      isCorrect: false
    }, {
      position: 'e',
      titleRu: 'в тихом океане',
      titleKy: 'Шош',
      questionId: questions[20]._id,
      isCorrect: false
    }, {
      position: 'a',
      titleRu: 'Магнитное',
      titleKy: 'магниттик',
      questionId: questions[21]._id,
      isCorrect: false
    }, {
      position: 'b',
      titleRu: 'Гравитационное',
      titleKy: 'тартылуу',
      questionId: questions[21]._id,
      isCorrect: true
    }, {
      position: 'c',
      titleRu: 'Электрическое',
      titleKy: 'электрдик',
      questionId: questions[21]._id,
      isCorrect: false
    }, {
      position: 'd',
      titleRu: 'во всех',
      titleKy: 'бардык',
      questionId: questions[21]._id,
      isCorrect: false
    }, {
      position: 'e',
      titleRu: 'ни-где',
      titleKy: 'жок-кайдае',
      questionId: questions[21]._id,
      isCorrect: false
    }, {
      position: 'a',
      titleRu: 'Электрон',
      titleKy: 'Электрон',
      questionId: questions[22]._id,
      isCorrect: true
    }, {
      position: 'b',
      titleRu: 'Пазирон',
      titleKy: 'Пазирон',
      questionId: questions[22]._id,
      isCorrect: false
    }, {
      position: 'c',
      titleRu: 'Электрод',
      titleKy: 'Электрод',
      questionId: questions[22]._id,
      isCorrect: false
    }, {
      position: 'd',
      titleRu: 'Атом',
      titleKy: 'Атом',
      questionId: questions[22]._id,
      isCorrect: false
    }, {
      position: 'e',
      titleRu: 'Флюид',
      titleKy: 'Флюид',
      questionId: questions[22]._id,
      isCorrect: false
    }, {
      position: 'a',
      titleRu: 'везде одинаково',
      titleKy: 'везде одинаково',
      questionId: questions[23]._id,
      isCorrect: false
    }, {
      position: 'b',
      titleRu: 'На суше',
      titleKy: 'На суше',
      questionId: questions[23]._id,
      isCorrect: false
    }, {
      position: 'c',
      titleRu: 'В воде',
      titleKy: 'Ввде',
      questionId: questions[23]._id,
      isCorrect: false
    }, {
      position: 'd',
      titleRu: 'нигде',
      titleKy: 'нишде',
      questionId: questions[23]._id,
      isCorrect: false
    }, {
      position: 'e',
      titleRu: 'ответа нет',
      titleKy: 'ответа нет',
      questionId: questions[23]._id,
      isCorrect: false
    }, {
      position: 'a',
      titleRu: 'Джоуль',
      titleKy: 'Джоуль',
      questionId: questions[24]._id,
      isCorrect: true
    }, {
      position: 'b',
      titleRu: 'Паскаль',
      titleKy: 'Паскаль',
      questionId: questions[24]._id,
      isCorrect: false
    }, {
      position: 'c',
      titleRu: 'Ньютон',
      titleKy: 'Ньютон',
      questionId: questions[24]._id,
      isCorrect: false
    }, {
      position: 'd',
      titleRu: 'ответа нет',
      titleKy: 'ответа нет',
      questionId: questions[24]._id,
      isCorrect: false
    }, {
      position: 'e',
      titleRu: 'Алберд',
      titleKy: 'Алберд',
      questionId: questions[24]._id,
      isCorrect: false
    }, {
      position: 'a',
      titleRu: 'Точка плавление',
      titleKy: 'эрүү',
      questionId: questions[25]._id,
      isCorrect: false
    }, {
      position: 'b',
      titleRu: 'Точка кипения',
      titleKy: 'кайноо чекити',
      questionId: questions[25]._id,
      isCorrect: true
    }, {
      position: 'c',
      titleRu: 'Точка кристаллизации',
      titleKy: 'кристаллдашуу чекити',
      questionId: questions[25]._id,
      isCorrect: false
    }, {
      position: 'd',
      titleRu: 'Точка испарения',
      titleKy: 'буулануу чекити',
      questionId: questions[25]._id,
      isCorrect: false
    }, {
      position: 'e',
      titleRu: 'Точка окисления',
      titleKy: 'кычкылдануу чекити',
      questionId: questions[25]._id,
      isCorrect: false
    },
    {
      position: 'a',
      titleRu: 'F1 = -F2',
      titleKy: 'F1 = -F2',
      questionId: questions[26]._id,
      isCorrect: true
    }, {
      position: 'b',
      titleRu: 'F1 = -F',
      titleKy: 'F1 = -F',
      questionId: questions[26]._id,
      isCorrect: false
    }, {
      position: 'c',
      titleRu: 'F = ma',
      titleKy: 'F = ma',
      questionId: questions[26]._id,
      isCorrect: false
    }, {
      position: 'd',
      titleRu: 'F = mc',
      titleKy: 'F =mc',
      questionId: questions[26]._id,
      isCorrect: false
    }, {
      position: 'e',
      titleRu: 'F1 = -F3',
      titleKy: 'F1 = -F3',
      questionId: questions[26]._id,
      isCorrect: false
    }, {
      position: 'a',
      titleRu: 'Больше масса воды',
      titleKy: 'Суу Large органдары',
      questionId: questions[27]._id,
      isCorrect: true
    }, {
      position: 'b',
      titleRu: 'Больше масса масла',
      titleKy: 'мунай массалык',
      questionId: questions[27]._id,
      isCorrect: false
    }, {
      position: 'c',
      titleRu: 'Вода и масло одинаковы',
      titleKy: 'Суу менен камсыздоо жана мунай эле',
      questionId: questions[27]._id,
      isCorrect: false
    }, {
      position: 'd',
      titleRu: 'нет очтвета',
      titleKy: 'жок ответ',
      questionId: questions[27]._id,
      isCorrect: false
    }, {
      position: 'e',
      titleRu: 'масла больше',
      titleKy: 'мунай',
      questionId: questions[27]._id,
      isCorrect: false
    }, {
      position: 'a',
      titleRu: 'm = pV',
      titleKy: 'm =pV',
      questionId: questions[28]._id,
      isCorrect: true
    }, {
      position: 'b',
      titleRu: 'm = V/p',
      titleKy: 'm =V/p',
      questionId: questions[28]._id,
      isCorrect: false
    }, {
      position: 'c',
      titleRu: 'm = pv3',
      titleKy: 'масла больше',
      questionId: questions[28]._id,
      isCorrect: false
    }, {
      position: 'd',
      titleRu: 'm = v/p2',
      titleKy: 'm = v/p2',
      questionId: questions[28]._id,
      isCorrect: false
    }, {
      position: 'e',
      titleRu: 'm = v3',
      titleKy: 'm = v3',
      questionId: questions[28]._id,
      isCorrect: false
    },
    {
      position: 'a',
      titleRu: 'Не изменяется',
      titleKy: 'Не изменяется',
      questionId: questions[29]._id,
      isCorrect: false
    }, {
      position: 'b',
      titleRu: 'Увеличевается',
      titleKy: 'Увеличевается',
      questionId: questions[29]._id,
      isCorrect: false
    }, {
      position: 'c',
      titleRu: 'Уменьшается',
      titleKy: 'Уменьшается',
      questionId: questions[29]._id,
      isCorrect: false
    }, {
      position: 'd',
      titleRu: 'Уввеличевается и уменшается',
      titleKy: 'Көбөйтүү жана азайтуу',
      questionId: questions[29]._id,
      isCorrect: false
    }, {
      position: 'e',
      titleRu: 'ответа нет',
      titleKy: 'ответ жок',
      questionId: questions[29]._id,
      isCorrect: false
    }, {
      position: 'a',
      titleRu: 'электроны и нейтроны',
      titleKy: 'лектрондор менен нейтрондор',
      questionId: questions[30]._id,
      isCorrect: false
    }, {
      position: 'b',
      titleRu: 'электроны и протоны',
      titleKy: 'электрон менен протон',
      questionId: questions[30]._id,
      isCorrect: false
    }, {
      position: 'c',
      titleRu: 'протоны и нейтроны',
      titleKy: 'протон менен нейтрондор',
      questionId: questions[30]._id,
      isCorrect: true
    }, {
      position: 'd',
      titleRu: 'Все вышеперечисленное',
      titleKy: 'жогоруда айтылгандардын баары',
      questionId: questions[30]._id,
      isCorrect: false
    }, {
      position: 'e',
      titleRu: 'протоны и  электроны',
      titleKy: ' протоны и  электроны',
      questionId: questions[30]._id,
      isCorrect: false
    }, {
      position: 'a',
      titleRu: 'моляльность',
      titleKy: ' моляльность',
      questionId: questions[31]._id,
      isCorrect: true

    }, {
      position: 'b',
      titleRu: 'молярность',
      titleKy: 'молярность',
      questionId: questions[31]._id,
      isCorrect: false
    }, {
      position: 'c',
      titleRu: 'нормальность',
      titleKy: 'нормалдуу',
      questionId: questions[31]._id,
      isCorrect: false
    }, {
      position: 'd',
      titleRu: 'формальность',
      titleKy: ' формалдык',
      questionId: questions[31]._id,
      isCorrect: false
    }, {
      position: 'e',
      titleRu: 'вышеперечисленное',
      titleKy: ' вжогору',
      questionId: questions[31]._id,
      isCorrect: false
    }, {
      position: 'a',
      titleRu: 'натрий',
      titleKy: 'натрий',
      questionId: questions[32]._id,
      isCorrect: false
    }, {
      position: 'b',
      titleRu: 'бром',
      titleKy: 'бром',
      questionId: questions[32]._id,
      isCorrect: false
    }, {
      position: 'c',
      titleRu: 'фтор',
      titleKy: 'фтор',
      questionId: questions[32]._id,
      isCorrect: true
    }, {
      position: 'd',
      titleRu: 'кислород',
      titleKy: 'кислород',
      questionId: questions[32]._id,
      isCorrect: false
    }, {
      position: 'e',
      titleRu: 'хлор',
      titleKy: 'хлор',
      questionId: questions[32]._id,
      isCorrect: false
    }, {
      position: 'a',
      titleRu: ' Na',
      titleKy: ' Na',
      questionId: questions[33]._id,
      isCorrect: false
    }, {
      position: 'b',
      titleRu: 'Ag',
      titleKy: 'Ag',
      questionId: questions[33]._id,
      isCorrect: false
    }, {
      position: 'c',
      titleRu: 'Hg',
      titleKy: 'Hg',
      questionId: questions[33]._id,
      isCorrect: false
    }, {
      position: 'd',
      titleRu: 'Fe',
      titleKy: 'Fe',
      questionId: questions[33]._id,
      isCorrect: true
    }, {
      position: 'e',
      titleRu: 'De',
      titleKy: 'De',
      questionId: questions[33]._id,
      isCorrect: false
    }, {
      position: 'a',
      titleRu: 'p-электроны в Ne(Z = 10)',
      titleKy: 'p-электроны в Ne(Z = 10)',
      questionId: questions[34]._id,
      isCorrect: false
    }, {
      position: 'b',
      titleRu: 's-электроны в Mg(Z = 12)',
      titleKy: 's-электроны в Mg(Z = 12)',
      questionId: questions[34]._id,
      isCorrect: false
    }, {
      position: 'c',
      titleRu: 'd-электроны в Fe(Z = 26)',
      titleKy: 'd-электроны в Fe(Z = 26)',
      questionId: questions[34]._id,
      isCorrect: false
    }, {
      position: 'd',
      titleRu: 'p-электроны в Cl(Z = 17)',
      titleKy: 'p-электроны в Cl(Z = 17)',
      questionId: questions[34]._id,
      isCorrect: true
    }, {
      position: 'e',
      titleRu: 'a-электроны в Cl(Z = 10)',
      titleKy: 'a-электроны в Cl(Z = 10)',
      questionId: questions[34]._id,
      isCorrect: false
    }, {
      position: 'a',
      titleRu: 'Виплавка',
      titleKy: 'Виплавка',
      questionId: questions[35]._id,
      isCorrect: true
    }, {
      position: 'b',
      titleRu: 'Обжиг',
      titleKy: 'Обжиг',
      questionId: questions[35]._id,
      isCorrect: false
    }, {
      position: 'c',
      titleRu: 'Прокаливание',
      titleKy: 'меши',
      questionId: questions[35]._id,
      isCorrect: false
    }, {
      position: 'd',
      titleRu: 'Пена размещения',
      titleKy: 'Пена размещения',
      questionId: questions[35]._id,
      isCorrect: false
    }, {
      position: 'e',
      titleRu: 'размещения',
      titleKy: 'размещения',
      questionId: questions[35]._id,
      isCorrect: false
    }, {
      position: 'a',
      titleRu: 'гранит',
      titleKy: 'гранит',
      questionId: questions[36]._id,
      isCorrect: false
    }, {
      position: 'b',
      titleRu: 'базальт',
      titleKy: 'базальт',
      questionId: questions[36]._id,
      isCorrect: false
    }, {
      position: 'c',
      titleRu: 'магматические породы',
      titleKy: 'магматические породы',
      questionId: questions[36]._id,
      isCorrect: true
    }, {
      position: 'd',
      titleRu: 'осадочные породы',
      titleKy: 'осадочные породы',
      questionId: questions[36]._id,
      isCorrect: false
    }, {
      position: 'e',
      titleRu: 'магнии',
      titleKy: 'магнии',
      questionId: questions[36]._id,
      isCorrect: false
    }, {
      position: 'a',
      titleRu: 'Закон Дальтона',
      titleKy: 'Закон Дальтона',
      questionId: questions[37]._id,
      isCorrect: false
    }, {
      position: 'b',
      titleRu: 'закон Гей Люссака',
      titleKy: 'закон Гей Люссака',
      questionId: questions[37]._id,
      isCorrect: false
    }, {
      position: 'c',
      titleRu: ' Закон Генри',
      titleKy: ' Закон Генри',
      questionId: questions[37]._id,
      isCorrect: true
    }, {
      position: 'd',
      titleRu: 'Закон Рауля',
      titleKy: 'Закон Рауля',
      questionId: questions[37]._id,
      isCorrect: false
    }, {
      position: 'e',
      titleRu: 'Закон Алона',
      titleKy: 'Закон Алона',
      questionId: questions[37]._id,
      isCorrect: false
    }, {
      position: 'a',
      titleRu: 'H2CO3 — HCO3',
      titleKy: 'H2CO3 — HCO3',
      questionId: questions[38]._id,
      isCorrect: true
    }, {
      position: 'b',
      titleRu: 'H2CO3 — CO32-',
      titleKy: 'H2CO3 — CO32-',
      questionId: questions[38]._id,
      isCorrect: false
    }, {
      position: 'c',
      titleRu: 'CH3COOH — CH3COO—',
      titleKy: 'CH3COOH — CH3COO—',
      questionId: questions[38]._id,
      isCorrect: false
    }, {
      position: 'd',
      titleRu: 'NH2CONH2 — NH2CONH+',
      titleKy: 'NH2CONH2 — NH2CONH+',
      questionId: questions[38]._id,
      isCorrect: false
    }, {
      position: 'e',
      titleRu: 'H2CO3 — CO34',
      titleKy: 'H2CO3 — CO34',
      questionId: questions[38]._id,
      isCorrect: false
    }, {
      position: 'a',
      titleRu: 'алкоголь',
      titleKy: 'алкоголь',
      questionId: questions[39]._id,
      isCorrect: false
    }, {
      position: 'b',
      titleRu: ' двуокись углерода',
      titleKy: 'көмүр кычкыл газы',
      questionId: questions[39]._id,
      isCorrect: false
    }, {
      position: 'c',
      titleRu: 'хлор',
      titleKy: 'хлор',
      questionId: questions[39]._id,
      isCorrect: true
    }, {
      position: 'd',
      titleRu: 'хлорид натрия',
      titleKy: 'хлорид натрия',
      questionId: questions[39]._id,
      isCorrect: false
    }, {
      position: 'e',
      titleRu: 'озон',
      titleKy: 'озон',
      questionId: questions[39]._id,
      isCorrect: false
    },
  );


  const tests = await Test.create(
    {subjectId: subjects[0]._id},
    {subjectId: subjects[1]._id},
    {subjectId: subjects[2]._id},
    {subjectId: subjects[3]._id},
  );

  this.state = {
    test: [],
    passed: []
  };

  for (let i = 0; i < 5; i++) {
    await this.state.test.push({testId: tests[1]._id, questionId: questions[i]._id})
  }

  await TestQuestion.create(
    this.state.test,
    {testId: tests[1]._id, questionId: questions[0]._id}
  );

  for (let i = 0; i < 20; i++) {
    await this.state.passed.push({
      userId: users[0]._id,
      date: new Date(),
      answers: [answers[i]._id],
      result: 80,
      testId: tests[0]._id,
      timer: '01:30:00'
    })
  }

  await PassedTest.create(
    this.state.passed
  );

  await connection.close();


};


run().catch(error => {
  console.log('Something wrong happened...', error);
});
