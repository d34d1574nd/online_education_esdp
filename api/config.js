const path = require('path');

const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, 'public/uploads'),
  dbUrl: process.env.NODE_ENV === 'test' ? 'mongodb://localhost/online_education_test' : 'mongodb://localhost/online_education',
  mongoOptions: {
    useNewUrlParser: true,
    useCreateIndex: true
  }
};
