const express = require('express');
const CryptoJS = require('crypto-js');
const axios = require('axios');
const nanoid =require('nanoid');
const auth = require('../middleware/auth');
const User = require('../models/User');
const PaymentStatus = require('../models/PaymentStatus');

const router = express.Router();


router.post('/', auth ,async (req, res) =>{
    try {
        const BASE_URL = 'https://mwalletsitold.dengi.o.kg/api/json/json.php';
        const passw = 'R7TZ0CJBK#3{##&';
        const user = await User.findById(req.user._id);
        const data = await  JSON.stringify({
            cmd: "createInvoice",
            version: 1005,
            sid: "8373950850",
            mktime: "1560398011274",
            lang: "ru",
            data: {
                order_id:  '4050230875d007e2ef5d1d' ,
                desc: "оплата",
                amount: 0,
                currency: null,
                test: 1,
                transtype: 1,
                long_term: 1,
                user_to:   user.phoneNumber,
                date_life: null,
                date_start_push: null,
                count_push: null,
                result_url: null,
                success_url: 'http://localhost:3000/',
                fail_url: null,
                fields_other: ""
            }
        });
        const hash = CryptoJS.HmacMD5(data,passw).toString();
        const dataAfter = await JSON.parse(data);
        dataAfter['hash']=hash;

        await  axios({
            method: 'post',
            url : BASE_URL,
            data: dataAfter,
            headers: {
                'Content-Type':'application/json',
            }

    }).then(response=>{
            const data = response.data.data;
            const payment = new PaymentStatus({
                order_id: data.order_id,
                invoice_id: data.invoice_id,
                qr: data.qr,
                link_app: data.link_app,
                site_pay: data.site_pay,
                userId: req.user._id
            });
            payment.save();
            res.send(payment.qr);
        })
    } catch (e) {
        console.log(e);
    }
});

router.get('/', [auth],  async (req, res) =>{
    try {
        const user = await User.findById(req.user._id);
        const BASE_URL = 'https://mwalletsitold.dengi.o.kg/api/json/json.php';
        const data = await PaymentStatus.findOne({userId: req.user._id});
        const passw = 'R7TZ0CJBK#3{##&';
        const statusData = await JSON.stringify({
            cmd: "statusPayment",
            version: 1005,
            sid: "8373950850",
            mktime: "1560398011274",
            lang: "ru",
            data: {
                order_id: data.order_id,
                invoice_id: data.invoice_id,
                mark: 1
            }
        });
        const hash = await CryptoJS.HmacMD5(statusData,passw).toString();
        const dataAfter = await JSON.parse(statusData);
        dataAfter['hash'] = hash;

        await axios({
            method: 'post',
            url : BASE_URL,
            data: dataAfter,
            headers: {
                'Content-Type':'application/json',
            }
        }).then((response)=> {
            const payments = response.data.data.payments;
            if (response.data && response.data.data.payments && response.data.data.payments[0].status === 'approved') {
                user.paid = true;
                let total = 0;
                for (let i = 0; i < payments.length; i++) {
                    const parseAmount = parseInt(payments[i].amount);
                    total += parseAmount;
                    user.balance = total/100;
                }
                console.log(user.balance);
                user.save()
            }
            return res.send(user)
        });

    } catch (e) {
       return res.send({'message': 'Оплата не прошла'})
    }
});



module.exports = router;














