const express = require('express');
const TestQuestion = require('../models/TestQuestion');

const router = express.Router();

router.get('/', async (req, res) => {
    try {
        const tests = await TestQuestion.find().populate('questionId');
        return res.send(tests)
    } catch (e) {
        return res.status(500).send(e);
    }
});

router.post('/', async (req, res) => {
    try {
        const testItem = new TestQuestion({
            testId: req.body.testId,
            questionId: req.body.questionId
        });
        await testItem.save();
        res.send(testItem);
    } catch (e) {
        return res.status(500).send(e);
    }
});

router.delete('/:id', async (req, res) => {
    const testQuestion = await TestQuestion.findOne({_id: req.params.id});

    if (!testQuestion) {
        return res.sendStatus(403);
    }
    await testQuestion.remove();
    res.send({message: "OK"})
});

module.exports = router;
