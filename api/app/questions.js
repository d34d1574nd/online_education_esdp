const express = require('express');

const Question = require('../models/Question');

const router = express.Router();

router.get('/', async (req, res) => {
    try {
        const question = await Question.find().populate('subjectId');
        return res.send(question);
    } catch (e) {
        return res.status(500).send(e);
    }
});

router.post('/', async (req, res) => {
    try {
        const question = new Question({
            titleRu: req.body.titleRu,
            titleKy: req.body.titleKy,
            subjectId: req.body.subjectId,
            rank: req.body.rank,
            isFree: req.body.isFree
        });

        await question.save();

        res.send(question);

    } catch (e) {
        return res.status(500).send(e);
    }
});

router.delete('/:id', async (req, res) => {
    try {
        const question = await Question.findOne({_id: req.params.id});

        if (!question) {
            return res.sendStatus(403);
        }

        await question.remove();
        res.send({message: "OK"})

    } catch (e) {
        return res.status(500).send(e);
    }
});

router.put('/:id', async (req, res) => {
    try {
        const editQuestion = req.body;

        const changeQuestion = await Question.findById({_id: req.params.id});
        changeQuestion.titleRu = editQuestion.titleRu;
        changeQuestion.titleKg = editQuestion.titleKy;
        changeQuestion.subjectId = editQuestion.subjectId;
        changeQuestion.rank = editQuestion.rank;
        changeQuestion.isFree = editQuestion.isFree;

        await changeQuestion.save();
        res.send(changeQuestion);

    } catch (e) {
        return res.status(500).send(e);
    }
});

module.exports = router;
