const express = require('express');
const multer = require('multer');
const nanoid = require('nanoid');
const config = require('../config');
const path = require('path');

const permit = require('../middleware/permit');
const auth = require('../middleware/auth');

const Question = require('../models/Question');
const Answer = require('../models/Answer');
const Subject = require('../models/Subject');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

router.get('/questions', [auth, permit('admin')], async (req, res) => {
  try {
    const question = await Question.find().populate('subjectId');
    return res.send(question);
  } catch (e) {
    return res.status(500).send(e);
  }
});

router.get('/subjects', [auth], async (req, res) => {
  try {
    const subject = await Subject.find();
    return res.send(subject)
  } catch (e) {
    return res.status(500).send(e);
  }
});

router.post('/', [auth, permit('admin'), upload.single('methodSolution')], async (req, res) => {

  try {
    const question = new Question({
      titleRu: req.body.titleRu,
      titleKy: req.body.titleKy,
      subjectId: JSON.parse(req.body.subject),
      rank: req.body.rank,
      themeKy: req.body.themeKy,
      themeRu: req.body.themeRu,
      isFree: req.body.isFree
    });
    if (req.file) {
      question.methodSolution = req.file.filename;
    } else {
      return res.send({'message': 'Ошибка загрузки'})
    }
    if (JSON.parse(req.body.subject) === '') {
      return await res.status(400).send({'error': 'Выберите предмет'})
    } else if (req.body.themeKy === '' && req.body.themeRu === '') {
      return await res.status(400).send({'error': 'Введите тему'})
    } else if (req.body.titleRu === '' && req.body.titleKy === '') {
      return await res.status(400).send({'error': 'Введите вопрос'})
    } else if (req.body.rank === 0) {
      return await res.status(400).send({'error': 'Выберите сложность вопроса (звездочки)'})
    } else if (JSON.parse(req.body.answers).length === 5 && req.body.titleKy !== '' || req.body.titleRu !== '') {
      await question.save();
    }
    const answers = await JSON.parse(req.body.answers).map(title => {
      if (title.titleRu !== '' || title.titleKy !== '') {
        return {
          position: title.position,
          titleRu: title.titleRu,
          titleKy: title.titleKy,
          questionId: question._id,
          isCorrect: title.isCorrect
        }
      } else {
        return res.status(400).send({'error': 'Введите варианты ответов'})
      }
    });
    const correct = await answers.filter(correct => correct.isCorrect === true);
    if (correct.length === 0) {
      return await res.status(400).send({'error': 'Выберите правильный ответ'})
    } else if (correct.length === 1) {
      answers.map(item => {
        const answers = new Answer(item);
        answers.save()
      })
    }

    return res.sendStatus(200);
  } catch (e) {
    console.log(e);
    if (e.name === 'ValidationError') {
      return res.status(400).send(e);
    }
    return res.status(500).send(e);
  }
});

router.get('/questions/:id', [auth, permit('admin')], async (req, res) => {
  try {
    const question = await Question.findById({_id: req.params.id}).populate('subjectId');
    const answers = await Answer.find({questionId: req.params.id});
    const singleQuestion = {
      id: question._id,
      answers: answers,
      rank: question.rank,
      show: false,
      subject: question.subjectId,
      subjectTitleKy: question.subjectId.titleKy,
      subjectTitleRu: question.subjectId.titleRu,
      titleKy: question.titleKy,
      titleKySubject: "",
      titleRu: question.titleRu,
      titleRuSubject: "",
      themeRu: question.themeRu,
      themeKy: question.themeKy,
      isFree: question.isFree
    };
    return res.send(singleQuestion);
  } catch (e) {
    console.log(e);
    return res.status(500).send(e);
  }
});
router.get('/subjects/:id', [auth, permit('admin')], async (req, res) => {
  try {
    const subject = await Subject.findById({_id: req.params.id});
    const sigleSubject = {
      id: subject._id,
      titleRu: subject.titleRu,
      titleKy: subject.titleKy,
      main: subject.main,
      timer: subject.timer
    };
    return res.send(sigleSubject)
  } catch (e) {
    return res.status(500).send(e);
  }
});

router.patch('/subjects/:id', [auth, permit('admin')], async (req, res) => {
  try {
    const editSubject = req.body;

    const changeSubject = await Subject.findById({_id: req.params.id});
    changeSubject.titleRu = editSubject.titleRu;
    changeSubject.titleKy = editSubject.titleKy;
    changeSubject.main = editSubject.main;
    changeSubject.timer = editSubject.timer;

    if (editSubject.titleKy === '' && editSubject.titleRu === '') {
      return await res.status(400).send({'error': 'Введите пожалуйста название предмета'})
    } else if (editSubject.titleKy !== '' && editSubject.titleRu !== '') {
      await changeSubject.save()
    }
    return res.send({message: "OK"});

  } catch (e) {
    return res.status(500).send(e);
  }
});


router.patch('/questions/:id', [auth, permit('admin'), upload.single('methodSolution')], async (req, res) => {
  try {
    await Question.updateOne({_id: req.params.id}, {
      $set: {
        titleRu: req.body.titleRu,
        titleKy: req.body.titleKy,
        subjectId: JSON.parse(req.body.subject),
        rank: req.body.rank,
        themeRu: req.body.themeRu,
        themeKy: req.body.themeKy,
        isFree: req.body.isFree,
        methodSolution: req.body.file
      }
    });
    if (req.body.titleKy === '' && req.body.titleRu === '') {
      return await res.status(400).send({'error': 'Введите пожалуйста вопросы'})
    }

    const changeAnswer = JSON.parse(req.body.answers);

    for (let i = 0; i < changeAnswer.length; i++) {
      await Answer.updateOne({_id: changeAnswer[i]._id}, {
        $set: {
          titleRu: changeAnswer[i].titleRu,
          titleKy: changeAnswer[i].titleKy,
          isCorrect: changeAnswer[i].isCorrect
        }
      })

    }
    return res.sendStatus(200);
  } catch (e) {
    console.log(e);
    if (e.name === 'ValidationError') {
      return res.status(400).send(e);
    }
    return res.status(500).send(e);
  }
});


router.delete('/subjects/:id', [auth, permit('admin')], async (req, res) => {
  try {
    await Subject.deleteOne({_id: req.params.id});

    await Question.find({subjectId: req.params.id}).then(result => {
      result.forEach((question) => {
        Answer.deleteMany({questionId: question._id})
          .catch(error => res.status(400).send(error))
      })
    });

    await Question.deleteMany({subjectId: req.params.id});

    res.send({message: "OK"})

  } catch (e) {
    return res.status(500).send(e);
  }
});

router.delete('/questions/:id', [auth, permit('admin')], async (req, res) => {
  try {
    await Question.find({_id: req.params.id}).then(result => {
      result.forEach((question) => {
        Answer.deleteMany({questionId: question._id})
          .catch(error => res.status(400).send(error))
      })
    });
    await Question.deleteOne({_id: req.params.id});
    res.send({message: "OK"})
  } catch (e) {
    return res.status(500).send(e);
  }

});


module.exports = router;
