
const express = require('express');

const User = require('../models/User');

const permit = require('../middleware/permit');

const auth = require('../middleware/auth');

const router = express.Router();

router.get('/:id', async (req, res) => {
    try {
        const user = await User.findOne({_id: req.params.id});
        return res.send(user)
    } catch (e) {
        return res.status(500).send(e);
    }
});

router.get('/', async (req, res) => {
    try {
        const users = await User.find();
        return res.send(users)
    } catch (e) {
        return res.status(500).send(e);
    }
});


router.post('/', async (req, res) => {
    const user = new User({
        phoneNumber: req.body.phoneNumber,
        password: req.body.password,
        name: req.body.name,
        surname: req.body.surname
    });
    user.generateToken();

    try {
        await user.save();
        return res.send({message: 'User registered', user});
    } catch (error) {
        return res.status(400).send(error)
    }

});

router.post('/sessions', async (req, res) => {
    const user = await User.findOne({phoneNumber: req.body.phoneNumber});

    if (!user) {
        return res.status(400).send({error: 'Вы ввели неправильно номер телефона или пароль'});
    }

    const isMatch = await user.checkPassword(req.body.password);

    if (!isMatch) {
        return res.status(400).send({error: 'Вы ввели неправильно  пароль'});
    }

    user.generateToken();

    await user.save();

    res.send({message: 'Login successful', user});
});

router.delete('/sessions', async (req, res) => {
    const token = req.get('Authorization');
    const success = {message: 'Logged out'};

    if (!token) {
        return res.send(success)
    }

    const user = await User.findOne({token});

    if (!user) {
        return res.send(success);
    }

    user.generateToken();
    await user.save();

    return res.send(success)
});


router.put('/change_role/:id',[auth, permit('admin')],async (req, res) => {
    const user = await User.findById(req.params.id);
    if (!user){
        return res.sendStatus(401)
    }

    user.role = req.body.role;

    await user.save();


    return res.sendStatus(200)


});

module.exports = router;
