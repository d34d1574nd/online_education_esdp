const express = require('express');
const PassedTest = require('../models/PassedTest');
const Answer = require('../models/Answer');
const Question = require('../models/Question');

const router = express.Router();

router.get('/:id', async (req, res) => {
  try {
    let resultTest = [];

    let result = await PassedTest.findById(req.params.id);
    let answersTest = [];


    for (let i = 0; i < result.answers.length; i++) {
      const answer = await Answer.findById(result.answers[i]).populate('questionId');
      answersTest.push(answer)
    }

    let questionTest = [];

    for (let i = 0; i < answersTest.length; i++) {
      const questions = await Question.findById(answersTest[i].questionId._id);
      questionTest.push(questions);
    }

    await questionTest.forEach(async (question, index) => {
      let item = {question: question, answers: [], answersTest: answersTest[index]};

      await Answer.find({questionId: question._id}).then(response => {
        item.answers = [...item.answers, ...response];
        resultTest = [...resultTest, item]
      });


      if (questionTest.length === resultTest.length) {
        return res.send({resultTest, result})
      }
    });

  } catch (e) {
    console.log(e);
    return res.status(500).send(e);
  }
});

module.exports = router;
