const express = require('express');

const auth = require('../middleware/auth');
const PassedTest = require('../models/PassedTest');
const User = require('../models/User');
const ResultModule = require('../ResultModule');

const router = express.Router();

router.get('/', async (req, res) => {
    try {
        const passedTest = await PassedTest.find().populate('testId').populate('userId');
        return res.send(passedTest);
    } catch (e) {
        return res.status(500).send(e);
    }
});

router.get('/:id', async (req, res) => {
    try {
        const passedTest = await PassedTest.find({userId: req.params.id});
        return res.send(passedTest);
    } catch (e) {
        return res.status(500).send(e);
    }
});


router.post('/', auth, async (req, res) => {
    try {
        const ModuleResult = await ResultModule(req.body.answers);
        const result = await new PassedTest({
            userId: req.user,
            date: new Date(),
            answers: [...req.body.answers],
            result: ModuleResult,
            testId: req.body.testId,
            timer: req.body.timer
        });
        // await User.updateOne(
        //     {_id: req.user},
        //     // {date: result.date},
        //     {upsert: false}
        // );
        //   The default value is false, which does not insert a new document when no match is found.
        //   https://docs.mongodb.com/manual/reference/method/db.collection.update/
        await result.save();
        return res.send(result)

    } catch (e) {
        return res.status(500).send(e);
    }
});

module.exports = router;
