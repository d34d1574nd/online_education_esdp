const express = require('express');

const Test = require('../models/Test');
const TestQuestion = require('../models/TestQuestion');
const Answer = require('../models/Answer');
const Question = require('../models/Question');

const router = express.Router();

router.get('/', async (req, res) => {
    try {
        console.log(req.query);
        // const array = [];
        // const ranks = {1: 15, 2: 15, 3: 10};
        //
        // let count = 0;
        // Object.values(ranks).map(number => {
        //     count += number;
        // });
        //
        // await Object.keys(ranks).map(async key => {
        //     let items = [];
        //     const questions = await Question.find({rank: key});
        //
        //     for (let i = 0; items.length < (ranks[key] + 1); i++) {
        //
        //         let item = questions[Math.floor(Math.random() * questions.length)];
        //         if (!items.includes(item)) {
        //             items.push(item);
        //         }
        //     }
        //
        //     items.map(item => {
        //         array.push(item);
        //     });
        //     // console.log(array);
        // });
        // console.log(array.length);




        // if (array.length === count) {
        //     res.send(array);
        // }


        // for (let i = 0; i < items.length; i++) {
        //     if (!items[i] === questionItem) {
        //         items.push(questionItem);
        //     }
        // }
        // }
        // console.log(questionItem);

        const tests = await Test.find();
        res.send(tests);
    } catch (e) {
        return res.status(500).send(e);
    }
});

router.post('/', async (req, res) => {
    try {
        const test = new Test({subjectId: req.body.subjectId});
        await test.save();
        res.send(test);
    } catch (e) {
        if (e.name === 'ValidationError') {
            return res.status(400).send(e);
        }
        return res.status(500).send(e);
    }
});

router.delete('/:id', async (req, res) => {
    const test = await Test.findOne({_id: req.params.id});

    if (!test) {
        return res.sendStatus(403);
    }
    await test.remove();
    res.send({message: "OK"})
});

router.get('/:id', async (req, res) => {
    try {
        let data = {testId: req.params.id, test: []};
        const questions = await TestQuestion.find({testId: req.params.id}).populate('questionId');

        await questions.forEach(async function (question) {
            let item = {question: question, answers: []};

            await Answer.find({questionId: question.questionId._id}).then(response => {
                item.answers = [...item.answers, ...response];
                data.test = [...data.test, item];
            });
            if (questions.length === data.test.length) {
                res.send(data)
            }
        });
    } catch (e) {
        return res.status(500).send(e);
    }
});

module.exports = router;
