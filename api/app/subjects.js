const express = require('express');

const permit = require('../middleware/permit');
const auth = require('../middleware/auth');
const Subject = require('../models/Subject');

const router = express.Router();

router.get('/', async (req, res) => {
  try {
    const subject = await Subject.find();
    return res.send(subject)
  } catch (e) {
    return res.status(500).send(e);
  }
});

router.post('/', [auth, permit('admin')], async (req, res) => {
  try {
    const subject = new Subject({
      titleRu: req.body.titleRu,
      titleKy: req.body.titleKy,
      main: req.body.main,
      timer: req.body.timer
    });

    await subject.save();

    res.send(subject);

  } catch (e) {
    if (e.name === 'ValidationError') {
      return res.status(400).send(e);
    }
    return res.status(500).send(e);
  }
});

router.delete('/:id', async (req, res) => {
  const subject = await Subject.findOne({_id: req.params.id});

  if (!subject) {
    return res.sendStatus(403);
  }

  await subject.remove();
  res.send({message: "OK"})
});

module.exports = router;
