const express = require('express');

const Answer = require('../models/Answer');

const router = express.Router();

router.get('/', async (req, res) => {
  try {
    const answer = await Answer.find().populate('questionId');
    return res.send(answer);
  } catch (e) {
    return res.status(500).send(e);
  }
});

router.post('/', async (req, res) => {
  try {
    const answer = new Answer({
      titleRu: req.body.titleRu,
      titleKy: req.body.titleKy,
      questionId: req.body.questionId,
      isCorrect: req.body.isCorrect
    });

    await answer.save();

    return res.send(answer);

  } catch (e) {
    if (e.name === 'ValidationError') {
      return res.status(400).send(e);
    }
    return res.status(500).send(e);
  }
});

router.delete('/:id', async(req, res) => {
  try {
    const answer = await Answer.findOne({_id: req.params.id});
    if (!answer) {
      return res.sendStatus(403);
    }
    await answer.remove();
    res.send({message: "OK"})
  } catch (e) {
    return res.status(500).send(e);
  }
});

router.put('/:id', async (req, res) => {
  try {
    const editAnswer = req.body;

    const changeAnswer  = await Answer.findById({_id: req.params.id});
    changeAnswer.titleRu = editAnswer.titleRu;
    changeAnswer.titleKg = editAnswer.titleKy;
    changeAnswer.questionId = editAnswer.questionId;
    changeAnswer.isCorrect = editAnswer.isCorrect;

    await changeAnswer.save();
    res.send(changeAnswer);

  } catch (e) {
    return res.status(500).send(e);
  }
});

module.exports = router;
