const express = require('express');

const pdf = require('html-pdf');
const pdfTemplate = require('../documents/pdfResult');
const option = {format: 'Letter', border: '1cm'};

const router = express.Router();

router.post('/create-pdf', (req, res) => {
  pdf.create(pdfTemplate(req.body), option).toFile(`${__dirname}/result.pdf`, (err) => {
    if(err) {
      res.send(Promise.reject());
    }
    return res.send(Promise.resolve());
  });
});

router.get('/fetch-pdf', (req, res) => {
  return res.sendFile(`${__dirname}/result.pdf`)
});

module.exports = router;
