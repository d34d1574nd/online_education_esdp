const mongoose = require('mongoose');
const config = require('./config');
const faker = require('faker');

const User = require('./models/User');
const Question = require('./models/Question');
const Subject = require('./models/Subject');
const Test = require('./models/Test');
const TestQuestion = require('./models/TestQuestion');
const PassedTest = require('./models/PassedTest');
const Answer = require('./models/Answer');

const run = async () => {
  await mongoose.connect(config.dbUrl, config.mongoOptions);

  const connection = mongoose.connection;
  const collections = await connection.db.collections();

  for (let collection of collections) {
    await collection.drop();
  }

  const users = await User.create(
    {
      name: 'u1',
      surname: 'U1',
      password: '123456Aa',
      phoneNumber: '+996 (551) 00-00-00',
      role: 'pupil',
      token: '1',
      paid: true,
      balance: 100
    },
    {
      name: 'u2',
      surname: 'U2',
      password: '123456Aa',
      phoneNumber: '+996 (702) 24-77-83',
      role: 'pupil',
      token: '2',
      paid: false
    },
    {
      name: 'u3',
      surname: 'U3',
      password: '123456Aa',
      phoneNumber: '+996 (555) 33-00-83',
      role: 'pupil',
      token: '3',
      paid: true
    },
    {
      name: 'u4',
      surname: 'U4',
      password: '123456Aa',
      phoneNumber: '+996 (550) 33-25-83',
      role: 'pupil',
      token: '3',
      paid: true
    },
    {
      name: 'admin',
      surname: 'administrator',
      password: '123456Aa',
      phoneNumber: '+996 (555) 55-55-55',
      role: 'admin',
      token: '4',
      paid: false
    }
  );

  const subjects = await Subject.create(
    {titleRu: 'предмет1Ру', titleKy: 'предмет1Кг', main: true, timer: "01:00"},
    {titleRu: 'предмет2Ру', titleKy: 'предмет2Кг', timer: "01:00"},
    {titleRu: 'предмет3Ру', titleKy: 'предмет3Кг', main: true, timer: "01:00"},
    {titleRu: 'история', titleKy: 'тарых', timer: "01:00"},
  );

  this.state = {
    question: [],
    questionKy: [],
    answers: [],
    answersKy: [],
    test: [],
    passed: []
  };

  for (let i = 0; i < 5; i++) {
    await this.state.question.push({
      themeRu: faker.lorem.sentence(),
      titleRu: faker.lorem.sentence(),
      subjectId: subjects[0]._id,
      rank: Math.floor((Math.random() * 3) + 1),
      methodSolution: "images.jpeg",
      isFree: false
    })
  }

  const questions = await Question.create(
    this.state.question
  );

  for (let i = 0; i < 5; i++) {
    await this.state.questionKy.push({
      themeKy: faker.lorem.sentence(),
      titleKy: faker.lorem.sentence(),
      subjectId: subjects[0]._id,
      rank: Math.floor((Math.random() * 3) + 1),
      methodSolution: "images.jpeg",
      isFree: false
    })
  }

  const questionsKy = await Question.create(
    this.state.questionKy
  );

  for (let i = 0; i < 5; i++) {
    await this.state.answers.push({
      position: 'a',
      titleRu: faker.lorem.word(),
      questionId: questions[i]._id,
      isCorrect: false
    })
  }
  for (let i = 0; i < 5; i++) {
    await this.state.answers.push({
      position: 'b',
      titleRu: faker.lorem.word(),
      questionId: questions[i]._id,
      isCorrect: false
    })
  }
  for (let i = 0; i < 5; i++) {
    await this.state.answers.push({
      position: 'c',
      titleRu: faker.lorem.word(),
      questionId: questions[i]._id,
      isCorrect: false
    })
  }
  for (let i = 0; i < 5; i++) {
    await this.state.answers.push({
      position: 'd',
      titleRu: faker.lorem.word(),
      questionId: questions[i]._id,
      isCorrect: false
    })
  }
  for (let i = 0; i < 5; i++) {
    await this.state.answers.push({
      position: 'e',
      titleRu: faker.lorem.word(),
      questionId: questions[i]._id,
      isCorrect: true
    })
  }

  const answers = await Answer.create(
    this.state.answers
  );

  for (let i = 0; i < 5; i++) {
    await this.state.answersKy.push({
      position: 'a',
      titleKy: faker.lorem.word(),
      questionId: questionsKy[i]._id,
      isCorrect: true
    })
  }
  for (let i = 0; i < 5; i++) {
    await this.state.answersKy.push({
      position: 'b',
      titleKy: faker.lorem.word(),
      questionId: questionsKy[i]._id,
      isCorrect: false
    })
  }
  for (let i = 0; i < 5; i++) {
    await this.state.answersKy.push({
      position: 'c',
      titleKy: faker.lorem.word(),
      questionId: questionsKy[i]._id,
      isCorrect: false
    })
  }
  for (let i = 0; i < 5; i++) {
    await this.state.answersKy.push({
      position: 'd',
      titleKy: faker.lorem.word(),
      questionId: questionsKy[i]._id,
      isCorrect: true
    })
  }
  for (let i = 0; i < 5; i++) {
    await this.state.answersKy.push({
      position: 'e',
      titleKy: faker.lorem.word(),
      questionId: questionsKy[i]._id,
      isCorrect: false
    })
  }

  await Answer.create(
    this.state.answersKy
  );

  const tests = await Test.create(
    {subjectId: subjects[0]._id},
    {subjectId: subjects[1]._id},
  );

  for (let i = 0; i < 5; i++) {
    await this.state.test.push({testId: tests[0]._id, questionId: questions[i]._id})
  }
  for (let i = 0; i < 5; i++) {
    await this.state.test.push({testId: tests[1]._id, questionId: questions[i]._id})
  }

  const questionsTests = await TestQuestion.create(
    this.state.test
  );

  for (let i = 0; i < 10; i++) {
    await this.state.passed.push({
      userId: users[0]._id,
      date: faker.date.past(),
      answers: [answers[i]._id],
      result: faker.random.number(250),
      testId: tests[0]._id,
      timer: '01:00:00'
    })
  }
  for (let i = 0; i < 10; i++) {
    await this.state.passed.push({
      userId: users[2]._id,
      date: faker.date.past(),
      answers: [answers[i]._id],
      result: faker.random.number(250),
      testId: tests[1]._id,
      timer: '01:00:00'
    })
  }

  const passedTests = await PassedTest.create(
    this.state.passed
  );


  await connection.close();
};

run().catch(error => {
  console.error('Something went wrong', error);
});
