const config = {
  apiURL: 'http://localhost:8000'
};

switch (process.env.REACT_APP_ENV) {
  case 'test':
    config.apiURL = 'http://localhost:8010';
    break;
  case 'production':
    config.apiURL = 'http://185.14.185.50:8000';
    break;
  default:
}

export default config;
