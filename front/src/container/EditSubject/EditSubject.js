import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {editSubject} from "../../store/action/adminAction";
import SubjectFormHandler from "../../component/FormHandler/SubjectFormHandler";

class EditSubject extends Component {

    render() {
        let editBox = <SubjectFormHandler
            onSubmit={this.props.editSubject}
            subject={this.props.subject}
        />;
        return (
            <Fragment>
                <h2>Редактирование предмета</h2>
                {editBox}
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    subject: state.admin.subject,
});

const mapDispatchToProps = (dispatch) => ({
    editSubject: (data) => dispatch(editSubject(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(EditSubject);
