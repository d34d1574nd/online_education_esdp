import React, {Component, Fragment} from 'react';
import SideNav, {Toggle, Nav, NavItem, NavIcon, NavText} from '@trendmicro/react-sidenav';
import {Route} from 'react-router-dom'
import '@trendmicro/react-sidenav/dist/react-sidenav.css';
import QuestionList from "../../component/QuestionList/QuestionList";
import SubjectList from "../../component/SubjectList/SubjectList";
import AddQuestion from "../AddQuestion/AddQuestion";
import AddSubject from "../AddSubject/AddSubject";
import RoleManagement from "../RoleManagement/RoleManagement";
import EditQuestion from "../EditQuestions/EditQuestions";
import EditSubject from "../EditSubject/EditSubject";
import AdminMainPage from "../../component/AdminMainPage/AdminMainPage";
import AddQuestionKy from "../AddQuestion/AddQuestionKy";
import EditQuestionKy from "../EditQuestions/EditQuestionKy";

class AdminPanel extends Component {
    state = {
        selected: 'home',
        expanded: false
    };

    onToggle = (expanded) => {
        this.setState({expanded: expanded});
    };

    render() {
        return (
            <Route render={({location, history}) => (
                <Fragment>
                    <SideNav
                        style={{height: '100%', marginTop: '56px'}}
                        onSelect={(selected) => {
                            const to = '/admin' + selected;
                            if (location.pathname !== to) {
                                history.push(to);
                            }
                        }}
                        onToggle={this.onToggle}
                    >
                        <Toggle/>
                        <Nav defaultSelected="home">
                            <NavItem eventKey="">
                                <NavIcon>
                                    <i className="fa fa-fw fa-home" style={{fontSize: '1.75em'}}/>
                                </NavIcon>
                                <NavText>
                                    Главная
                                </NavText>
                            </NavItem>
                            <NavItem eventKey="/questions" id="questions">
                                <NavIcon>
                                    <i className="fa fa-fw fa-th-list" style={{fontSize: '1.75em'}}/>
                                </NavIcon>
                                <NavText>
                                    Вопросы
                                </NavText>
                            </NavItem>
                            <NavItem eventKey="/subjects" id="subjects">
                                <NavIcon>
                                    <i className="fa fa-fw fa-book" style={{fontSize: '1.75em'}}/>
                                </NavIcon>
                                <NavText>
                                    Предметы
                                </NavText>
                            </NavItem>
                            <NavItem eventKey="/role" id="users">
                                <NavIcon>
                                    <i className="fa fa-fw fa-user" style={{fontSize: '1.75em'}}/>
                                </NavIcon>
                                <NavText>
                                    Пользователи
                                </NavText>
                            </NavItem>
                        </Nav>
                    </SideNav>
                    <main style={{marginLeft: `${this.state.expanded ? 190 : 0}px`}}>
                        <Route path="/admin" exact component={() => <AdminMainPage/>}/>
                        <Route path="/admin/questions" component={() => <QuestionList/>}/>
                        <Route path="/admin/subjects" component={() => <SubjectList/>}/>
                        <Route path="/admin/newQuestion/ru" component={() => <AddQuestion/>}/>
                        <Route path="/admin/newQuestion/ky" component={() => <AddQuestionKy/>}/>
                        <Route path="/admin/newSubject" component={() => <AddSubject/>}/>
                        <Route path="/admin/role" component={() => <RoleManagement/>}/>
                        <Route path="/admin/question/ru/:id" component={() => <EditQuestion {...this.props}/>}/>
                        <Route path="/admin/question/ky/:id" component={() => <EditQuestionKy {...this.props}/>}/>
                        <Route path="/admin/subject/:id" component={() => <EditSubject/>}/>
                    </main>
                </Fragment>
            )}
            />
        )
    }
}

export default AdminPanel;
