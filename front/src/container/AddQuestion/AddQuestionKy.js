import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {sendQuestion} from "../../store/action/adminAction";
import FormHandler from "../../component/FormHandler/FormHandler";


class AddQuestionKy extends Component {

  render() {
    return (
      <Fragment>
        <h2>Добавить новый вопрос</h2>
        <FormHandler
          onSubmit={this.props.sendQuestion}
          themeText="themeKy"
          titleText="titleKy"
          placeholderTheme="Тема на кыргызском"
          placeholderTitle="Вопрос на кыргызском"
          themeValue="themeKy"
          titleValue="titleKy"
          titleAnswers="answersKy"
          value="titleKy"
        />
      </Fragment>
    );
  }
}


const mapDispatchToProps =(dispatch)=>({
  sendQuestion: data => dispatch(sendQuestion(data))
});

export default connect(null, mapDispatchToProps)(AddQuestionKy);
