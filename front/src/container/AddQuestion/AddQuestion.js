import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {sendQuestion} from "../../store/action/adminAction";
import FormHandler from "../../component/FormHandler/FormHandler";


class AddQuestion extends Component {

    render() {
        return (
            <Fragment>
                <h2>Добавить новый вопрос</h2>
                <FormHandler
                  onSubmit={this.props.sendQuestion}
                  themeText="themeRu"
                  titleText="titleRu"
                  placeholderTheme="Тема на русском"
                  placeholderTitle="Вопрос на русском"
                  themeValue="themeRu"
                  titleValue="titleRu"
                  titleAnswers="answersRu"
                  value="titleRu"
                />
            </Fragment>
        );
    }
}


const mapDispatchToProps =(dispatch)=>({
    sendQuestion: data => dispatch(sendQuestion(data))
});

export default connect(null, mapDispatchToProps)(AddQuestion);
