import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import SubjectFormHandler from "../../component/FormHandler/SubjectFormHandler";
import {createSubject} from "../../store/action/subjectsAction";



class AddSubject extends Component {

    render() {
        let addBox = <SubjectFormHandler
            onSubmit={this.props.createSubject}
        /> ;
        return (
            <Fragment>
                <h2>Добавить новый предмет</h2>
                {addBox}
            </Fragment>
        );
    }
}


const mapDispatchToProps =(dispatch)=>({
    createSubject: subjectData => dispatch(createSubject(subjectData))
});

export default connect(null, mapDispatchToProps)(AddSubject);
