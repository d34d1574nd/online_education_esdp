import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {changeUsers, fetchUsers} from "../../store/action/UserAction";
import {Col, Input, Row, Table} from "reactstrap";


class RoleManagement extends Component {
    componentDidMount() {
        this.props.fetchUsers()
    }

    inputChangeHandler = (event, userId) => {
        this.props.changeUsers(event.target.value, userId)
    };

    render() {
        return (
            <Row form style={{marginTop: '40px'}} sm="12" md={{size: 6, offset: 3}}>
                <Col md={12}>
                    <h2 className="my-3">Список пользователей</h2>
                    <div className="table-container">
                        <Table bordered>
                            <thead>
                            <tr className="d-flex">
                                <th className="col-3">Имя пользователя</th>
                                <th className="col-3">Номер телефона</th>
                                <th className="col-3">Баланс</th>
                                <th className="col-3">Роль</th>
                                <th className="col-3">Распределение ролей</th>
                            </tr>
                            </thead>
                            <tbody>
                            {this.props.users && this.props.users.map(user => (
                                <tr key={user._id} className="d-flex">
                                    {user.phoneNumber === '+996 (555) 55-55-55' ? null :
                                        <Fragment>
                                            <td className="col-3">{user.surname} {user.name}</td>
                                            <td className="col-3">{user.phoneNumber}</td>
                                            <td className="col-3">{user.balance}&nbsp;сом</td>
                                            <td className="col-3">{user.role}</td>
                                            <td className="col-3">
                                                <Input onChange={(event) => this.inputChangeHandler(event, user._id)}
                                                       type="select" name="select" id="roleAdmin">
                                                    <option>Выбор ролей:</option>
                                                    <option id="role" value="admin">Администратор</option>
                                                    <option value="pupil">Ученик</option>
                                                </Input>
                                            </td>
                                        </Fragment>
                                    }
                                </tr>
                            ))}
                            </tbody>
                        </Table>
                    </div>
                </Col>
            </Row>
        );
    }
}

const mapStateToProps = (state) => ({
    users: state.user.users
});

const mapDispatchToProps = dispatch => ({
    fetchUsers: () => dispatch(fetchUsers()),
    changeUsers: (role, id) => dispatch(changeUsers(role, id))
});

export default connect(mapStateToProps, mapDispatchToProps)(RoleManagement);