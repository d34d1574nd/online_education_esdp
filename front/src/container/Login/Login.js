import React, {Component} from 'react';
import {Alert, Button, Col, Form, FormGroup} from "reactstrap";
import {connect} from "react-redux";
import InputMask from "react-input-mask";

import {loginUser} from "../../store/action/UserAction";
import FormElement from "../../component/UI/Form/FormElement";

class Login extends Component {
  state = {
     phoneNumber: '',
     password: ''
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    })
   };

  submitFormHandler = event => {
    event.preventDefault();
    this.props.loginUser({...this.state})
  };
  render() {
    return (
      <div className="formForUser">
          {this.props.error && (
              <Alert color="danger">{this.props.error.error || this.props.error.global}</Alert>
          )}
        <Form onSubmit={this.submitFormHandler}>
          <h2 className="titleUser">Авторизация пользователя</h2>
          <FormElement
            propertyName="phoneNumber"
            title="Логин (номер телефона)"
            type="tel"
            value={this.state.phoneNumber}
            mask="+\9\96 (999) 99-99-99"
            maskChar="_"
            tag={InputMask}
            onChange={this.inputChangeHandler}
            placeholder="Введите номер телефона"
          />
          <FormElement
            propertyName="password"
            title="Пароль"
            type="password"
            value={this.state.password}
            onChange={this.inputChangeHandler}
            placeholder="Введите пароль"
            autoComplete="new-password"
          />
          <FormGroup row>
            <Col sm={{offset: 3, size: 9}}>
              <Button type="submit" color="success">
                Войти
              </Button>
            </Col>
          </FormGroup>
        </Form>
      </div>
     );
   }
}

const mapStateToProps = state => ({
    error: state.user.loginError
});

const mapDispatchToProps = dispatch => ({
    loginUser: userData => dispatch(loginUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
