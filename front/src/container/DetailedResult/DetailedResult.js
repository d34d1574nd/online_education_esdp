import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";

import {detailResult, getPdfResult} from "../../store/action/detailedResultAction";

import "./DetailedResult.css";
import {Button, Col, Row} from "reactstrap";
import MethodThumbnail from "../../component/UI/MethodThumbnail/MethodThumbnail";


class DetailedResult extends Component {
  componentDidMount() {
    this.props.detailResult(this.props.match.params.id)
  };

  createPdf = () => {
    const data = {
      result: this.props.result,
      user: this.props.user
    };
    this.props.getPdfResult(data)
  };

  render() {
    let resultTest = null;
    if (this.props.result && this.props.result.resultTest) {
      resultTest = this.props.result.resultTest.map((item, index) => {
        return <Fragment key={index + 1}>
          <p>{item.question.titleRu}</p>
          <div className="answers">
            <span className='answer'
                  style={item.answers.find(answer => answer.position === "a").isCorrect === true ? {"color": "green"} : item.answers.find(answer => answer.position === "a").titleRu === item.answersTest.titleRu ? {"color": "red"} : null }
            >
              <span>{item.answers.find(answer => answer.position === "a").position}) </span>
              {item.answers.find(answer => answer.position === 'a').titleRu}
            </span>
            <span className='answer'
                  style={item.answers.find(answer => answer.position === "b").isCorrect === true ? {"color": "green"} : item.answers.find(answer => answer.position === "b").titleRu === item.answersTest.titleRu ? {"color": "red"} : null }
            >
              <span>{item.answers.find(answer => answer.position === "b").position}) </span>
              {item.answers.find(answer => answer.position === 'b').titleRu}
            </span>
            <span className='answer'
                  style={item.answers.find(answer => answer.position === "c").isCorrect === true ? {"color": "green"} : item.answers.find(answer => answer.position === "c").titleRu === item.answersTest.titleRu ? {"color": "red"} : null }
            >
              <span>{item.answers.find(answer => answer.position === "c").position}) </span>
              {item.answers.find(answer => answer.position === 'c').titleRu}
            </span>
            <span className='answer'
                  style={item.answers.find(answer => answer.position === "d").isCorrect === true ? {"color": "green"} : item.answers.find(answer => answer.position === "d").titleRu === item.answersTest.titleRu ? {"color": "red"} : null }
            >
              <span>{item.answers.find(answer => answer.position === "d").position}) </span>
              {item.answers.find(answer => answer.position === 'd').titleRu}
            </span>
            <span className='answer'
                  style={item.answers.find(answer => answer.position === "e").isCorrect === true ? {"color": "green"} : (item.answers.find(answer => answer.position === "e").titleRu === item.answersTest.titleRu ? {"color": "red"} : null) }
            >
              <span>{item.answers.find(answer => answer.position === "e").position}) </span>
              {item.answers.find(answer => answer.position === 'e').titleRu}
            </span>
          </div>
          <br/>
          <div>
            <p><strong>Метод решения задачи: </strong></p>
            <MethodThumbnail
              image={item.question.methodSolution}
            />
          </div>
          <hr/>
        </Fragment>
      });
    }

    return (
      <div className="resultTest">
        <div className="export">
          <Button color="primary" outline onClick={this.createPdf}>export <i className="buttonExport far fa-file-pdf"/></Button>
        </div>
        <div className='userTest'>
          <Row>
            <Col>Имя: </Col>
            <Col>{this.props.user.name}</Col>
          </Row>
          <Row>
            <Col>Фамилия: </Col>
            <Col>{this.props.user.surname}</Col>
          </Row>
          <Row>
            <Col>Номер телефона: </Col>
            <Col>{this.props.user.phoneNumber}</Col>
          </Row>
          <Row>
            <Col>Общее время: </Col>
            <Col>{this.props.result.result && this.props.result.result.timer}</Col>
          </Row>
          <Row>
            <Col>Количество баллов: </Col>
            <Col>{this.props.result.result && this.props.result.result.result}</Col>
          </Row>
        </div>
        <div className="legendColor">
          <p className="textColor"><span className="colorGreen">зеленый</span> правильный ответ</p>
          <p className="textColor"><span className="colorRed">крассный</span> не правильный ответ</p>
        </div>
        <h4 className="testText">Подробное описание прохождения теста</h4>
        {resultTest}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user.user,
  result: state.result.questionResult
});

const mapDispatchToProps = dispatch => ({
  detailResult: id => dispatch(detailResult(id)),
  getPdfResult: data => dispatch(getPdfResult(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(DetailedResult);
