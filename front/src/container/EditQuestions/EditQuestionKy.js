import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {editQuestion} from "../../store/action/adminAction";
import FormHandler from "../../component/FormHandler/FormHandler";

class EditQuestion extends Component {

  render() {
    let editBox = <FormHandler
      onSubmit={this.props.editQuestion}
      question={this.props.question}
      themeText="themeKy"
      titleText="titleKy"
      placeholderTheme="Тема на кыргызском"
      placeholderTitle="Вопрос на кыргызском"
      themeValue="themeKy"
      titleValue="titleKy"
      titleAnswers="answersKy"
      value="titleKy"
    />;
    return (
      <Fragment>
        <h2>Редактирование вопроса</h2>
        {editBox}
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  question: state.admin.question,
});

const mapDispatchToProps = (dispatch) => ({
  editQuestion: (data) => dispatch(editQuestion(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(EditQuestion);
