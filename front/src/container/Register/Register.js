import React, {Component} from 'react';
import {Button, Col, Form, FormGroup, InputGroupAddon} from "reactstrap";
import {connect} from "react-redux";
import {NotificationManager} from "react-notifications";
import InputMask from 'react-input-mask';
import Recaptcha from 'react-recaptcha'
import zxcvbn from 'zxcvbn';


import {registerUser} from "../../store/action/UserAction";
import FormElement from "../../component/UI/Form/FormElement";

import './Register.css';


class Register extends Component {
  state = {
    phoneNumber: "",
    name: '',
    surname: '',
    password: '',
    passwordSecond: '',
    hiddenPassword: true,
    verifyUser: false,
    show: false,
    testedResult: ''
  };


  componentDidMount() {
    if (this.props.password) {
      this.setState({password: this.props.password});
    }
  }

  inputChangeHandler = event => {
    const testedResult = zxcvbn(this.state.password);
    this.setState({
      [event.target.name]: event.target.value,
      testedResult: testedResult.score
    })
  };

  toggleShowPassword = () => {
    this.setState({hiddenPassword: !this.state.hiddenPassword})
  };


  callback = () => {
    console.log('Done!!!');
  };

  verifyCallback = (response) => {
    if (response) {
      this.setState({
        verifyUser: true
      })
    }
  };

  submitFormHandler = event => {
    event.preventDefault();
    const regularExpression = /(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])/;
    const passCorrect = regularExpression.test(this.state.password);
    if (passCorrect === false) {
      NotificationManager.error('Пароль должен содержать прописные, заглавные буквы и цифры!');
    } else if (passCorrect === true && this.state.password.length < 8) {
      NotificationManager.error('Пароль должен быть от 8 символов!');
    } else if (passCorrect === true && this.state.password !== this.state.passwordSecond) {
      NotificationManager.error('Пароли не верны !');
    }  // else if (this.state.verifyUser === false) {
    //   NotificationManager.error('Неверная капча');
    // }
    else {
      const user = {
        name: this.state.name,
        phoneNumber: this.state.phoneNumber,
        surname: this.state.surname,
        password: this.state.password
      };
      this.props.registerUser({user})
    }
  };

  ChangePass = () => {
    this.setState({
      show: true,
    })
  };

  ClosePassCorrect = () => {
    this.setState({
      show: false
    })
  };

  PasswordCorrect = result => {
    switch (result) {
      case 0:
        return 'Пустой';
      case 1:
        return 'Пустой';
      case 2:
        return 'Средний';
      case 3:
        return 'Хороший';
      case 4:
        return 'Сильный';
      default:
        return 'Пустой';
    }
  };

  getFieldError = fieldName => {
    return this.props.error && this.props.error.errors && this.props.error.errors[fieldName]
      && (this.props.error.errors[fieldName].message = "Это поле обязательно для заполнения")
  };

  render() {
    return (
      <div className="formForUser">
        <h2 className="titleUser">Регистрация нового пользователя</h2>
        <Form onSubmit={this.submitFormHandler}>
          <FormElement
            propertyName="phoneNumber"
            title="Логин (номер телефона)"
            type="tel"
            value={this.state.phoneNumber}
            mask="+\9\96 (999) 99-99-99"
            error={this.getFieldError('phoneNumber')}
            valid={this.state.phoneNumber !== ''}
            maskChar="_"
            tag={InputMask}
            onChange={this.inputChangeHandler}
            placeholder="Введите номер телефона"
          />

          <FormElement
            propertyName="name"
            title="Имя"
            type="text"
            valid={this.state.name !== ''}
            value={this.state.name}
            onChange={this.inputChangeHandler}
            placeholder="Введите имя"
            error={this.getFieldError("name")}
          />

          <FormElement
            propertyName="surname"
            title="Фамилия"
            type="text"
            valid={this.state.surname !== ''}
            value={this.state.surname}
            onChange={this.inputChangeHandler}
            error={this.getFieldError('surname')}
            placeholder="Введите фамилию"
          />

          <FormElement
            propertyName="password"
            title="Пароль"
            type={this.state.hiddenPassword ? "password" : "text"}
            valid={this.state.password.length >= 8}
            value={this.state.password}
            onClick={this.ChangePass}
            onChange={this.inputChangeHandler}
            error={this.getFieldError('password')}
            placeholder="Введите пароль"
          >
            <InputGroupAddon addonType="prepend">
              <Button color="primary" className="buttonPassword" onClick={this.toggleShowPassword}>
                {this.state.hiddenPassword === false ? <i className="far fa-eye"/> : <i className="far fa-eye-slash"/>}
              </Button>
            </InputGroupAddon>
          </FormElement>

          {this.state.show === true ?
            <FormGroup row>
              <Col sm={{offset: 3, size: 4}}>
                <div className="password-strength-meter">
                  <progress
                    className={`password-strength-meter-progress strength-${this.PasswordCorrect(this.state.testedResult)}`}
                    value={this.state.testedResult}
                    max="4"
                  />
                </div>
              </Col>
              <Col sm={{size: 5}}>
                <label
                  className="password-strength-meter-label"
                >
                {this.state.password && (
                  <>
                    <strong>Сложность пароля : </strong> {this.PasswordCorrect(this.state.testedResult)}
                  </>
                )}
                </label>
              </Col>
            </FormGroup>
          : null}

          <FormElement
            propertyName="passwordSecond"
            type={this.state.hiddenPassword ? "password" : "text"}
            title=""
            onClick={this.ClosePassCorrect}
            value={this.state.passwordSecond}
            valid={this.state.password.length >= 8 && this.state.password === this.state.passwordSecond}
            onChange={this.inputChangeHandler}
            error={this.getFieldError('passwordSecond')}
            placeholder="Введите пароль еще раз"
          />
          <FormGroup row>
            <Col sm={{offset: 3, size: 9}}>
              <Recaptcha
                sitekey="6Ld6JK0UAAAAAC5SDvJUHtByfWUbTJEpc7FaO0Pt"
                hl="ru"
                verifyCallback={this.verifyCallback}
                onloadCallback={this.callback}
              />
            </Col>
          </FormGroup>

          <FormGroup row>

            <Col sm={{offset: 3, size: 9}}>
              <Button type="submit" color="primary">
                Зарегистрироваться
              </Button>

            </Col>
          </FormGroup>

        </Form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  error: state.user.registerError
});


const mapDispatchToProps = dispatch => ({
  registerUser: (userData) => dispatch(registerUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);
