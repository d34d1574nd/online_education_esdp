import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import axios from "../../axios-api";

import {fetchQuestions, sendData, switchToKy, switchToRu} from "../../store/action/testsActions";
import {Button, Col} from "reactstrap";
import Question from "../../component/Question/Question";
import {getUserInfo} from "../../store/action/UserAction";

import './Test.css';

class Test extends Component {
  _interval;

  state = {
    answers: [],
    buttonIsDisabled: true,
    currentPage: 1,
    questionPerPage: 1,
    count: 3599,
    running: false,
    time: 1,
    allTimer: 1
  };

  componentWillMount() {
    this._interval = setInterval(() => {
      const newCount = this.state.count - 1;
      const oldCount = this.state.time + 1;
      const allCount = this.state.allTimer + 1;
      this.setState({
        count: newCount >= 0 ? newCount : 0,
        running: true,
        time: oldCount,
        allTimer: allCount
      });
      localStorage.setItem('time', this.state.count);
      localStorage.setItem('timer', this.state.time);
    }, 1000);
  }

  componentWillUnmount() {
    clearInterval(this._interval)
  }

  format = (time) => {
    let seconds = time % 60;
    let minutes = Math.floor(time / 60);
    let hour = Math.floor(time / (60 * 60));
    hour = hour.toString().length === 1 ? "0" + hour : hour;
    minutes = minutes.toString().length === 1 ? "0" + minutes : minutes;
    seconds = seconds.toString().length === 1 ? "0" + seconds : seconds;
    return hour + ":" + minutes + ':' + seconds;
  };

  handleClickPrev = () => {
    localStorage.removeItem('time');
    localStorage.removeItem('timer');
    this.setState({
      currentPage: this.state.currentPage - 1,
      count: 3599,
      time: 1
    });

  };
  handleClickNext = () => {
    localStorage.removeItem('time');
    localStorage.removeItem('timer');
    this.setState({
      currentPage: this.state.currentPage + 1,
      count: 3599,
      time: 1
    });
  };

  changedRadio = (event) => {
    const answer = this.state.answers.find(answer => answer.question === event.target.name);

    if (!answer) {
      this.setState({answers: [...this.state.answers, {question: event.target.name, answer: event.target.id}]});
    }

    if (answer) {
      const answers = [...this.state.answers];
      const answerIndex = answers.indexOf(answer);
      const newAnswers = answers.filter(function (value, index, arr) {
        return value !== answers[answerIndex];
      });
      newAnswers.push({question: event.target.name, answer: event.target.id});
      this.setState({answers: newAnswers});
    }

    if (this.state.answers.length === this.props.questions.length) {
      this.setState({buttonIsDisabled: false})
    }
  };

  submitFormHandler = event => {
    event.preventDefault();
    const data = {
      answers: [],
      user: this.props.user._id,
      testId: this.props.testId,
      timer: this.format(this.state.allTimer)
    };
    this.state.answers.map(item => {
      return data.answers.push(item.answer);
    });

    this.props.onSubmit(data);
    this.props.onGetUserInfo(this.props.user._id);
  };

  componentDidMount() {
    this.props.onGetUserInfo(this.props.user._id);

    let testId;
    axios.get('/tests').then(response => {   // временное решение для получения теста
      testId = response.data[0]._id;
      this.props.fetchQuestions(testId);
    });
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevState.answers.length !== this.state.answers.length) {
      if (this.state.answers.length === this.props.questions.length) {
        this.setState({buttonIsDisabled: false});
      }
    }
  }

  render() {
    let currentQuestion;
    if (this.props.questions) {
      const indexOfLastTodo = this.state.currentPage * this.state.questionPerPage;
      const indexOfFirstTodo = indexOfLastTodo - this.state.questionPerPage;
      currentQuestion = this.props.questions.slice(indexOfFirstTodo, indexOfLastTodo);
    }
    let questions;

    (this.props.questions) ? (
      questions = currentQuestion.map((item, index) => {
        return <Question
          form={`form${index + 1}`}
          changed={this.changedRadio}
          key={item.question._id}
          question={(this.props.language === 'ru') ? item.question.questionId.titleRu : item.question.questionId.titleKy}
          answerA={(this.props.language === 'ru') ? (item.answers.find(answer => answer.position === 'a').titleRu) : (item.answers.find(answer => answer.position === 'a').titleKy)}
          answerB={(this.props.language === 'ru') ? (item.answers.find(answer => answer.position === 'b').titleRu) : (item.answers.find(answer => answer.position === 'b').titleKy)}
          answerC={(this.props.language === 'ru') ? (item.answers.find(answer => answer.position === 'c').titleRu) : (item.answers.find(answer => answer.position === 'c').titleKy)}
          answerD={(this.props.language === 'ru') ? (item.answers.find(answer => answer.position === 'd').titleRu) : (item.answers.find(answer => answer.position === 'd').titleKy)}
          answerE={(this.props.language === 'ru') ? (item.answers.find(answer => answer.position === 'e').titleRu) : (item.answers.find(answer => answer.position === 'e').titleKy)}
          answerAid={item.answers.find(answer => answer.position === 'a')._id}
          answerBid={item.answers.find(answer => answer.position === 'b')._id}
          answerCid={item.answers.find(answer => answer.position === 'c')._id}
          answerDid={item.answers.find(answer => answer.position === 'd')._id}
          answerEid={item.answers.find(answer => answer.position === 'e')._id}
          questionId={item.question._id}
        />
      })
    ) : (
      questions = null
    );

    const content = <div>
      <Col>
        <div>Хотите переключить язык?</div>
        <button onClick={this.props.switchToKy}>Кыргызча</button>
        <button onClick={this.props.switchToRu}>Русский</button>
      </Col>
      <p>Имя: {this.props.user.name}</p>
      <p>Фамилия: {this.props.user.surname}</p>
      <p>Телефон: {this.props.user.phoneNumber}</p>
      <div className="timer">
        <div className='timerDop'>
          <i className="far fa-clock clocks"/>
        </div>
        <div className='timerDop'>
          <span className="textTimer">Оставшееся время:</span>
        </div>
        <div className='timerDop'>
          <h3>{this.format(localStorage.getItem('time'))}</h3>
        </div>
      </div>
      <h1>Тест</h1>
      <div className="questions">
        <nav aria-label="Навигация по страницам сайта">
          <ul className="pager">
            <li className="button previous" id={this.state.currentPage}
                onClick={this.state.currentPage === 1 ? null : this.handleClickPrev}>
              <span className="link"><span aria-hidden="true">←</span> Предыдущий предмет</span>
            </li>
            <li className="button next" id={this.state.currentPage}
                onClick={this.state.currentPage === 5 ? null : this.handleClickNext}>
              <span className="link">Следующий предмет <span aria-hidden="true">→</span></span>
            </li>
          </ul>
        </nav>
        {questions}
        <Button onClick={this.submitFormHandler} disabled={this.state.buttonIsDisabled}>Отправить</Button>
      </div>
    </div>;


    return (
      <Fragment>
        {content}
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user.user,
  language: state.tests.language,
  questions: state.tests.questions,
  testId: state.tests.testId,
  timer: state.tests.timer
});

const mapDispatchToProps = dispatch => ({
  fetchQuestions: id => dispatch(fetchQuestions(id)),
  switchToKy: () => dispatch(switchToKy()),
  switchToRu: () => dispatch(switchToRu()),
  onSubmit: data => dispatch(sendData(data)),
  onGetUserInfo: id => dispatch(getUserInfo(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(Test);
