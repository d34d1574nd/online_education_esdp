import React, {Component} from 'react';
import {connect} from "react-redux";
import {Button, Col, Modal, ModalBody, ModalFooter, ModalHeader, Row, Container} from "reactstrap";
import {paymentFetch, paymentStatus} from "../../../store/action/UserAction";
import SubjectsModalForm from "../../../component/UI/Modal/SubjectsModal";
import {adminFetchSubjects} from "../../../store/action/adminAction";
import {getQuestions} from "../../../store/action/testsActions";

class Registered extends Component {
    state = {
        modal: false,
        qrModal: false,
        subjects: []
    };

    displayModal = () => {
        this.props.paymentFetch();
        this.setState({qrModal: true})
    };


    keepModal = () => {
        this.props.paymentStatus();
        this.setState({qrModal: false})
    };

    showModal = () => {
        this.props.fetchSubjects();
        this.setState({modal: true})
    };

    hideModal = () => {
        this.setState({modal: false})
    };

    tests = () => {
        this.props.getQuestions(this.state.subjects);
    };

    changeCheckbox = (event) => {
        const subjects = this.state.subjects;
        let index;
        if (event.target.checked) {
            subjects.push(event.target.value)
        } else {
            index = subjects.indexOf(event.target.value);
            subjects.splice(index, 1)
        }
        this.setState(subjects)
    };

    render() {
        return (
            <Container>
                <Row style={{marginTop: '50px'}}>
                    <Col md={{size: 6, offset: 5}}>
                        <Button
                            color="primary"
                            onClick={() => this.displayModal()}
                        >
                            Оплатить
                        </Button>
                    </Col>
                    <Col md={{size: 6, offset: 5}}>
                        <Button
                            color="primary"
                            onClick={() => this.showModal()}
                        >
                            Пробное тестирование
                        </Button>
                    </Col>
                </Row>
                <SubjectsModalForm
                    toggleModal={this.hideModal}
                    tests={this.tests}
                    changeCheckbox={this.changeCheckbox}
                    subjects={this.props.subjects}
                    modal={this.state.modal}/>

                <Modal isOpen={this.state.qrModal} toggle={this.keepModal}>
                    <ModalHeader>Отсканируйте код</ModalHeader>
                    <ModalBody>
                        <img src={this.props.payment} alt="QRCode"/>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={this.keepModal}>Закрыть</Button>
                    </ModalFooter>
                </Modal>
            </Container>
        );
    }
}

const mapStateToProps = state => ({
    payment: state.user.payment,
    subjects: state.admin.subjects
});

const mapDispatchToProps = dispatch => ({
    paymentFetch: () => dispatch(paymentFetch()),
    paymentStatus: () => dispatch(paymentStatus()),
    fetchSubjects: () => dispatch(adminFetchSubjects()),
    getQuestions: (data) => dispatch(getQuestions(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(Registered);
