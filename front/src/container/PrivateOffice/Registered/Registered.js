import React, {Component} from 'react';
import {connect} from "react-redux";
import {Button, Col, Container, Modal, ModalBody, ModalFooter, ModalHeader, Row} from "reactstrap";
import {CartesianGrid, Line, LineChart, Tooltip, XAxis} from "recharts";
import {getUserInfo} from "../../../store/action/UserAction";
import {getPassedTests, getQuestions} from "../../../store/action/testsActions";
import './Registered.css';
import Table from "../../../component/Table/Table";
import Spinner from "../../../component/UI/Spinner/Spinner";
import {adminFetchSubjects} from "../../../store/action/adminAction";
import SubjectsModalForm from "../../../component/UI/Modal/SubjectsModal";

class Registered extends Component {

    state = {
        modal: false,
        subjects: []
    };

    showModal = () => {
        this.props.fetchSubjects();
        this.setState({modal: true})
    };

    hideModal = () => {
        this.setState({modal: false})
    };

    tests = () => {
        this.props.getQuestions(this.state.subjects);
    };

    componentDidMount() {
        this.props.onGetPassedTests(this.props.user._id);
    }

    changeCheckbox = (event) => {
        const subjects = this.state.subjects;
        let index;
        if (event.target.checked) {
            subjects.push(event.target.value)
        } else {
            index = subjects.indexOf(event.target.value);
            subjects.splice(index, 1)
        }
        this.setState(subjects)
    };

    render() {
        if (this.props.loading) {
            return <Spinner/>;
        }

        let data = [];
        if (this.props.passedTests) {
            this.props.passedTests.map(test => {
                const date = new Date(test.date);
                let zero;
                (date.getMonth() > 8) ? zero = '' : zero = '0';
                return data.push({
                    date: (date.getDate() + '.' + zero + (date.getMonth() + 1) + '.' + date.getFullYear()),
                    point: test.result
                })
            })
        }

        return (
            <Container>
                <Row style={{marginTop: '40px'}}>
                    <Col md={{size: 6}} style={{margin: '0 auto'}}>
                        <Button color="primary" size="lg" onClick={() => this.showModal()}
                                style={{margin: '0 auto', display: 'block'}}>
                            Начать тест
                        </Button>
                    </Col>
                </Row>
                <SubjectsModalForm
                    toggleModal={this.hideModal}
                    tests={this.tests}
                    changeCheckbox={this.changeCheckbox}
                    subjects={this.props.subjects}
                    modal={this.state.modal}
                />

                <h2 className="Results-Heading">Результаты</h2>
                <Row style={{marginTop: '10px'}} className="Box">


                    <div className="Table">
                        {this.props.passedTests ? <Table data={this.props.passedTests}/> : null}
                    </div>

                    <div className="Chart-Box">
                        <span id="Points-Label">Баллы</span>
                        <span id="Date-Label">Даты прохождения тестов</span>
                        <div className="Chart">
                            <LineChart
                                width={(this.props.passedTests && this.props.passedTests.length > 5) ? 800 : 508}
                                height={400} data={data}
                            >
                                <CartesianGrid stroke="#ccc" strokeDasharray="1 1"/>
                                <XAxis dataKey="date" height={80} padding={{left: 50}}/>
                                <Tooltip stroke="red"/>
                                <Line type="monotone" dot={{stroke: 'blue', strokeWidth: 3}} dataKey="point"
                                      stroke="#8884d8"/>
                            </LineChart>
                        </div>
                    </div>
                </Row>
                <div>
                </div>
            </Container>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user.user,
    passedTests: state.tests.passedTests,
    loading: state.tests.loading,
    subjects: state.admin.subjects
});

const mapDispatchToProps = dispatch => ({
    onGetUserInfo: id => dispatch(getUserInfo(id)),
    onGetPassedTests: id => dispatch(getPassedTests(id)),
    fetchSubjects: () => dispatch(adminFetchSubjects()),
    getQuestions: (data) => dispatch(getQuestions(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(Registered);
