import {NotificationManager} from "react-notifications";
import {
    FETCH_QUESTIONS_SUCCESS,
    GET_PASSED_TESTS_SUCCESS,
    LOADING_PASSED_TEST,
    SWITCH_TO_KY,
    SWITCH_TO_RU,
} from "../actionTypes";
import axios from "../../axios-api";
import {push} from 'connected-react-router';
import {getUserInfo} from "./UserAction";

const fetchQuestionsSuccess = data => ({type: FETCH_QUESTIONS_SUCCESS, data});
const getPassedTestsSuccess = data => ({type: GET_PASSED_TESTS_SUCCESS, data});
const loadingPassedTest = cancel => ({type: LOADING_PASSED_TEST, cancel});

export const switchToKy = () => {
    return dispatch => {
        dispatch({type: SWITCH_TO_KY});
        NotificationManager.success('Вы переключились на кыргызский язык');
    }
};

export const switchToRu = () => {
    return dispatch => {
        dispatch({type: SWITCH_TO_RU});
        NotificationManager.success('Вы переключились на русский язык');
    }
};

export const fetchQuestions = id => {
    return dispatch => {
        return axios.get('/tests/' + id).then(
            response => {
                dispatch(fetchQuestionsSuccess(response.data));
            },
            () => {
                //
            }
        )
    }
};
export const getQuestions = (data) => {
    return dispatch => {
        let url = '/tests?';
        for (let i = 0; i < data.length; i++) {
            if (data) {
                url += `subjectId${[i]}=${data[i]}&&`
            }
        }
        return axios.get(url).then(
            response => {
                dispatch(fetchQuestionsSuccess(response.data));
                dispatch(push('/test'))
            }
        )
    }
};


export const sendData = data => {
    return (dispatch, getState) => {
        return axios.post('/passedTest', data).then(
            response => {
                NotificationManager.success('Отправлено!');
                dispatch(getUserInfo(getState().user.user._id));
                dispatch(push('/'));
            },
            () => {
                //
            }
        ).finally(() => dispatch(loadingPassedTest(true)))
    }
};

export const getPassedTests = id => {
    return dispatch => {
        return axios.get('/passedTest/' + id).then(
            response => {
                dispatch(getPassedTestsSuccess(response.data))
            },
            () => {
                //
            }
        ).finally(() => dispatch(loadingPassedTest(false)))
    }
};

