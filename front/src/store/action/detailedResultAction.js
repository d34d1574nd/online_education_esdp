import axios from "../../axios-api";
import {DETAILED_RESULT_SUCCESS} from "../actionTypes";
import {saveAs} from "file-saver";
import {NotificationManager} from "react-notifications";

const detailedResult = result => ({type: DETAILED_RESULT_SUCCESS, result});

export const detailResult = (id) => {
  return dispatch => {
    return axios.get(`/detailResult/${id}`).then(
      response => {
        dispatch(detailedResult(response.data));
      }
    )
  }
};

export const getPdfResult = data => {
  return dispatch => {
    return axios.post('/result/create-pdf', data)
      .then(() => axios.get('/result/fetch-pdf', { responseType: 'blob' }))
      .then((res) => {
        const pdfBlob = new Blob([res.data], { type: 'application/pdf' });
        NotificationManager.success("Pdf файл прохождения теста готов");
        saveAs(pdfBlob, `${data.user.phoneNumber}.pdf`);
      })
  }
};
