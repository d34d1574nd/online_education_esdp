import axios from '../../axios-api';
import {push} from "connected-react-router";
import {NotificationManager} from "react-notifications";


export const FETCH_SUBJECTS_SUCCESS = 'FETCH_SUBJECTS_SUCCESS';
export const CREATE_SUBJECTS_SUCCESS = 'CREATE_SUBJECTS_SUCCESS';
export const SUBJECT_CREATE_FAILURE = 'SUBJECT_CREATE_FAILURE';
export  const TOGGLE_MODAL = 'TOGGLE_MODAL';
export  const CLEAR_MODAL = 'CLEAR_MODAL';

export const fetchSubjectsSuccess = subjects => ({type: FETCH_SUBJECTS_SUCCESS, subjects});
export const createSubjectsSuccess = () => ({type: CREATE_SUBJECTS_SUCCESS});
export const subjectCreateFailure = error => ({type: SUBJECT_CREATE_FAILURE, error});
export const toggleModalSuccess = () => ({type: TOGGLE_MODAL});
export const clearModalSuccess = () => ({type: CLEAR_MODAL});

export const toggleModal =()=>{
    return dispatch =>{
        dispatch(toggleModalSuccess());
    }
};

export const clearModal = () =>{
    return dispatch =>{
        dispatch(clearModalSuccess())
    }
};


export const fetchSubjects = () => {
    return dispatch => {
        return axios.get('/subjects').then(
            response => {
                dispatch(fetchSubjectsSuccess(response.data))
            }
        );
    };
};

export const createSubject = subjectData => {
    return (dispatch) => {
            return axios.post('/subjects', subjectData).then(
                response => {
                    dispatch(createSubjectsSuccess());
                    NotificationManager.success('Предмет упешно добавлен!');
                    dispatch(push('/admin/subjects'));
                },
                error => {
                    dispatch(push('/admin/newSubject'));
                    if (error.response && error.response.data) {
                        dispatch(subjectCreateFailure(error.response.data))
                    } else {
                        dispatch(subjectCreateFailure({global: 'Нет соединения'}))
                    }
                }
            );
        }
};


export const createSubjectQuestions = subjectData => {
    return (dispatch) => {
        return axios.post('/subjects', subjectData).then(
            response => {
                if(response.data){
                    dispatch(toggleModal());
                }
                dispatch(createSubjectsSuccess());
                dispatch(fetchSubjects());
                NotificationManager.success('Предмет упешно добавлен!');
            },
            error => {
                if (error.response && error.response.data) {
                    dispatch(subjectCreateFailure(error.response.data))
                } else {
                    dispatch(subjectCreateFailure({global: 'Нет соединения'}))
                }
            }
        );
    }
};
