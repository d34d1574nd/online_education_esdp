import axios from "../../axios-api";
import {push} from "connected-react-router";
import {
    SEND_QUESTIONS_SUCCESS,
    SEND_QUESTIONS_FAILURE,
    ADMIN_FETCH_QUESTIONS_SUCCESS,
    ADMIN_FETCH_QUESTIONS_FAILURE,
    ADMIN_FETCH_SUBJECTS_SUCCESS,
    ADMIN_FETCH_SUBJECTS_FAILURE,
    DELETE_QUESTIONS_SUCCESS,
    ADMIN_FETCH_QUESTION_SUCCESS, DELETE_SUBJECT_SUCCESS, ADMIN_FETCH_SUBJECT_SUCCESS,
} from "../actionTypes";
import {NotificationManager} from 'react-notifications';

const sendQuestionSuccess = () => ({type: SEND_QUESTIONS_SUCCESS});
const deleteQuestionSuccess = () => ({type: DELETE_QUESTIONS_SUCCESS});
const deleteSubjectSuccess = () => ({type: DELETE_SUBJECT_SUCCESS});
const sendQuestionFailure = error => ({type: SEND_QUESTIONS_FAILURE, error});

export const adminFetchQuestionsSuccess = questions => {
    return {type: ADMIN_FETCH_QUESTIONS_SUCCESS, questions};
};

export const adminFetchQuestionSuccess = question => {
    return {type: ADMIN_FETCH_QUESTION_SUCCESS, question};
};

export const adminFetchQuestionsFailure = error => {
    return {type: ADMIN_FETCH_QUESTIONS_FAILURE, error};
};

export const adminFetchSubjectSuccess = subject => {
    return {type: ADMIN_FETCH_SUBJECT_SUCCESS, subject};
};

export const adminFetchSubjectsSuccess = subjects => {
    return {type: ADMIN_FETCH_SUBJECTS_SUCCESS, subjects};
};

export const adminFetchSubjectsFailure = error => {
    return {type: ADMIN_FETCH_SUBJECTS_FAILURE, error};
};

export const sendQuestion = (data) => {
    return dispatch => {
        return axios.post('/admin', data).then(
            () => {
                dispatch(sendQuestionSuccess());
                NotificationManager.success('Добавлено успешно');
                dispatch(push('/admin/questions'));
            },
            error => {
                if (error.response && error.response.data) {
                    NotificationManager.error(error.response.data.error);
                    dispatch(sendQuestionFailure(error.response.data));
                } else {
                    dispatch(sendQuestionFailure({global: 'Нет соединения'}))
                }
            }
        )
    }
};

export const adminFetchQuestions = () => {
    return dispatch => {
        return axios.get('/admin/questions').then(
            response => dispatch(adminFetchQuestionsSuccess(response.data)),
            error => dispatch(adminFetchQuestionsFailure(error))
        );
    };
};

export const adminFetchSubjects = () => {
    return dispatch => {
        return axios.get('/admin/subjects').then(
            response => dispatch(adminFetchSubjectsSuccess(response.data)),
            error => dispatch(adminFetchSubjectsFailure(error))
        );
    };
};

export const getQuestion = (id) => {
    return dispatch => {
        return axios.get(`/admin/questions/${id}`).then(
            response => {
                dispatch(adminFetchQuestionSuccess(response.data));
                if (response.data.titleRu) {
                  dispatch(push('/admin/question/ru/' + id));
                } else {
                dispatch(push('/admin/question/ky/' + id));
              }

            }
        );
    };
};

export const getSubject = (id) => {
    return dispatch => {
        return axios.get(`/admin/subjects/${id}`).then(
            response => {
                dispatch(adminFetchSubjectSuccess(response.data));
                dispatch(push('/admin/subject/' + id));
            }
        );
    };
};

export const editQuestion = (data) => {
    return (dispatch, getState) => {
        const id = getState().admin.question.id;
        return axios.patch('/admin/questions/' + id, data).then(
            response => {
                dispatch(adminFetchQuestions());
                dispatch(push('/admin/questions'));
                NotificationManager.success('Успешно изменено');
            }
        )
    }
};

export const editSubject = (data) => {
    return (dispatch, getState) => {
        const id = getState().admin.subject.id;
        return axios.patch('/admin/subjects/' + id, data).then(
            response => {
                dispatch(adminFetchSubjects());
                dispatch(push('/admin/subjects'));
                NotificationManager.success('Успешно изменено');
            }
        )
    }
};

export const deleteQuestion = id => {
    return dispatch => {
        return axios.delete('/admin/questions/' + id).then(
            () => {
                dispatch(deleteQuestionSuccess());
                NotificationManager.success('Вопрос удален успешно');
                dispatch(adminFetchQuestions())
            },
            error => {
                dispatch(adminFetchQuestionsFailure(error));
                NotificationManager.error("Удаление невозможно");
            }
        )
    }
};

export const deleteSubject = id => {
    return dispatch => {
        return axios.delete('/admin/subjects/' + id).then(
            () => {
                dispatch(deleteSubjectSuccess());
                NotificationManager.success('Предмет удален успешно');
                dispatch(adminFetchSubjects())
            },
            error => {
                dispatch(adminFetchSubjectsSuccess(error));
                NotificationManager.error("Удаление невозможно");
            }
        )
    }
};
