import {NotificationManager} from 'react-notifications';
import {push} from 'connected-react-router'

import axios from '../../axios-api';

export const REGISTER_USER_SUCCESS = 'REGISTER_USER_SUCCESS';
export const REGISTER_USER_FAILURE = 'REGISTER_USER_FAILURE';

export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const LOGIN_USER_FAILURE = 'LOGIN_USER_FAILURE';

export const GET_USER_INFO_SUCCESS = 'GET_USER_INFO_SUCCESS';

export const LOGOUT_USER = 'LOGOUT_USER';

export const FETCH_USERS_SUCCESS = 'FETCH_USERS_SUCCESS';

export const CHANGE_USERS_ROLE = 'CHANGE_USERS_ROLE';

export const GET_PAYMENT_SUCCESS = 'GET_PAYMENT_SUCCESS';
export const GET_PAYMENT_STATUS_SUCCESS = 'GET_PAYMENT_STATUS_SUCCESS';

export const fetchUsersSuccess = (users) => ({type: FETCH_USERS_SUCCESS,users});

export const changeUsersRole = () => ({type: CHANGE_USERS_ROLE});

export const registerUserSuccess = user => ({type: REGISTER_USER_SUCCESS, user});
export const registerUserFailure = error => ({type: REGISTER_USER_FAILURE, error});

export const loginUserSuccess = user => ({type: LOGIN_USER_SUCCESS, user});
export const loginUserFailure = error => ({type: LOGIN_USER_FAILURE, error});

export const getUserInfoSuccess = data => ({type: GET_USER_INFO_SUCCESS, data});

export const getPaymentSuccess = payment => ({type: GET_PAYMENT_SUCCESS, payment});
export const getPaymentStatusSuccess = paymentStatus => ({type: GET_PAYMENT_STATUS_SUCCESS, paymentStatus});

export const logoutUser = () => {
    return dispatch => {
        return axios.delete('/users/sessions').then(
            () => {
                dispatch({type: LOGOUT_USER});
                NotificationManager.success('Вы вышли из системы!');
                dispatch(push('/register'))
            },
            () => {
                NotificationManager.error('Не возможно авторизоваться, попробуйте позже')
            }
        )
    }
};

export const registerUser = userData => {
    return dispatch => {
        return axios.post('/users', userData.user).then(
            response => {
                dispatch(registerUserSuccess(response.data.user));
                NotificationManager.success('Регистрация завершена');
                dispatch(push('/'))
            },
            error => {
                if (error.response && error.response.data) {
                    dispatch(registerUserFailure(error.response.data))
                } else {
                    dispatch(registerUserFailure({global: 'Нет соединения'}))
                }
            }
        )
    }
};

export const loginUser = userData => {
    return dispatch => {
        return axios.post('/users/sessions', userData).then(
            response => {
                dispatch(loginUserSuccess(response.data.user));
                NotificationManager.success('Вы успешно вошли в систему!');
                if (response.data.user && response.data.user.role === 'admin') {
                    dispatch(push('/admin'))
                } else if(response.data.user && response.data.user.paid === false) {
                    dispatch(push('/'))
                } else {
                    dispatch(push('/'))
                }
            },
            error => {
                if (error.response && error.response.data) {
                    NotificationManager.error('Нет такого пользователя! Пожалуйста зарегистрируйтесь');
                    dispatch(loginUserFailure(error.response.data))
                } else {
                    dispatch(loginUserFailure({global: 'Нет соединения'}))
                }
            }
        )
    }
};

export const getUserInfo = id => {
    return dispatch => {
        return axios.get('/users/' + id).then(
            response => {
                dispatch(getUserInfoSuccess(response.data));
            },
            error => {
                //
            }
        )
    }
};


export const fetchUsers = () => {
    return dispatch => {
        return axios.get('/users').then(
            response => {
                dispatch(fetchUsersSuccess(response.data))
            }
        )
    }
};


export const changeUsers = (role, id) => {
    return dispatch => {
      axios.put('/users/change_role/' + id , {role}).then(
          () => {
              dispatch(changeUsersRole());
              dispatch(fetchUsers());
              NotificationManager.success('Роль изменена успешно');
          }
      )
  }
};

export const paymentFetch = () => {
    return dispatch => {
        axios.post('/payment').then(
            response =>{
                dispatch(getPaymentSuccess(response.data))
            }
        )
    }
};
export const paymentStatus = () => {
    return (dispatch, getState) => {
        axios.get('/payment').then(
            response =>{
             console.log(response.data);
                localStorage.removeItem('user');
                getState().user.user=null;
                if(response.data && response.data.paid === true){
                    dispatch(push('/login'))
                    NotificationManager.success('Залогиньтесь заново!');
                } else if (response.data && response.data.message){
                    NotificationManager.error(response.data.message);
                }

            }
        );

    }
};


