import {
    ADMIN_FETCH_QUESTION_SUCCESS,
    ADMIN_FETCH_QUESTIONS_FAILURE,
    ADMIN_FETCH_QUESTIONS_SUCCESS,
    ADMIN_FETCH_SUBJECT_SUCCESS,
    ADMIN_FETCH_SUBJECTS_FAILURE,
    ADMIN_FETCH_SUBJECTS_SUCCESS,
    SEND_QUESTIONS_FAILURE
} from "../actionTypes";


const initialState = {
    error: null,
    questions: [],
    question: null,
    subjects: [],
    subject: null,
};

const adminReducer = (state = initialState, action) => {
    switch (action.type) {
        case SEND_QUESTIONS_FAILURE:
            return {
                ...state,
                error: action.error
            };
        case ADMIN_FETCH_QUESTIONS_SUCCESS:
            return {
                ...state,
                questions: action.questions
            };
        case ADMIN_FETCH_QUESTION_SUCCESS:
            return {
                ...state,
                question: action.question
            };
        case ADMIN_FETCH_SUBJECT_SUCCESS:
            return {
                ...state,
                subject: action.subject
            };
        case ADMIN_FETCH_QUESTIONS_FAILURE:
            return {
                ...state,
                error: action.error
            };
        case ADMIN_FETCH_SUBJECTS_SUCCESS:
            return {
                ...state,
                subjects: action.subjects
            };
        case ADMIN_FETCH_SUBJECTS_FAILURE:
            return {
                ...state,
                error: action.error
            };
        default:
            return state;
    }
};
export default adminReducer;
