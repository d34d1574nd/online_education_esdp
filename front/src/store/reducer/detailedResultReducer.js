import {DETAILED_RESULT_SUCCESS} from "../actionTypes";

const initialState = {
  questionResult: []
};

const detailedResultReducer = (state = initialState, action) => {
  switch (action.type) {
    case DETAILED_RESULT_SUCCESS:
      return {
        ...state,
        questionResult: action.result
      };
    default:
      return state;
  }
};
export default detailedResultReducer;
