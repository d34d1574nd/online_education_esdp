import {
    FETCH_USERS_SUCCESS, GET_PAYMENT_STATUS_SUCCESS, GET_PAYMENT_SUCCESS,
    GET_USER_INFO_SUCCESS,
    LOGIN_USER_FAILURE,
    LOGIN_USER_SUCCESS,
    LOGOUT_USER,
    REGISTER_USER_FAILURE,
    REGISTER_USER_SUCCESS
} from "../action/UserAction";

const initialState = {
    registerError: null,
    loginError: null,
    user: null,
    users: [],
    payment: null,
    paymentStatus: null
};

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case REGISTER_USER_SUCCESS:
            return {...state, registerError: null, user: action.user};
        case REGISTER_USER_FAILURE:
            return {...state, registerError: action.error};
        case LOGOUT_USER:
            return {...state, user: null};
        case LOGIN_USER_SUCCESS:
            return {...state, user: action.user, loginError: null};
        case LOGIN_USER_FAILURE:
            return {...state, loginError: action.error};
        case GET_USER_INFO_SUCCESS:
            return {...state, user: action.data};
        case FETCH_USERS_SUCCESS:
            return {...state, users: action.users};
        case GET_PAYMENT_SUCCESS:
            return {...state, payment: action.payment};
        case GET_PAYMENT_STATUS_SUCCESS:
            return {...state, paymentStatus: action.paymentStatus};
        default:
            return state
    }
};

export default userReducer;
