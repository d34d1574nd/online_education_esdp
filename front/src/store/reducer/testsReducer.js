import {
    FETCH_QUESTIONS_SUCCESS,
    GET_PASSED_TESTS_SUCCESS,
    LOADING_PASSED_TEST,
    SWITCH_TO_KY,
    SWITCH_TO_RU, TIMER
} from "../actionTypes";

const initialState = {
    language: 'ru',
    questions: null,
    testId: '',
    passedTests: null,
    loading: true
};

const testsReducer = (state = initialState, action) => {
    switch (action.type) {
        case SWITCH_TO_KY:
            return {...state, language: 'ky'};
        case SWITCH_TO_RU:
            return {...state, language: 'ru'};
        case FETCH_QUESTIONS_SUCCESS:
            return {...state, questions: action.data.test, testId: action.data.testId};
        case GET_PASSED_TESTS_SUCCESS:
            return {...state, passedTests: action.data};
        case LOADING_PASSED_TEST:
            return {...state, loading: action.cancel};
        case TIMER:
            return {...state, timer: action.timer};
        default:
            return state
    }
};

export default testsReducer;
