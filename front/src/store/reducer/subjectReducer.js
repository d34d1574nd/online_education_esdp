import {FETCH_SUBJECTS_SUCCESS} from "../actionTypes";
import {CLEAR_MODAL, SUBJECT_CREATE_FAILURE, TOGGLE_MODAL} from "../action/subjectsAction";


const initialState = {
    subjects: [],
    createError: null,
    modal: false
};

const subjectsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_SUBJECTS_SUCCESS:
            return {...state, subjects: action.subjects, createError: null};
        case SUBJECT_CREATE_FAILURE:
            return {...state, createError: action.error, modal: true};
        case TOGGLE_MODAL:
            return {...state, modal: !state.modal};
        case CLEAR_MODAL:
            return{ ...state, createError: null};

        default:
            return state;
    }
};

export default subjectsReducer;
