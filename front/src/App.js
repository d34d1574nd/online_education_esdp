import React, {Component, Fragment} from 'react';
import {NotificationContainer} from "react-notifications";
import {connect} from "react-redux";
import {Container} from "reactstrap";

import Toolbar from "./component/UI/Toolbar/Toolbar";
import {logoutUser} from "./store/action/UserAction";
import Routes from "./Routes";

import './App.css';


class App extends Component {
  render() {
    return (
        <Fragment>
          <NotificationContainer/>
          <Toolbar user={this.props.user} logout={this.props.logoutUser} />
          <Container>
            <Routes user={this.props.user}/>
          </Container>
        </Fragment>
    );
  }
}

const mapStateToProps = state => ({
    user: state.user.user,
    language: state.tests.language
});

const mapDispatchToProps = dispatch => ({
    logoutUser: () => dispatch(logoutUser())
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
