import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";

import Register from "./container/Register/Register";
import Login from "./container/Login/Login";
import Registered from "./container/PrivateOffice/Registered/Registered";
import Unregistered from "./container/PrivateOffice/Unregistered/Unregistered";
import Test from "./container/Test/Test";
import AdminPanel from "./container/AdminPanel/AdminPanel";
import DetailedResult from "./container/DetailedResult/DetailedResult";

const ProtectedRoute = ({isAllowed, ...props}) => (
    isAllowed ? <Route {...props} /> : <Redirect to={"/login"}/>
);

const redirectUser = (user) => {
    if (user.paid) {
        return <Route path="/" exact component={Registered}/>
    }
    return <Route path="/" exact component={Unregistered}/>
};

const Routes = ({user}) => {
    return (
        <Switch>
            {user
                ? redirectUser(user)
                : <Route path="/register" exact component={Register}/>
            }
            <Route path="/login" exact component={Login}/>
            <Route path="/test" exact component={Test}/>
            <Route path="/detail_result/:id" exact component={DetailedResult}/>
            <ProtectedRoute isAllowed={user && user.role === 'admin'} path="/admin"
                            component={AdminPanel}>
            </ProtectedRoute>
        </Switch>
    );
};

export default Routes;
