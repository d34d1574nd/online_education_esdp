import React, {Component} from 'react';
import {connect} from "react-redux";
import StarRatings from "react-star-ratings";
import {RANK} from "../../constants"
import {
  Form,
  FormGroup,
  Col,
  Button,
  Alert,
  Label, Input, FormText
} from "reactstrap";


import {clearModal, createSubjectQuestions, fetchSubjects, toggleModal} from "../../store/action/subjectsAction";

import '../../component/FormHandler/FormHandler.css';
import ModalForm from "../../component/UI/Modal/Modal";
import AddForm from "../UI/Form/AddForm";
import {NotificationManager} from "react-notifications";

class FormHandler extends Component {


  constructor(props) {
    super(props);
    if (props.question) {
      this.state = {...props.question}
    } else {
      this.state = {
        subject: '',
        rank: 0,
        titleRu: '',
        titleRuSubject: '',
        titleKySubject: '',
        timer: '',
        main: false,
        titleKy: '',
        answers: [
          {position: 'a', titleRu: '', titleKy: '', isCorrect: false},
          {position: 'b', titleRu: '', titleKy: '', isCorrect: false},
          {position: 'c', titleRu: '', titleKy: '', isCorrect: false},
          {position: 'd', titleRu: '', titleKy: '', isCorrect: false},
          {position: 'e', titleRu: '', titleKy: '', isCorrect: false}
        ],
        methodSolution: null,
        show: false,
        subjectTitleRu: 'Выберите предмет',
        subjectTitleKy: 'Сураныч, бир теманы тандап',
        themeRu: '',
        themeKy: '',
        isFree: false,
        isCorrectFile: false,
      }
    }
  }


  showSelectSubject = (event) => {
    event.preventDefault();
    this.setState(prevState => ({
      show: !prevState.show
    }));
  };

  selectSubject = (titleRu, titleKy, id) => {
    this.setState({
      subjectTitleRu: titleRu,
      subjectTitleKy: titleKy,
      subject: id,
      show: false
    })
  };


  componentDidMount() {
    if (this.props.user && this.props.user !== "admin") {
      this.props.history.push('/');
    }

    this.props.fetchSubjects()
  }

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
    if (this.props.errorSubject) {
      this.props.clearModal();
    }
  };

  fileChangeHandler = event => {
    event.preventDefault();
    const fileValidation = /(.*?)\.(jpg|bmp|jpeg|png|img)$/;
    if (event.target.value.match(fileValidation)) {
      this.setState({
        [event.target.name]: event.target.files[0]
      })
    } else {
      NotificationManager.error('Не поддерживаемый формат файла')
    }
  };

  submitFormHandler = event => {
    event.preventDefault();

    const formData = new FormData();

    const answers = JSON.stringify(this.state.answers);
    const subject = JSON.stringify(this.state.subject);

    formData.append("titleRu", this.state.titleRu);
    formData.append("titleKy", this.state.titleKy);
    formData.append("subject", subject);
    formData.append("rank", this.state.rank);
    formData.append("themeKy", this.state.themeKy);
    formData.append("themeRu", this.state.themeRu);
    formData.append("isFree", this.state.isFree);
    formData.append("methodSolution", this.state.methodSolution);
    formData.append("answers", answers);

    this.props.onSubmit(formData);
  };

  changeRadioMain = () => {
    this.setState(prevState => ({
      main: !prevState.main
    }))
  };

  submitSubject = event => {
    event.preventDefault();
    event.stopPropagation();
    const subject = {
      titleRu: this.state.titleRuSubject,
      titleKy: this.state.titleKySubject,
      timer: this.state.timer,
      main: this.state.main
    };

    if (!this.props.errorSubject) {
      if (this.state.titleRuSubject !== '' && this.state.titleKySubject !== '') {
        this.props.createSubjectQuestions(subject);
      }
    }
  };


  handleAnswerChange = index => event => {
    const newAnswer = this.state.answers.map((answers, answersIndex) => {
      if (index !== answersIndex) return answers;
      return {...answers, [event.target.name]: event.target.value};
    });

    this.setState({answers: newAnswer});
  };

  changeRadio = (position) => {
    const answers = this.state.answers.map((answer) => {
      answer.isCorrect = false;
      if (answer.position === position) {
        answer.isCorrect = !answer.isCorrect;
        this.setState({
          isCorrectFile: answer.isCorrect
        })
      }
      return answer
    });
    this.setState({answers});
  };

  changeCheckbox = () => {
    this.setState(prevState => ({
      isFree: !prevState.isFree
    }))
  };

  changeRank = (newRank) => {
    this.setState({rank: newRank});
  };
  onChangeTimer = timer => this.setState({timer});

  render() {
    return (
      <div className="formForUser">
        {this.props.error && this.props.error.global && (
          <Alert color="danger">
            Проверьте подключение к интернету
          </Alert>
        )}
        <Form onSubmit={this.submitFormHandler}>
          <FormGroup row>
            <Label for="answer" sm={2}>Предмет</Label>
            <Col sm={5}>
              <div id="selectBox" className="form-control" onClick={this.showSelectSubject}>
                <p className="valueTag" name="select">
                  <span>{this.state.subjectTitleRu};</span>
                  <span className="subjectTitleSelect">{this.state.subjectTitleKy}</span>
                  {this.state.show === true ? <i className="fas fa-arrow-down arrow"/> :
                    <i className="fas fa-arrow-right arrow"/>}
                </p>
              </div>
              {this.state.show === true ?
                <div className="dropDownSelectDiv">
                  <ul id="selectMenuBox" className="dropDownSelect">
                    {this.props.subjects.map((subject, index) => (
                      <li key={subject._id} id={`subjectTitle${index + 1}`}
                          onClick={() => this.selectSubject(subject.titleRu, subject.titleKy, subject._id)}>
                                                <span
                                                  className={`subjectTitle`}>{subject.titleRu}; </span><span
                        className="subjectTitle">{subject.titleKy}</span>
                      </li>
                    ))}
                  </ul>
                  <hr/>
                  <Button color="link" size="sm" className="addButtonSubject"
                          onClick={this.props.toggleModal}>
                    <i className="fas fa-plus plus-button"/>Добавить предмет
                  </Button>
                </div>
                : null}
              <ModalForm
                toggleModal={() => this.props.toggleModal()}
                submitSubject={this.submitSubject}
                onChange={this.inputChangeHandler}
                errorSubject={this.props.errorSubject}
                titleKySubject={this.state.titleKySubject}
                titleRuSubject={this.state.titleRuSubject}
                titleRu={this.state.titleRu}
                titleKy={this.state.titleKy}
                modal={this.props.modal}
                changeRadio={this.changeRadioMain}
                onChangeTimer={this.onChangeTimer}
              />
            </Col>
            <Col sm={2} className="starRank">
              <Label for="rank">Количество баллов: </Label>
            </Col>
            <Col sm={3} className="starDimension">
              <StarRatings
                starDimension={'30px'}
                rating={this.state.rank}
                starRatedColor="orange"
                changeRating={this.changeRank}
                numberOfStars={RANK}
                name='rating'
                id='rank'
              />
            </Col>
          </FormGroup>
          <AddForm
            changeCheckbox={this.changeCheckbox}
            theme={this.props.themeText}
            title={this.props.titleText}
            themeValue={this.props.themeValue === 'themeRu' ? this.state.themeRu : this.state.themeKy}
            titleValue={this.props.titleValue === 'titleRu' ? this.state.titleRu : this.state.titleKy}
            placeholderTheme={this.props.placeholderTheme}
            placeholderTitle={this.props.placeholderTitle}
            titleAnswers={this.props.titleAnswers}
            inputChangeHandler={this.inputChangeHandler}
            answers={this.state.answers}
            changeRadio={this.changeRadio}
            handleAnswerChange={this.handleAnswerChange}
            value={this.props.value}
          />
          {this.state.isCorrectFile ?
            <FormGroup row>
              <Col sm={2}>
                <Label for="exampleFile">Загрузите решение:</Label>
              </Col>
              <Col>
                <Input sm={10} type="file" name="methodSolution" id="exampleFile" onChange={this.fileChangeHandler}/>
                <FormText color="muted">
                    Поддерживаются файлы формата: .png, .img, .jpeg, .bmp, .jpg
                </FormText>
              </Col>
            </FormGroup> : null}
          <FormGroup row>
            <Col sm={{offset: 2, size: 10}}>
              <Button type="submit" color="primary">Добавить</Button>
            </Col>
          </FormGroup>
        </Form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  error: state.admin.error,
  errorSubject: state.subject.createError,
  subjects: state.subject.subjects,
  modal: state.subject.modal
});

const mapDispatchToProps = dispatch => ({
  fetchSubjects: () => dispatch(fetchSubjects()),
  toggleModal: () => dispatch(toggleModal()),
  clearModal: () => dispatch(clearModal()),
  createSubjectQuestions: subjectData => dispatch(createSubjectQuestions(subjectData))
});

export default connect(mapStateToProps, mapDispatchToProps)(FormHandler);
