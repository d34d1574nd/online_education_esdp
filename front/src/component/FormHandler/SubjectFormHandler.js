import React, {Component} from 'react';
import {connect} from "react-redux";
import {
    Alert,
    Button,
    Col,
    Form,
    FormFeedback,
    FormGroup,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Label
} from "reactstrap";
import TimePicker from "react-time-picker";

import './SubjectFormHandler.css';

class AddSubject extends Component {

    constructor(props) {
        super(props);
        if (props.subject) {
            this.state = {...props.subject}
        } else {
            this.state = {
                titleRu: '',
                titleKy: '',
                main: false,
                timer: ''
            };
        }
    }

    componentDidMount() {
        if (this.props.user && this.props.user !== "admin") {
            this.props.history.push('/');
        }
    }

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    changeRadio = () => {
        this.setState(prevState => ({
            main: !prevState.main
        }))
    };

    submitFormHandler = async event => {
        event.preventDefault();
        this.props.onSubmit({...this.state});
        console.log(this.state);
    };

    onChangeTimer = timer => this.setState({ timer });
    render() {
        return (
            <div className="formForUser">
                {this.props.error && this.props.error.global && (
                    <Alert color="danger">
                        Проверьте подключение к интернету
                    </Alert>
                )}
                <Form onSubmit={this.submitFormHandler} style={{marginTop: "40px"}}>
                    <FormGroup row>
                        <Label for="titleRu" sm={2}>Название предмета (на русском)</Label>
                        <Col sm={5}>
                            <InputGroup>
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText>
                                        <Label check>
                                            <Input addon type='checkbox'
                                                   name="main"
                                                   onChange={this.changeRadio}
                                                   />
                                        </Label>
                                    </InputGroupText>
                                </InputGroupAddon>
                            <Input invalid={this.props.error}
                                   type="text" name="titleRu"
                                   id="titleRu"
                                   placeholder="Введите название предмета на русском языке"
                                   value={this.state.titleRu}
                                   onChange={this.inputChangeHandler}
                            />

                            </InputGroup>
                            <FormFeedback>{this.props.error && this.props.error.errors && this.props.error.errors['titleRu'] && this.props.error.errors['titleRu'].message}</FormFeedback>
                        </Col>
                        <Col sm={5}>
                            <Input invalid={this.props.error}
                                   type="text" name="titleKy"
                                   id="titleKy"
                                   placeholder="Введите  название предмета на кыргызском языке"
                                   value={this.state.titleKy}
                                   onChange={this.inputChangeHandler}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label sm={2}>Введите время:</Label>
                        <Col sm={10}>
                            <TimePicker
                              onChange={this.onChangeTimer}
                              value={this.state.timer}
                              format="H:m"
                              locale="ru-RU"
                              maxTime="05:00"
                              className="timerInput"
                              disableClock={true}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Col sm={{offset: 2, size: 10}}>
                            <Button type="submit" color="primary">Добавить</Button>
                        </Col>
                    </FormGroup>
                </Form>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    error: state.subject.createError
});


export default connect(mapStateToProps)(AddSubject);
