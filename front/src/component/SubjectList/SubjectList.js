import React, {Component} from 'react';
import {adminFetchSubjects, deleteSubject, getSubject} from "../../store/action/adminAction";
import {Button, Table, Col, Row, Modal, ModalHeader, ModalBody, ModalFooter} from "reactstrap";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import {faEdit} from "@fortawesome/free-solid-svg-icons";
import {faTrash} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

class SubjectList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            id: ""
        };
        this.toggle = this.toggle.bind(this);
    }

    toggle = (id) => {
        this.setState(prevState => ({
            modal: !prevState.modal,
            id: id
        }));
    };

    componentDidMount() {
        this.props.adminFetchSubjects();
    }

    editSubject = id => {
        this.props.getSubject(id);
    };

    deleteSubject = id => {
        this.props.deleteSubject(id);
        this.toggle();
    };

    render() {
        console.log(this.props.subjects);
        return (
            <Row form style={{marginTop: '40px'}} sm="12" md={{size: 6, offset: 3}}>
                <Col md={12}>
                    <Link to='/admin/newSubject'>
                        <Button
                            className="float-right"
                            color="info"
                        >
                            Добавить предмет
                        </Button>
                    </Link>
                    <h2 className="my-3">Все предметы</h2>
                    <div className="table-container">
                        <Table bordered>
                            <thead>
                            <tr className="d-flex">
                                <th className="col-1">№</th>
                                <th className="col-8">Название предмета</th>
                                <th className="col-3">Действия</th>
                            </tr>
                            </thead>
                            <tbody>
                            {this.props.subjects.map((subject, index) => {
                                return <tr key={subject._id} className="d-flex">
                                    <td className="col-1">{index + 1}</td>
                                    <td className="col-8">
                                        <strong>{subject.main === true ? "Основной предмет" : "Дополнительный предмет"}</strong>
                                        <br/>
                                        <strong>На русском: </strong>{subject.titleRu}
                                        <br/>
                                        <strong> На кыргызском: </strong>{subject.titleKy}
                                        <br/>
                                        <strong>Время прохождения предмета: </strong>{subject.timer}
                                    </td>
                                    <td className="col-3">
                                        <Button id={`editSubject${index + 1}`} className="mx-2" type="button"
                                                color="warning"
                                                onClick={() => this.editSubject(subject._id)}>
                                            <FontAwesomeIcon icon={faEdit}/>
                                        </Button>
                                        <Button id={`deleteSubject${index + 1}`} className="mx-2" type="button" color="danger"
                                                onClick={() => this.toggle(subject._id)}>
                                            <FontAwesomeIcon icon={faTrash}/>
                                        </Button>
                                    </td>
                                </tr>
                            })}
                            </tbody>
                        </Table>
                    </div>
                    <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                        <ModalHeader toggle={this.toggle}>Удаление</ModalHeader>
                        <ModalBody>
                            Вы дейсвительно хотите удалить ?
                        </ModalBody>
                        <ModalFooter>
                            <Button color="danger"
                                    onClick={() => this.deleteSubject(this.state.id)}>Удалить</Button>{' '}
                            <Button color="secondary" onClick={this.toggle}>Закрыть</Button>
                        </ModalFooter>
                    </Modal>
                </Col>
            </Row>
        )
    }
}

const mapStateToProps = state => ({
    user: state.user.user,
    subjects: state.admin.subjects

});

const mapDispatchToProps = dispatch => ({
    adminFetchSubjects: () => dispatch(adminFetchSubjects()),
    deleteSubject: (id) => dispatch(deleteSubject(id)),
    getSubject: (id) => dispatch(getSubject(id))
});


export default connect(mapStateToProps, mapDispatchToProps)(SubjectList);
