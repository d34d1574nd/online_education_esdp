import React from 'react';
import './Table.css';
import {NavLink} from "react-router-dom";

const Table = (props) => {

    const fields = props.data.map((item, index) => {
      const date = new Date(item.date);
        let zero;
        (date.getMonth() > 8) ? zero = '' : zero = '0';
      return (
            <tr key={item._id}>
                <td className={`Slot ${index + 1}`}>{(date.getDate() + '.' + zero + (date.getMonth() + 1) + '.' + date.getFullYear())}</td>
                <td className={`Slot ${index + 1}`}>{item.timer}</td>
                <td className={`Slot ${index + 1}`}>{item.result}</td>
                <td className={`Slot ${index + 1}`}><NavLink to={`/detail_result/${item._id}`}>Подробнее</NavLink></td>
            </tr>
        )
    });

    return (
        <div>
            <table className="Results-Table">
                <thead>
                <tr className="Table-Row">
                    <th className="Head-Slot">Дата</th>
                    <th className="Head-Slot">Время прохождения</th>
                    <th className="Head-Slot">Баллы</th>
                    <th className="Head-Slot">Подробно</th>
                </tr>
                </thead>
                <tbody>
                {fields}
                </tbody>
            </table>
        </div>
    );
};

export default Table;
