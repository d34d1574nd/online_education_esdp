import React, {Component} from 'react';
import {Col, Row, Table} from "reactstrap";
import {adminFetchQuestions, adminFetchSubjects} from "../../store/action/adminAction";
import connect from "react-redux/es/connect/connect";
import {fetchUsers} from "../../store/action/UserAction";

class AdminMainPage extends Component {

    componentDidMount() {
        this.props.adminFetchQuestions();
        this.props.adminFetchSubjects();
        this.props.fetchUsers();
    }

    render() {
        return (
            <Row form style={{marginTop: '40px'}} sm="12" md={{size: 6, offset: 3}}>
                <Col md={12}>
                    <h2 className="my-3">Статистика</h2>
                    <div className="table-container">
                        <Table bordered>
                            <thead>
                            <tr className="d-flex">
                                <th className="col-1">№</th>
                                <th className="col-8">Наименование</th>
                                <th className="col-3">Количество</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr className="d-flex">
                                <td className="col-1">1</td>
                                <td className="col-8">Вопросы</td>
                                <td className="col-3">{this.props.questions && this.props.questions.length}</td>
                            </tr>
                            <tr className="d-flex">
                                <td className="col-1">2</td>
                                <td className="col-8">Предметы</td>
                                <td className="col-3">{this.props.subjects && this.props.subjects.length}</td>
                            </tr>
                            <tr className="d-flex">
                                <td className="col-1">3</td>
                                <td className="col-8">Пользователи</td>
                                <td className="col-3">{this.props.users && this.props.users.length}</td>
                            </tr>
                            </tbody>
                        </Table>
                    </div>
                </Col>
            </Row>
        );
    }
}

const mapStateToProps = state => ({
    users: state.user.users,
    questions: state.admin.questions,
    subjects: state.admin.subjects

});

const mapDispatchToProps = dispatch => ({
    adminFetchQuestions: () => dispatch(adminFetchQuestions()),
    adminFetchSubjects: () => dispatch(adminFetchSubjects()),
    fetchUsers: () => dispatch(fetchUsers())
});

export default connect(mapStateToProps, mapDispatchToProps)(AdminMainPage);
