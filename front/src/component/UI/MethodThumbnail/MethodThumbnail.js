import React from 'react';

import config from "../../../config";

const styles = {
  width: '50%',
  marginRight: '10px'
};

const MethodThumbnail = props => {
  let image = null;

  if (props.image) {
    image = config.apiURL + '/uploads/' + props.image;
  }

  return <img src={image} style={styles} align="top" className="img-thumbnail" alt="method solution" />;
};

export default MethodThumbnail;
