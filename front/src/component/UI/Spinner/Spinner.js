import React from 'react';

import './Spinner.css';

const Spinner = () => {
  return (
    <div className='Spinner'>
      <div className="lds-ring">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
      </div>
      <div className='textUsers'>
        <span className='text textRu'>Ожидаю результаты</span>
        <span className='text textKy'>Күтө туруңуз</span>
      </div>
    </div>
  );
};

export default Spinner;
