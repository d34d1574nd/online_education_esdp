import React, {Fragment} from 'react';
import {Col, FormGroup, Input, InputGroup, InputGroupAddon, InputGroupText, Label} from "reactstrap";

const AddForm = (props) => {
  return (
    <Fragment>
      <FormGroup row className="displayPreSubjectSelect">
        <Label for="answer" sm={2}>Тема предмета</Label>
        <Col sm={10}>
          <Input
            type="text" name={props.theme}
            id={props.theme}
            placeholder={props.placeholderTheme}
            value={props.themeValue}
            onChange={props.inputChangeHandler}
          />
        </Col>
      </FormGroup>
      <FormGroup row className="displayPreSubjectSelect">
        <Label for="answer" sm={2}>Вопрос (галочку если бесплатный)</Label>
        <Col sm={10}>
          <InputGroup>
            <InputGroupAddon addonType="prepend">
              <InputGroupText>
                <Label check name="isFree">
                  <Input addon type='checkbox'
                         name="isFree"
                         onChange={props.changeCheckbox}
                  />
                </Label>
              </InputGroupText>
            </InputGroupAddon>
          <Input
            type="text" name={props.title}
            id={props.title}
            placeholder={props.placeholderTitle}
            value={props.titleValue}
            onChange={props.inputChangeHandler}
          />
          </InputGroup>
        </Col>
      </FormGroup>
      {props.answers.map((answer, index) => (
          <div key={index}>
            <FormGroup row>
              <Label for="answer" sm={2}>Вариант ответа {answer.position}</Label>
              <Col sm={10}>
                <InputGroup>
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <Label check>
                        <Input addon type='radio'
                               name="isCorrect" required
                               className={`class${index + 1}`}
                               onChange={() => props.changeRadio(answer.position)}/>
                      </Label>
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input
                    type="text" name={props.titleAnswers === 'answersRu' ? 'titleRu' : 'titleKy'}
                    id="answer"
                    placeholder={`Добавить вариант ответа #${answer.position}`}
                    value={props.value === 'titleRu' ? answer.titleRu : answer.titleKy }
                    onChange={props.handleAnswerChange(index)}
                  />
                </InputGroup>
              </Col>
            </FormGroup>
          </div>
        )
      )}
    </Fragment>
  );
};

export default AddForm;
