import React, {Component, Fragment} from 'react';
import {Col, FormGroup, Input, Label} from "reactstrap";
import {EditorState, convertToRaw, convertFromRaw} from 'draft-js';
import {Editor} from 'react-draft-wysiwyg';

import '../../../../../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import './AddForm.css'

class AddForm extends Component {
    state = {
        editorState: EditorState.createEmpty(),
        answersStates: [
            EditorState.createEmpty(),
            EditorState.createEmpty(),
            EditorState.createEmpty(),
            EditorState.createEmpty(),
            EditorState.createEmpty()
        ]

    };

    componentDidMount() {
        const {titleValue} = this.props;
        // Если есть данные конвертируем из текста в JSON
        const convertedValue = titleValue ? convertFromRaw(JSON.parse(titleValue)) : '';
        if (convertedValue) {
            // Если есть данные, берём JSON и конвертируем в объект EditorState
            let editorState = EditorState.createWithContent(convertedValue);
            // Перемещаем курсор в конец
            editorState = EditorState.moveFocusToEnd(editorState);
            this.setState({editorState});
        }

        // Конвертируем ответы из текстового формата в EditorState
        const newAnswersStates = this.props.answers.map(answer => {
            const answerText = answer[this.props.title];
            const convertedValue = answerText.length ? convertFromRaw(JSON.parse(answerText)) : '';
            if (convertedValue) {
                // Если есть данные, берём JSON и конвертируем в объект EditorState
                let editorState = EditorState.createWithContent(convertedValue);
                // Перемещаем курсор в конец
                editorState = EditorState.moveFocusToEnd(editorState);
                return editorState
            }
            return EditorState.createEmpty()
        });
        this.setState({answersStates: newAnswersStates})
    }

    onQuestionChange = (editorState) => {
        const fieldName = this.props.title;
        // Конвертируем содержимое EditorState сначала в JSON, затем в строковое значение
        const rawDraftContentState = JSON.stringify(convertToRaw(editorState.getCurrentContent()));
        // Сохраняем
        this.props.questionChangeHandler(fieldName, rawDraftContentState);
        this.setState({editorState});
    };

    onAnswerChange = index => editorState => {
        const name = this.props.title;
        const rawDraftContentState = JSON.stringify(convertToRaw(editorState.getCurrentContent()));
        // Т.к. handleAnswerChange функция высокого уровня, просим её родить нам новую функцию
        const handlerFn = this.props.handleAnswerChange(index);
        // ... вызываем новую функцию
        handlerFn(name, rawDraftContentState);
        // Формируем новый стейт
        const newAnswer = this.state.answersStates.map((answer, answerIndex) => {
            if (index !== answerIndex) return answer;
            return editorState;
        });
        this.setState({answersStates: newAnswer});
    };

    render() {
        const {editorState} = this.state;
        return (
            <Fragment>
                <FormGroup row className="displayPreSubjectSelect">
                    <Label for="answer" sm={2}>Тема предмета</Label>
                    <Col sm={10}>
                        <Input
                            type="text" name={this.props.theme}
                            id={this.props.theme}
                            placeholder={this.props.placeholderTheme}
                            value={this.props.themeValue}
                            onChange={this.props.inputChangeHandler}
                        />
                    </Col>
                </FormGroup>
                <FormGroup row className="displayPreSubjectSelect">
                    <Label for="answer" sm={2}>Вопрос</Label>
                    <Col sm={10}>
                        <Editor
                            editorState={editorState}
                            wrapperClassName="demo-wrapper"
                            editorClassName="rdw-editor-toolbar"
                            onEditorStateChange={this.onQuestionChange}
                        />
                    </Col>
                </FormGroup>
                {this.props.answers.map((answer, index) => {
                        const [answerState] = this.state.answersStates.slice(index, index + 1);
                        return (
                            <div key={index}>
                                <FormGroup row>
                                    <Label for="answer" sm={2}>Вариант ответа {answer.position}</Label>
                                    <Col sm={1}>
                                        <Label check className="Radio">
                                            <Input addon type='radio'
                                                   name="isCorrect" required
                                                   className={`class${index + 1}`}
                                                   onChange={() => this.props.changeRadio(answer.position)}/>
                                        </Label>
                                    </Col>
                                    <Col sm={9}>
                                        <Editor
                                            editorState={answerState}
                                            wrapperClassName="demo-wrapper"
                                            editorClassName="rdw-editor-toolbar"
                                            onEditorStateChange={this.onAnswerChange(index)}
                                        />
                                    </Col>
                                </FormGroup>
                            </div>
                        )
                    }
                )}
            </Fragment>
        );
    }
}

export default AddForm;