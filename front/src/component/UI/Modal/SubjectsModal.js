import React from 'react';
import PropTypes from 'prop-types';
import {Button, Col, Modal, ModalBody, ModalFooter, ModalHeader, Row} from "reactstrap";



const SubjectsModalForm = props => {
    return (
        <Modal isOpen={props.modal} toggle={props.toggleModal}>
            <ModalHeader>Выберите предметы</ModalHeader>
            <ModalBody>
                <Row className="form-row">
                    <Col xs="6">
                        <p>Основные предметы: </p>
                        {props.subjects.map((subject, index) => {
                            if(subject.main === true){
                                return (
                                    <ul key={index}>
                                        <li><input type="checkbox" value={subject._id} name={`subject${index + 1}`}
                                                   id={`subject${index + 1}`}
                                                   onChange={props.changeCheckbox}/><label
                                            htmlFor={`subject${index + 1}`}><span/> {subject.titleRu} {subject.titleKy}
                                        </label></li>
                                    </ul>
                                )
                            }
                        })}
                    </Col>
                    <Col xs="6">
                        <p> Дополнительные предметы:</p>
                        {props.subjects.map((subject, index) => {
                            if(subject.main === false){
                                return (
                                    <ul key={index}>
                                        <li><input value={subject._id} type="checkbox"
                                                   name={`subjectAdd${index + 1}`}
                                                   id={`subjectAdd${index + 1}`}
                                                   onChange={props.changeCheckbox}/><label
                                            htmlFor={`subjectAdd${index + 1}`}><span/> {subject.titleRu} {subject.titleKy}
                                        </label></li>
                                    </ul>
                                )
                            }
                        })}
                    </Col>
                </Row>
            </ModalBody>
            <ModalFooter>
                <Button color="danger" onClick={props.toggleModal}>Закрыть</Button>
                <Button color="primary" onClick={props.tests}>Начать тест</Button>
            </ModalFooter>
        </Modal>
    )
        ;
};

SubjectsModalForm.propTypes = {
    toggleModal: PropTypes.func.isRequired,
    tests: PropTypes.func.isRequired,
    changeCheckbox: PropTypes.func.isRequired,
   subjects: PropTypes.string.isRequired,
    modal: PropTypes.bool

};
export default SubjectsModalForm;
