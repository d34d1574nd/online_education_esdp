import React from 'react';
import PropTypes from 'prop-types';
import {
    Button,
    Col,
    Form,
    FormFeedback,
    FormGroup,
    Input,
    InputGroup, InputGroupAddon, InputGroupText,
    Label,
    Modal,
    ModalBody,
    ModalHeader
} from "reactstrap";
import TimePicker from "react-time-picker";


const ModalForm = props => {
    return (
        <Modal isOpen={props.modal} toggle={props.toggleModal}>
            <ModalHeader toggle={props.toggleModal}> Добавьте предмет</ModalHeader>
            <ModalBody>
                <Form onSubmit={props.submitSubject}>
                    <FormGroup>

                        <Label for={props.titleRu}>Название предмета (на русском)</Label>
                        <InputGroup>
                            <InputGroupAddon addonType="prepend">
                                <InputGroupText>
                                    <Label check>
                                        <Input addon type='checkbox'
                                               name="main"
                                               onChange={props.changeRadio}
                                        />
                                    </Label>
                                </InputGroupText>
                            </InputGroupAddon>
                        <Input invalid={!!props.errorSubject}
                               type="text" name='titleRuSubject'
                               id='titleRuSubject' required
                               placeholder="Введите название предмета на русском языке"
                               value={props.titleRuSubject}
                               onChange={props.onChange}
                        />
                        <FormFeedback>{props.errorSubject && props.errorSubject.errors && props.errorSubject.errors['titleRu'] && props.errorSubject.errors['titleRu'].message}</FormFeedback>
                        </InputGroup>
                    </FormGroup>
                    <FormGroup>
                        <Label for={props.titleKy}>Название предмета (на Кыргызском)</Label>
                        <Input invalid={!!props.errorSubject}
                               type="text" name='titleKySubject'
                               id='titleKySubject' required
                               placeholder="Введите название предмета на кыргызском языке"
                               value={props.titleKySubject}
                               onChange={props.onChange}
                        />
                        <FormFeedback>{props.errorSubject && props.errorSubject.errors && props.errorSubject.errors['titleRu'] && props.errorSubject.errors['titleRu'].message}</FormFeedback>
                    </FormGroup>
                    <FormGroup row>
                        <Col sm={4}>
                            <Label for="timer">Введите время</Label>
                        </Col>
                        <Col sm={8}>
                            <TimePicker
                              onChange={props.onChangeTimer}
                              value={props.timer}
                              format="H:m"
                              locale="ru-RU"
                              maxTime="05:00"
                              className="timerInput"
                              disableClock={true}
                            />
                        </Col>
                        <FormFeedback>{props.errorSubject && props.errorSubject.errors && props.errorSubject.errors['timer'] && props.errorSubject.errors['timer'].message}</FormFeedback>
                    </FormGroup>
                    <FormGroup row>
                        <Col sm={9}>
                            <Button color="primary" type="submit">Отправить</Button>
                        </Col>
                        <Col sm={3}>
                            <Button color="secondary"
                                    onClick={props.toggleModal}>Закрыть</Button>
                        </Col>
                    </FormGroup>
                </Form>

            </ModalBody>
        </Modal>
    )
        ;
};

ModalForm.propTypes = {
    toggleModal: PropTypes.func.isRequired,
    submitSubject: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    errorSubject: PropTypes.string,
    titleKySubject: PropTypes.string.isRequired,
    titleRuSubject: PropTypes.string.isRequired,
    titleRu: PropTypes.string.isRequired,
    titleKy: PropTypes.string.isRequired,
    modal: PropTypes.bool

};
export default ModalForm;
