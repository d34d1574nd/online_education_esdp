import React, {Fragment} from 'react';
import {NavItem, NavLink} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";

import './UserMenu.css';

const AnonymousMenu = () => (
  <Fragment>
    <NavItem>
      <NavLink tag={RouterNavLink} to="/register" className="user" exact>Регистрация</NavLink>
    </NavItem>
    <NavItem>
      <NavLink tag={RouterNavLink} to="/login" className="user" exact>Авторизация</NavLink>
    </NavItem>
  </Fragment>
);

export default AnonymousMenu;
