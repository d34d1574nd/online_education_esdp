import React, {Fragment} from 'react';
import {Navbar, NavLink} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";

import './UserMenu.css';

const showPaidStatus = (user) => {
    if (user.paid) {
        return <i className="far fa-check-circle paid"/>
    }
    return <i className="far fa-times-circle unPaid"/>
};

const NonAdminElements = ({user}) => (
    <Fragment>
        <span>Ваш лицевой счет: {user.phoneNumber}&nbsp;</span>
        <span>Ваш баланс: {user.balance}сом &nbsp; </span>
        {showPaidStatus(user)}
    </Fragment>
);

const UserMenu = ({user, logout}) => {
    return (
        <Navbar>
            Привет, <NavLink tag={RouterNavLink} rel="nofollow" to={user && user.role === 'admin' ? '/admin' : '/'}
                             className="user username">{user.surname} {user.name} </NavLink>
            {user.role !== 'admin' && <NonAdminElements user={user}/>}
            <NavLink tag={RouterNavLink} rel="nofollow" to="/" className="user" onClick={logout}>Выйти</NavLink>
        </Navbar>
    )
};

export default UserMenu;
