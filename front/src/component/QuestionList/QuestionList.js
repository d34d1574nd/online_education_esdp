import React, {Component} from 'react';
import {adminFetchQuestions, deleteQuestion, getQuestion} from "../../store/action/adminAction";
import {Button, Col, Modal, ModalBody, ModalFooter, ModalHeader, Row, Table} from "reactstrap";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import {faEdit} from "@fortawesome/free-solid-svg-icons";
import {faTrash} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import StarRatings from "react-star-ratings";
import {RANK} from "../../constants";
import {convertFromRaw, EditorState} from 'draft-js';
import {Editor} from 'react-draft-wysiwyg';

class QuestionList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            id: ""
        };
        this.toggle = this.toggle.bind(this);
    }

    toggle = (id) => {
        this.setState(prevState => ({
            modal: !prevState.modal,
            id: id
        }));
    };

    componentDidMount() {
        this.props.adminFetchQuestions();
    }

    editQuestion = id => {
        this.props.getQuestion(id);
    };

    deleteQuestion = id => {
        this.props.deleteQuestion(id);
        this.toggle();
    };

    renderEditorState = (storedStateFromDb) => {
        let editorState;

        if (!storedStateFromDb || storedStateFromDb.length === 0) {
            return
        }
        try {
            const convertedStoredState = convertFromRaw(JSON.parse(storedStateFromDb));
            editorState = EditorState.createWithContent(convertedStoredState);
        }catch (e) {
            return
        }
        return (
            <div className="readonly-editor">
                <Editor
                    editorState={editorState}
                    readOnly={true}
                    toolbarStyle={{display: 'None'}}
                />
            </div>
        )
    };

    render() {
        return (
            <Row form style={{marginTop: '40px'}} sm="12" md={{size: 6, offset: 3}}>
                <Col md={12}>
                    <Link to='/admin/newQuestion/ky'>
                        <Button
                            className="float-right marginButton"
                            color="info"
                        >
                            Добавить вопрос (кыргызский)
                        </Button>
                    </Link>
                    <Link to='/admin/newQuestion/ru'>
                        <Button
                            className="float-right"
                            color="info"
                        >
                            Добавить вопрос (русский)
                        </Button>
                    </Link>
                    <h2 className="my-3">Все вопросы</h2>
                    <div className="table-container">
                        <Table bordered>
                            <thead>
                            <tr className="d-flex">
                                <th className="col-1">№</th>
                                <th className="col-8">Название вопроса</th>
                                <th className="col-3">Действия</th>
                            </tr>
                            </thead>
                            <tbody>
                            {this.props.questions.map((question, index) => {
                                return <tr key={question._id} className="d-flex">
                                    <td className="col-1">{index + 1}</td>
                                    <td className="col-8">
                                        <span><strong>На русском: </strong>{this.renderEditorState(question.titleRu)}</span>
                                        <br/>
                                        <span><strong> На кыргызском: </strong>{this.renderEditorState(question.titleKy)}</span>
                                        <br/>
                                        <strong>Количество баллов: </strong>
                                        <StarRatings
                                            starDimension={'20px'}
                                            rating={question.rank}
                                            starRatedColor="orange"
                                            numberOfStars={RANK}
                                            name='rating'
                                            id='rank'
                                        />
                                    </td>
                                    <td className="col-3">
                                        <Button id={`editQuestion${index + 1}`} className="mx-2" type="button"
                                                color="warning" onClick={() => this.editQuestion(question._id)}>
                                            <FontAwesomeIcon icon={faEdit}/>
                                        </Button>
                                        <Button id={`deleteQuestion${index + 1}`} className="mx-2" type="button"
                                                color="danger"
                                                onClick={() => this.toggle(question._id)}>
                                            <FontAwesomeIcon icon={faTrash}/>
                                        </Button>
                                    </td>
                                </tr>
                            })}
                            </tbody>
                        </Table>
                    </div>
                    <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                        <ModalHeader toggle={this.toggle}>Удаление</ModalHeader>
                        <ModalBody>
                            Вы дейсвительно хотите удалить ?
                        </ModalBody>
                        <ModalFooter>
                            <Button color="danger"
                                    onClick={() => this.deleteQuestion(this.state.id)}>Удалить</Button>{' '}
                            <Button color="secondary" onClick={this.toggle}>Закрыть</Button>
                        </ModalFooter>
                    </Modal>
                </Col>
            </Row>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user.user,
    questions: state.admin.questions

});

const mapDispatchToProps = dispatch => ({
    adminFetchQuestions: () => dispatch(adminFetchQuestions()),
    deleteQuestion: (id) => dispatch(deleteQuestion(id)),
    getQuestion: (id) => dispatch(getQuestion(id))
});


export default connect(mapStateToProps, mapDispatchToProps)(QuestionList);
