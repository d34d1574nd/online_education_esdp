import React, {Component} from 'react';
import {Form, FormGroup, Input, Label} from "reactstrap";

class Question extends Component {
    render() {
        return (
            <Form className={this.props.form}>
                <FormGroup tag="fieldset" id={this.props.question} >
                    <legend>{this.props.question}</legend>
                    <FormGroup check>
                        <Label check>
                            <Input type="radio" className="radio1" id={this.props.answerAid} name={this.props.questionId} onChange={this.props.changed}/>{' a) '}
                            {this.props.answerA}
                        </Label>
                    </FormGroup>
                    <FormGroup check>
                        <Label check>
                            <Input type="radio" className="radio2" id={this.props.answerBid} name={this.props.questionId} onChange={this.props.changed}/>{' b) '}
                            {this.props.answerB}
                        </Label>
                    </FormGroup>
                    <FormGroup check>
                        <Label check>
                            <Input type="radio" className="radio3" id={this.props.answerCid} name={this.props.questionId} onChange={this.props.changed}/>{' c) '}
                            {this.props.answerC}
                        </Label>
                    </FormGroup>
                    <FormGroup check>
                        <Label check>
                            <Input type="radio" className="radio4" id={this.props.answerDid} name={this.props.questionId} onChange={this.props.changed}/>{' d) '}
                            {this.props.answerD}
                        </Label>
                    </FormGroup>
                    <FormGroup check>
                        <Label check>
                            <Input type="radio" className="radio5" id={this.props.answerEid} name={this.props.questionId} onChange={this.props.changed}/>{' e) '}
                            {this.props.answerE}
                        </Label>
                    </FormGroup>
                </FormGroup>
            </Form>

        );
    }
}

export default Question;
