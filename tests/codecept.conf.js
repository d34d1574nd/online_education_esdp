exports.config = {
  output: './output',
  helpers: {
    Puppeteer: {
      url: 'http://localhost:3010/',
        show: !process.env.CI,
        headless: !!process.env.CI
    }
  },
  include: {
    I: './steps_file.js'
  },
  mocha: {},
  bootstrap: null,
  teardown: null,
  hooks: [],
  gherkin: {
    features: './features/*.feature',
    steps: [
        './step_definitions/tests.js',
        './step_definitions/addQuestion.js',
        './step_definitions/editQuestion.js',
        './step_definitions/passedTest.js',
        './step_definitions/changeRole.js',
        './step_definitions/register.js',
        './step_definitions/login.js',
        './step_definitions/changeRank.js',
        './step_definitions/editSubject.js',
        './step_definitions/questionDelete.js',
        './step_definitions/deleteSubject.js',
        './step_definitions/exportPdf.js',
        './step_definitions/addSubject.js',
    ]
  },
  plugins: {
    screenshotOnFail: {
      enabled: true
    }
  },
  tests: './*_test.js',
  name: 'tests'
};
