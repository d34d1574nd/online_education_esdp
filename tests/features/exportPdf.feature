# language: ru

Функция: Редактирование предмета

  @exportPdf
  Сценарий: Редактирование предмета
    Допустим я нахожусь на странице Авторизация пользователя
    Если ввожу "(555) 33-00-83" в поле "phoneNumber"
    И ввожу "123456Aa" в поле "password"
    И нажимаю кнопку "Войти"
    И я вижу текст "Вы успешно вошли в систему!"
    И я вижу текст "Результаты"
    И я "Подробнее" нажал
    И я "Подробное описание прохождения теста" увидел текст
    И я нажму кнопку "export"
    То я вижу текст "Pdf файл прохождения теста готов"
