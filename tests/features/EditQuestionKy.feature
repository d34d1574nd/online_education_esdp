# language: ru

Функция: Редактирование вопросов и ответов теста

  @editQuestionKy
  Сценарий: Редактировать вопрос
    Допустим я нахожусь на странице Авторизация пользователя
    И ввожу "(555) 55-55-55" в поле "phoneNumber"
    И ввожу "123456Aa" в поле "password"
    И нажимаю кнопку "Войти"
    И я вижу текст "Вы успешно вошли в систему!"
    И я нажимаю иконку questions "Вопросы"
    И я надавил на кнопку Редактировать кыргызский
    И я нажимаю на див "Выберите предмет"
    И я нажимаю на кнопку "Добавить предмет"
    И я вижу модальное окно "Добавьте предмет"
    И я ввожу "математика 2" в "titleRuSubject"
    И я ввожу текст "математика 2" в "titleKySubject"
    И я часы "01" в поле "timer"
    И я минуты "30" в поле "timer"
    И я нажимаю кнопку "Отправить"
    И я нажимаю на див "Выберите предмет"
    И я нажимаю на див "Выберите предмет"
    И я нажимаю на спан "предмет2Кг"
    И я нажимаю на див звездочек "Количество баллов"
    И ввожу "Текст" в поле "themeKy"
    И я удаляю "удалить" в этом поле "themeKy"
    И ввожу "Вопрос русский" в поле "themeKy"
    И беру "ОтветА1" в "Добавить вариант ответа #A"
    И я выбираю правильный ответ "Ответ"
    И я нажимаю кнопку для отправки "Добавить"
    То я вижу ответ от сервера "Успешно изменено"

