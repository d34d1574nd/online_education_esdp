#!/bin/bash

REL_PATH=`dirname $0`
cd $REL_PATH
CURRENT_DIR=`pwd`

cd $CURRENT_DIR/../api

NODE_ENV=test yarn run seed

pm2 start "yarn start:test" --name="test_education_api"

cd $CURRENT_DIR/../front

pm2 start "yarn start:test" --name="test_education_front"

while ! nc -z localhost 8010; do
  sleep 0.1 # wait for 1/10 of the second before check again
done
while ! nc -z localhost 3010; do
  sleep 0.1 # wait for 1/10 of the second before check again
done

cd $CURRENT_DIR

set +e
yarn start
EXIT_CODE=$?
set -e

pm2 kill

exit $EXIT_CODE

