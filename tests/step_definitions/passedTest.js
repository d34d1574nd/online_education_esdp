const { I } = inject();
// Add in your custom step files

When('я ставлю правильный ответ {string}', () => {
    I.wait(10)
    I.click({xpath: `//form[@class='form1']//div[@class='form-check']//input[@class='radio1 form-check-input']`})
});
When('я кликаю на кнопку {string}', () => {
   I.click({xpath: `//li[@class="button next"]`})
});
When('я выберу правильный ответ {string}', () => {
    I.click({xpath: `//form[@class='form1']//div[@class='form-check']//input[@class='radio2 form-check-input']`})
});
When('я взял правильный ответ {string}', () => {
    I.click({xpath: `//form[@class='form1']//div[@class='form-check']//input[@class='radio3 form-check-input']`})
});
When('правильный ответ {string}', () => {
    I.click({xpath: `//form[@class='form1']//div[@class='form-check']//input[@class='radio4 form-check-input']`})
});
When('я ставлю ответ {string}', () => {
    I.click({xpath: `//form[@class='form1']//div[@class='form-check']//input[@class='radio5 form-check-input']`})
});
When('я кнопку {string}', () => {
    I.wait(5);
    I.click({xpath: `//button[@class='btn btn-secondary']`});
});
Then('я вижу баллы в таблице', () => {
    I.seeElement({xpath: `//table[@class='Results-Table']//tbody//tr[last()]//td[last()]`});
});
