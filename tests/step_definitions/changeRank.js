const {I} = inject();
// Add in your custom step files

When('я нажимаю на див звездочек {string}', () => {
    I.click(`//div[@class='starDimension col-sm-3']//div[2]`)
});

When('я вижу у добавленого вопроса 2 звезды {string}', () => {
    I.retry({ retries: 3, maxTimeout: 3000 }).seeElement(`//table/tbody/tr[last()]/td[2]/div[@title='2 Stars']`);
});