const {I} = inject();
// Add in your custom step files

When('я часы {string} в поле {string}', text => {
  I.fillField({xpath:`//div[@class='react-time-picker__inputGroup']//input[2]`} , text)
});
When('я минуты {string} в поле {string}', text => {
  I.fillField({xpath:`//div[@class='react-time-picker__inputGroup']//input[3]`} , text)
});
