const { I } = inject();
// Add in your custom step files

Given('я нахожусь на странице Регистрация нового пользователя', () => {
    I.amOnPage('register')
});

When('ввожу {string} в поле {string}', (text,fieldName) => {
    I.waitForElement('#' + fieldName);
    I.fillField('#' + fieldName, text);
});

When('я нажимаю кнопку {string}', buttonName => {
    I.click(buttonName);
});

Then('я вижу текст {string}', text => {

    I.waitForText(text);
});










