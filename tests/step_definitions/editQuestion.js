const {I} = inject();
// Add in your custom step files

When('я давлю на кнопку Редактировать русский', () => {
    I.wait(5)
    I.click({xpath: `//button[@id='editQuestion1']`})
});

When('я надавил на кнопку Редактировать кыргызский', () => {
    I.wait(5)
    I.click({xpath: `//button[@id='editQuestion9']`})
});

When('я удалить {string} в этом поле {string}', () => {
    I.click({xpath: `//input[@id='titleRu']`});
    I.pressKey(['Control', 'a']);
    I.pressKey("Backspace");
});


When('я удаляю {string} в этом поле {string}', () => {
    I.click({xpath: `//input[@id='titleKy']`});
    I.pressKey(['Control', 'a']);
    I.pressKey("Backspace");
});
