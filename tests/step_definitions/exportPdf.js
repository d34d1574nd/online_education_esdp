const {I} = inject();
// Add in your custom step files

When('я {string} нажал', () => {
  I.click({xpath: `//td[@class='Slot 10']//a`})
});

When('я {string} увидел текст', (text) => {
  I.waitForText(text)
});

When('я нажму кнопку {string}', () => {
  I.click({xpath: `//button[@class='btn btn-outline-primary']`})
});
