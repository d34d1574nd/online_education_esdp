const {I} = inject();
// Add in your custom step files

When('я выбираю  {string}', () => {
    I.wait(5)
    I.click(`//div[@class='form-row row']//div[1]//ul[1]//li[1]//label[1]`);
});

When('я беру предмет {string}', () => {
    I.click(`//div[@class='modal-body']//div[2]//ul[1]//li[1]//label[1]`);
});

When('я выбрал предмет {string}', () => {
    I.click(`//div[@class='modal-body']//div[2]//ul[2]//li[1]//label[1]`);
});

When('я нажимаю начать тест {string}', () => {
    I.click({xpath:`//button[@class='btn btn-primary']`});
});