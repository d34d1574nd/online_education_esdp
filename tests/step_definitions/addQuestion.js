const {I} = inject();
// Add in your custom step files

When('я нажимаю иконку questions {string}', () => {
    I.click({xpath:`//div[@id="questions"]`});
});

When('я нажимаю иконку subjects {string}', () => {
    I.click({xpath:`//div[@id="subjects"]`});
});

When('я нажимаю на див {string}', () => {
    I.click(`//div[@id='selectBox']`)
});

When('я нажимаю на кнопку {string}', button => {
    I.click(button);
});

When('я нажимаю на див звездочек {string}', () => {
    I.click(`//div[@class='starDimension col-sm-3']//div[2]`)
});

When("я нажимаю на спан {string}", () => {
    I.click(`//li[@id='subjectTitle2']`)
});

When('я ввожу {string} в {string}', text => {
    I.fillField({xpath:`//input[@id='titleRuSubject']`} , text)
});

When('я ввожу текст {string} в {string}', text => {
    I.fillField({xpath:`//input[@id='titleKySubject']`} , text)
});

When("я выбираю чекбокс {string}", () => {
    I.click(`//label[@name='isFree']`)
});

When('беру {string} в {string}', (text) => {
    I.fillField({xpath:`//input[@placeholder='Добавить вариант ответа #a']`}, text);
});
When('введу {string} в {string}', (text) => {
    I.fillField({xpath:`//input[@placeholder='Добавить вариант ответа #a']`}, text);
});
When('беру ответ {string} в {string}', (text) => {
    I.fillField({xpath:`//input[@placeholder='Добавить вариант ответа #b']`}, text);
});
When('введу ответ {string} в {string}', (text) => {
    I.fillField({xpath:`//input[@placeholder='Добавить вариант ответа #b']`}, text);
});
When('ответ {string} в {string}', (text) => {
    I.fillField({xpath:`//input[@placeholder='Добавить вариант ответа #c']`}, text);
});
When('ответ 2 {string} в {string}', (text) => {
    I.fillField({xpath:`//input[@placeholder='Добавить вариант ответа #c']`}, text);
});
When('хочу видеть {string} в {string}', (text) => {
    I.fillField({xpath:`//input[@placeholder='Добавить вариант ответа #d']`}, text);
});
When('хочу видеть 2 {string} в {string}', (text) => {
    I.fillField({xpath:`//input[@placeholder='Добавить вариант ответа #d']`}, text);
});
When('ввожу последний {string} в {string}', (text) => {
    I.fillField({xpath:`//input[@placeholder='Добавить вариант ответа #e']`}, text);
});
When('вижу последний 2 {string} в {string}', (text) => {
    I.fillField({xpath:`//input[@placeholder='Добавить вариант ответа #e']`}, text);
});

When('я загружаю файл правильного ответа {string}', () => {
    I.attachFile({xpath:`//input[@id='exampleFile']`}, 'file/images.jpeg');
});

When('я нажимаю кнопку для отправки {string}', () => {
    I.click({xpath:`//button[@class='btn btn-primary']`});
});

When('я выбираю правильный ответ {string}', () => {
    I.checkOption({xpath:`//input[@class='class2']`});
});

When('я вижу ответ от сервера {string}', text => {
    I.waitForText(text);
});

When('я вижу модальное окно {string}', () => {
   I.wait(`//h5[@class='modal-title']`)
});
