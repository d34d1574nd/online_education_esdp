const {I} = inject();
// Add in your custom step files

When('я жму кнопку Редактировать {string}', () => {
    I.click({xpath: `//button[@id='editSubject1']`})
});

When('я удаляю {string} в этом поле {string}',()  => {
    I.click({xpath:`//input[@id='titleRu']`} );
    I.pressKey(['Control','a']);
    I.pressKey("Backspace");
});
When('я удаляю {string} в этом поле {string}',()  => {
    I.click({xpath:`//input[@id='titleKy']`} );
    I.pressKey(['Control','a']);
    I.pressKey("Backspace");
});
