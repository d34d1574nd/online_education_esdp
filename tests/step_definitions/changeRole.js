const { I } = inject();
// Add in your custom step files


Given('я нахожусь на странице Авторизация пользователя', () => {
    I.amOnPage('login')
});

When('я{string} в поле {string}', (text,fieldName) => {
    I.waitForElement('#' + fieldName);
    I.fillField('#' + fieldName, text);
});

When('нажимаю кнопку {string}', button => {
    I.click(button);
    I.wait(10)
});

When('я нажимаю иконку users {string}', () => {
    I.wait(10);
    I.click({xpath:`//div[@id="users"]`});
});

When('я нажимаю на {string}', () => {
    I.wait(10);
    I.click({xpath: `//a[@id='adminRole']`});

});

When('я вижу пользователя {string}', () => {
    I.wait(4);
    I.waitForVisible("//td[.='U1 u1']")
});

When('я меняю роль у этого пользователя на {string}', (option) => {
    I.wait(2);
    I.selectOption("//td[.='U1 u1']/ancestor::tr/descendant::select[@name='select']",'admin');
});

Then('я вижу уведомление {string}', (text) => {
    I.waitForText(text);
});


