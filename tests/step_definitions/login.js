const { I } = inject();
// Add in your custom step files

Given('я нахожусь на странице Авторизация пользователя', () => {
    I.amOnPage('login')
});

When('я{string} в поле {string}', (text,fieldName) => {
    I.waitForElement('#' + fieldName);
    I.fillField('#' + fieldName, text);
});

When('нажимаю кнопку {string}', button => {
    I.click(button);
});

Then('я вижу текст {string}', text => {
    I.waitForText(text);
});

When('я выбираю ответ {string}', (text) => {
    I.waitForText(text);
});

